
import { _decorator, Component, Node, instantiate } from 'cc';
import { JCCS } from './JCC';
import { JChatUserListItem } from './JChatUserListItem';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JChatUserList
 * DateTime = Thu Dec 16 2021 13:52:15 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JChatUserList.ts
 * FileBasenameNoExtension = JChatUserList
 * URL = db://assets/resources/scripts/JChatUserList.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JChatUserList')
export class JChatUserList extends Component {

    @property(Node)
    contentNode:Node | null = null;

    @property(Node)
    userItem:Node | null = null;

    start () {
        // [3]
        let self = this;
        self.userItem.active = false;
        self.userItem.parent = self.node;
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    /**
     * upList
     */
    public upList() {
        let self = this;
        self.contentNode.removeAllChildren();
        JCCS.instance.SendServer.getUserList((res:any)=>{
            let users = JSON.parse(res).users;

            if(users)
            {
                for(let i = 0 ; i < users.length ; i++)
                {
                    let user  = users[i];
                    let cnode = instantiate(self.userItem);
                    let s = cnode.getComponent(JChatUserListItem);
                    cnode.active = true;
                    self.contentNode.addChild(cnode);
                    s.upItem(user);
                }                
            }

        })

    }



}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
