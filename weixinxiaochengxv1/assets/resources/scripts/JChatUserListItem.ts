
import { _decorator, Component, Node, Sprite, Label, SpriteFrame, systemEvent, SystemEvent, Input } from 'cc';
import { getUrlSpriteFrame } from './common/JTools';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JChatUserListItem
 * DateTime = Thu Dec 16 2021 13:52:30 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JChatUserListItem.ts
 * FileBasenameNoExtension = JChatUserListItem
 * URL = db://assets/resources/scripts/JChatUserListItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JChatUserListItem')
export class JChatUserListItem extends Component {

    @property(Sprite)
    headSprite:Sprite | null = null;

    @property(Label)
    userName:Label | null    = null;

    @property(Sprite)
    xinSprite:Sprite | null  = null;

    @property(SpriteFrame)
    xinSFs:SpriteFrame[]  = [];

    @property(Label)
    xinNumber:Label | null   = null;

    private _touchSW:boolean = true;

    start () {
        // [3]
    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    public upItem(data:any)
    {
        let self = this;

        let hsf = JCCS.instance.HeadSpriteFrame[data.avatar];

        if(hsf)
        {
            self.headSprite.spriteFrame = hsf;
        }else{
            getUrlSpriteFrame(data.avatar,(sf:SpriteFrame)=>{
                self.headSprite.spriteFrame = sf;
            })            
        }

        self.userName.string = data.nickname;

        if(data.like)
        {
            self.xinSprite.spriteFrame = self.xinSFs[1];
        }else{
            self.xinSprite.spriteFrame = self.xinSFs[0];

            self.xinSprite.node.on(Input.EventType.TOUCH_END,()=>{
                if(!self._touchSW)return;
                JCCS.instance.audioManager.playSound('button');
                JCCS.instance.SendServer.like(data.userId,(res:any)=>{
                    res = JSON.parse(res);
                    switch(res.result)
                    {
                        case -1:

                            break;
                        case 0:

                            break;
                        default:
                            self._touchSW = false;
                            self.xinSprite.spriteFrame = self.xinSFs[1];
                            self.xinNumber.string = res.result.toString();
                            break;
                    }
                })

            })

        }
        
        self.xinNumber.string = data.likeAmount.toString();


    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
