
import { _decorator, Component, Node, Sprite, SpriteFrame, EventTouch } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JSelectShou
 * DateTime = Mon Dec 06 2021 20:57:12 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JSelectShou.ts
 * FileBasenameNoExtension = JSelectShou
 * URL = db://assets/resources/scripts/JSelectShou.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JSelectShou')
export class JSelectShou extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    buttonSprite:Sprite[]       = [];

    @property(SpriteFrame)
    button_offs:SpriteFrame[]   = [];

    @property(SpriteFrame)
    button_ons:SpriteFrame[]    = [];

    @property(SpriteFrame)
    lingChongs:SpriteFrame[]     = [];

    @property(SpriteFrame)
    lingChongNames:SpriteFrame[] = [];

    @property(Sprite)
    lingChong:Sprite | null     = null;

    @property(Sprite)
    lingChongName:Sprite | null = null;


    start () {
        // [3]
        let self = this;

        for(let i = 0 ; i < 3;i++)
        {
            let button = self.buttonSprite[i];
            button.node.j_value = {index:i,touch:false};
        }
        JCCS.instance.LingChong = 0;
        self.upSelect(0);
    }

    private onSelect(event:EventTouch)
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        let node = event.target as Node;
        if(node.j_value.touch)
        {

        }else{
            self.upSelect(node.j_value.index);
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private upSelect(index:number)
    {
        let self = this;
        for(let i = 0 ; i < 3;i++)
        {
            if(index == i)
            {
                self.buttonSprite[i].spriteFrame = self.button_ons[i];
                self.buttonSprite[i].node.j_value.touch = true;
                
                self.lingChong.spriteFrame     = self.lingChongs[i];
                self.lingChongName.spriteFrame = self.lingChongNames[i];   
                
                JCCS.instance.LingChong = i+1;

            }else{
                self.buttonSprite[i].spriteFrame = self.button_offs[i];
                self.buttonSprite[i].node.j_value.touch = false;
            }
            

        }  
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
