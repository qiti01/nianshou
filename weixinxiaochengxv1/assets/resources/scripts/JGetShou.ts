
import { _decorator, Component, Node, Sprite, Label, SpriteFrame } from 'cc';
import { showTip } from './common/JTools';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JGetShou
 * DateTime = Tue Dec 07 2021 15:00:49 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JGetShou.ts
 * FileBasenameNoExtension = JGetShou
 * URL = db://assets/resources/scripts/JGetShou.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JGetShou')
export class JGetShou extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    longzi:Sprite | null = null;

    @property(SpriteFrame)
    longzisfs:SpriteFrame[] = [];

    @property(Sprite)
    sou:Sprite | null = null;

    @property(SpriteFrame)
    sousfs:SpriteFrame[] = [];

    @property(Label)
    describe:Label | null = null;


    start () {
        // [3]
        let self = this;
        let index = Number(self.node.j_convey_data);

        self.longzi.spriteFrame = self.longzisfs[index-1];
        
        self.sou.spriteFrame  =  self.sousfs[index-1];

        switch(index)
        {
            case 1:
                self.describe.string = '发现了一只噬运兽，正在睡觉';
                break;
            case 2:
                self.describe.string = '发现了一只噬财兽，正在偷钱';
                break;
            case 3:
                self.describe.string = '发现了一只噬情兽，正在玩耍';
                break;
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private onDelete()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.node.removeFromParent();
        
    }


    private onZhua()
    {
        let self = this;
        self.onDelete();
        showTip(JCCS.instance.MainNode,'成功抓住年兽');
        JCCS.instance.audioManager.playSound('button');

        JCCS.instance.dakaID++;
        //localStorage.setItem('dakaid',JCCS.instance.dakaID.toString());
        JCCS.instance.mapNode.upSpotUI();
        JCCS.instance.checkin();
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
