
import { _decorator, Component, Node, Label } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JTip
 * DateTime = Wed Dec 08 2021 17:13:08 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JTip.ts
 * FileBasenameNoExtension = JTip
 * URL = db://assets/resources/scripts/JTip.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JTip')
export class JTip extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;
    @property(Label)
    contentLabel:Label | null;

    start () {
        // [3]
        let self = this;
        if(self.node.j_convey_data)
        {
            self.contentLabel.string =  self.node.j_convey_data;   
            self.scheduleOnce(()=>{
                self.node.removeFromParent();
            },2);    
        }else{
            self.node.removeFromParent();
        }

    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
