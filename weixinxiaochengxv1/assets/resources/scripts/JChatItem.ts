
import { _decorator, Node, Sprite, Label, SpriteFrame, UITransform, size, Overflow } from 'cc';
import { JComponent } from './common/JComponent';
import { getUrlSpriteFrame } from './common/JTools';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JChatItem
 * DateTime = Thu Dec 09 2021 20:18:55 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JChatItem.ts
 * FileBasenameNoExtension = JChatItem
 * URL = db://assets/resources/scripts/JChatItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JChatItem')
export class JChatItem extends JComponent {

    @property(Sprite)
    headSprite:Sprite | null = null;

    @property(Label)
    nicknameLabel:Label | null = null;

    @property(Label)
    msgLabel:Label | null = null;

    @property(Node)
    msgBK:Node | null = null;

    public type:number = 0;

    public my:boolean =false;

    start () {
        // [3]
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    public setData(data:{headUrl:string,name:string,msg:string})
    {
        let self = this;

        let hsf = JCCS.instance.HeadSpriteFrame[data.headUrl];

        if(hsf)
        {
            self.headSprite.spriteFrame = hsf;
        }else{
            getUrlSpriteFrame(data.headUrl,(sf:SpriteFrame)=>{
                self.headSprite.spriteFrame = sf;
            })            
        }


        
        self.nicknameLabel.string   = data.name;
        self.msgLabel.string        = data.msg;




        self.upW();

    }

    public upItemWidth(w:number)
    {
        let self = this;

        let uit = self.msgBK.getComponent(UITransform);

        uit.setContentSize(size(uit.contentSize.width+w,uit.contentSize.height));

        let uitl = self.msgLabel.node.getComponent(UITransform);

        uitl.setContentSize(size(uitl.contentSize.width+w,uitl.contentSize.height));

        if(w<0)
        {
            self.type = 1;
        }else{
            self.type = 0;
            
        }

        if(self.my)
        self.node.setPosition(self.node.getPosition().x+w,self.node.getPosition().y);

        self.upW();


    }


    private upW()
    {
        let self = this;
        self.msgLabel.overflow = Overflow.NONE;
        //900
        let uitl = self.msgLabel.getComponent(UITransform);

        self.scheduleOnce(()=>{

            if(self.type == 1  )
            {
                let uitbk = self.msgBK.getComponent(UITransform);                
                if(uitl.contentSize.width < 588)
                {
                    uitbk.setContentSize(size(uitl.contentSize.width+40,uitl.contentSize.height) );  
                }else{
                    self.msgLabel.overflow = Overflow.RESIZE_HEIGHT;
                    uitl.setContentSize(size(588,uitl.contentSize.height) ); 
                    uitbk.setContentSize(size(588+40,uitl.contentSize.height) );  
                }
                
            }else{
                let uitbk = self.msgBK.getComponent(UITransform);
                uitbk.setContentSize(size(uitl.contentSize.width+40,uitl.contentSize.height) );     
            }

        },0.01)

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
