
import { _decorator, Component, Node, AudioClip, AudioSource } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JAudioManager
 * DateTime = Thu Dec 23 2021 15:56:31 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JAudioManager.ts
 * FileBasenameNoExtension = JAudioManager
 * URL = db://assets/resources/scripts/JAudioManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JAudioManager')
export class JAudioManager extends Component {

    private _mainBGM:AudioSource    = new AudioSource();

    private _combatBGM:AudioSource  = new AudioSource();

    private _button:AudioSource     = new AudioSource();

    private _typing:AudioSource     = new AudioSource();

    start () {
        // [3]

        let self = this;

        JCCS.instance.audioManager = self;

        self._mainBGM.clip   = null;
        self._combatBGM.clip = null;
        self._button.clip    = null;
        self._typing.clip    = null;

        self._mainBGM.node = new Node();
        self._combatBGM.node = new Node();
        self._button.node = new Node();
        self._typing.node = new Node();

        JCCS.instance.LoadManage.resourcesLoad_audio('bgm/mainBGM',(res:AudioClip)=>{
            self._mainBGM.clip = res;
            if(!JCCS.instance.audio_off)
            {
                self._mainBGM.play();
                self._mainBGM.loop = true;                
            }
            JCCS.instance.LoadManage.resourcesLoad_audio('bgm/combatBGM',(res:AudioClip)=>{
                self._combatBGM.clip = res;
            })
            JCCS.instance.LoadManage.resourcesLoad_audio('sound/button',(res:AudioClip)=>{
                self._button.clip = res;
            })

            JCCS.instance.LoadManage.resourcesLoad_audio('sound/aigei_com2',(res:AudioClip)=>{
                self._typing.clip = res;
                console.log('打字音效加载完成');
            })

        })

    }

    /**切换背景音乐*/
    public playBGM(name:string = 'main')
    {
        let self = this;

        self._mainBGM.stop();
        self._combatBGM.stop();

        if(JCCS.instance.audio_off)
        {
            return;
        }

        console.log('播放:'+name);
        if(name == 'main')
        {
            self._mainBGM.play();
            self._mainBGM.loop = true;
        }else{
            self._combatBGM.play();
            self._combatBGM.loop = true;
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    /**播放按钮音效*/
    public playSound(name:string = '')
    {
        let self = this;
        // if(JCCS.instance.audio_off)
        // {
        //     return;
        // }
        console.log('播放音效:'+name);
        if(name == 'typing')
        {
            if(self._typing.clip)
            self._typing.play();   
        }else{
            if(self._button.clip)
            self._button.play();            
        }

    }

    public stopAll()
    {
        let self = this;
        self._mainBGM.stop();
        self._combatBGM.stop();
    }
    
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
