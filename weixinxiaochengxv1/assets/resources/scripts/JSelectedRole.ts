
import { _decorator, Component, Node, SpriteFrame, Input, EventTouch, Sprite, UIOpacity, tween, Label, v3 } from 'cc';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { showTip } from './common/JTools';
import { JCCS } from './JCC';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JSelectedRole
 * DateTime = Wed Jan 12 2022 14:23:07 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JSelectedRole.ts
 * FileBasenameNoExtension = JSelectedRole
 * URL = db://assets/resources/scripts/JSelectedRole.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JSelectedRole')
export class JSelectedRole extends Component {

    /**头像集合*/
    @property(Node)
    heads:Node;

    /**角色名称上面的 头像*/
    @property(Sprite)
    roleName_head_s:Sprite;

    @property(SpriteFrame)
    head_on_s:SpriteFrame[] =[];

    @property(SpriteFrame)
    head_on1_s:SpriteFrame[] =[];

    @property(SpriteFrame)
    head_off_s:SpriteFrame[]=[];

    @property(Sprite)
    roleImg:Sprite;

    @property(SpriteFrame)
    roleImgs:SpriteFrame[]=[];

    @property(Sprite)
    chong:Sprite;

    @property(SpriteFrame)
    chong_sf:SpriteFrame[] = [];

    /**灵宠集合*/
    @property(Node)
    lingchongs:Node;

    @property(SpriteFrame)
    lingchong_on_s:SpriteFrame[] =[];

    @property(SpriteFrame)
    lingchong_off_s:SpriteFrame[]=[];

    /**职业介绍*/
    @property(Node)
    layoutIntroduce:Node;

    /**角色选择*/    
    @property(Node)
    layoutRoleSelect:Node;

    /**灵宠选择*/
    @property(Node)
    layoutLingChongSelect:Node;

    /**角色名称*/
    @property(Node)
    jueseName:Node;

    /**灵宠名称*/
    @property(Node)
    lingChongName:Node;

    /**角色选择*/
    @property(Node)
    buttonRoleSelect:Node;

    /**下一步*/
    @property(Node)
    buttonNext:Node;


    /**主背景 地图显示的时候 需要隐藏*/
    @property(Node)
    mainBK:Node;

    /**选角色页面关闭*/
    @property(Node)
    buttonBack:Node;

    /**关闭提示动画*/
    @property(UIOpacity)
    hideTip:UIOpacity;

    /**关闭页面*/
    @property(Node)
    buttonHide:Node;

    /**充值页面*/
    @property(Node)
    topUpPage:Node;

    @property(Label)
    roleName:Label;

    @property(Label)
    roleData:Label;

    @property(Node)
    wutai:Node;

    @property(Sprite)
    lingChong_wancheng:Sprite;

    @property(SpriteFrame)
    lingChong_wancheng_sf:SpriteFrame[] = [];

    @property(Label)
    lingChong_wancheng_name:Label;

    /**红包折扣Node*/
    @property(Node)
    hbzkNode:Node;

    private _tiaoguoStart:boolean = false;

    /**阶段*/
    private _stage:number = 0;

    /**按钮是否可点*/
    private _isTouch:boolean = true;

    private _roleNames:string[] =['地质学家','游历书生','诗者','释者','博物学家','仁心医者','天地道者'];

    private _roleData:string[] =[
                                '东太行的地质学家，可为您解说地质构造、山川形成，带您走上地质探索之旅。',
                                '游历至此的学者，可以为您介绍太行相承之文脉，介绍景点故事与文化传袭。',
                                '流连于山水之间的诗者，感叹大自然之奇美壮阔，时不时给你吟诗作句，写尽山河激荡。',
                                '常年驻守在东太行山脉的行者僧，跋涉修行的过程中，带您聆听梵者之音，立山川而阅众生。',
                                '万物皆其爱，也能讲出所见之物的前世今生，如果你想了解更多的见闻之外的知识，她是一个好的选择。',
                                '仁爱的医者，时刻关注您身体的各种动向，为您提供更科学的徒步建议。',    
                                '人法地、地法天、天法道、道法自然，长期驻扎东太行而观天地之盛景的道者，能为您揭开东太行天地之谜。',
                            ]

    start () {
        // [3]
        let self = this;

        self.topUpPage.active = false;

        tween(self.hideTip)
        .delay(0.5)
        .to(1,{opacity:50})
        .to(1,{opacity:255})
        .union()
        .repeatForever()
        .start()   

        self.buttonHide.on(Input.EventType.TOUCH_END,()=>{
            self.node.active = false;
        })        
        if(self._tiaoguoStart){
            
            
            
            return;
        }
        self.lingChongName.active = false;
        console.log('选择角色页面');
        self.buttonBack.active = false;
        self.mainBK.active = true;

        self.hbzkNode.active = false;

        if(!JCCS.instance.isTopUp)
        {
            self.topUpPage.active = true;

            // JCCS.instance.SendServer.useTicket('12345678987654321',(d:any)=>{
            //     console.log('使用门票反馈'+d);
            // })

        }

        let ar = self.heads.children;

        JCCS.instance.RoleIndex = 1;
        JCCS.instance.LingChong = 1;

        for(let i = 0 ; i < ar.length;i++)
        {
            let a = ar[i];
            
            let s = a.getComponent(Sprite);

            if(i==0)
            {
                a.j_value = true;
                s.spriteFrame = self.head_on_s[i];
                self.roleImg.spriteFrame = self.roleImgs[i];
                self.roleImg.sizeMode    = Sprite.SizeMode.RAW;
                self.roleImg.trim        = false;
            }else{
                a.j_value = false;
                s.spriteFrame = self.head_off_s[i];
            }

            self.roleName.string = self._roleNames[i];
            self.roleData.string = self._roleData[i];

            a.on(Input.EventType.TOUCH_END,(event:EventTouch)=>{
                if(a.j_value == false)
                {
                    a.j_value = true;
                }
                JCCS.instance.audioManager.playSound();
                self.roleName.string = self._roleNames[i];
                self.roleData.string = self._roleData[i];

                if(a.j_value)
                {
                    s.spriteFrame = self.head_on_s[i];
                    self.roleImg.spriteFrame = self.roleImgs[i];

                    JCCS.instance.RoleIndex = i+1;

                    for(let k = 0 ; k < ar.length;k++)
                    {
                        let ss = ar[k].getComponent(Sprite);
                        if(i!=k)
                        {
                            ss.spriteFrame = self.head_off_s[k];
                        }
                    }                    
                }

            })

        }


        let br = self.lingchongs.children;
        for(let i = 0 ; i < br.length;i++)
        {
            let a = br[i];
            
            let s = a.getComponent(Sprite);

            if(i==0)
            {
                a.j_value = true;
                s.spriteFrame = self.lingchong_on_s[i];
                self.chong.spriteFrame = self.chong_sf[i];
            }else{
                a.j_value = false;
                s.spriteFrame = self.lingchong_off_s[i];

            }
            
            a.on(Input.EventType.TOUCH_END,(event:EventTouch)=>{
                if(a.j_value == false)
                {
                    a.j_value = true;
                }
                JCCS.instance.audioManager.playSound();
                if(a.j_value)
                {
                    s.spriteFrame = self.lingchong_on_s[i];

                    self.chong.spriteFrame = self.chong_sf[i];
                    JCCS.instance.LingChong = i+1;
                    for(let k = 0 ; k < br.length;k++)
                    {
                        let ss = br[k].getComponent(Sprite);
                        if(i!=k)
                        {
                            ss.spriteFrame = self.lingchong_off_s[k];
                            
                        }
                    }                    
                }

            })

        }


        self.upPage()
    }

    /**更新页面*/
    private upPage(animation:boolean = false)
    {
        let self = this;
        if(animation)
        {
            self.jueseName.active             = true;
            self.layoutRoleSelect.active      = true;
            self.layoutLingChongSelect.active = true;
            self.buttonRoleSelect.active      = true;
            self.buttonNext      .active      = true;
            self.layoutIntroduce.active       = true;
            self.roleName_head_s.spriteFrame  = self.head_on1_s[JCCS.instance.RoleIndex-1];

            let uios:UIOpacity[] = [];

            let uio1        = self.jueseName.getComponent(UIOpacity);
            uio1.opacity    = 0;
            let uio2        = self.layoutRoleSelect.getComponent(UIOpacity);
            uio2.opacity    = 0;
            let uio3        = self.layoutLingChongSelect.getComponent(UIOpacity);
            uio3.opacity    = 0;
            let uio4        = self.buttonRoleSelect.getComponent(UIOpacity);
            uio4.opacity    = 0;
            let uio5        = self.buttonNext.getComponent(UIOpacity);
            uio5.opacity    = 0;
            let uio6        = self.layoutIntroduce.getComponent(UIOpacity);
            uio6.opacity    = 0;

            uios.push(uio1);
            uios.push(uio2);
            uios.push(uio3);
            uios.push(uio4);
            uios.push(uio5);
            uios.push(uio6);

            switch(self._stage)
            {
                case 0:
                    uio1.opacity    = 255;
                    uio2.opacity    = 0;
                    uio3.opacity    = 255;
                    uio4.opacity    = 0;
                    uio5.opacity    = 255;
                    uio6.opacity    = 0;
                    break;
                case 1:
                    uio1.opacity    = 0;
                    uio2.opacity    = 255;
                    uio3.opacity    = 0;
                    uio4.opacity    = 255;
                    uio5.opacity    = 0;
                    uio6.opacity    = 255;           
                    break;
            }   

            self._isTouch = false;
            for(let i = 0 ; i < uios.length;i++)
            {
                let ui = uios[i];
                let toValue = 0;
                if(ui.opacity == 0)
                {
                    toValue = 255;
                }

                tween(ui)
                .to(0.2,{opacity:toValue})
                .call(()=>{
                    if(i==uios.length-1)
                    {
                        self._isTouch = true;
                    }
                    switch(self._stage)
                    {
                        case 0:
                            self.jueseName.active             = false;
                            self.layoutRoleSelect.active      = true;
                            self.layoutLingChongSelect.active = false;
                            self.chong.node.active = false;
                            self.buttonRoleSelect.active      = true;
                            self.buttonNext      .active      = false;
                            self.layoutIntroduce.active       = true;
                            break;
                        case 1:
                            self.jueseName.active             = true;
                            self.layoutRoleSelect.active      = false;
                            self.layoutLingChongSelect.active = true;
                            self.chong.node.active = true;
                            self.buttonRoleSelect.active      = false;
                            self.buttonNext      .active      = true;  
                            self.layoutIntroduce.active       = false;              
                            break;
                    }  
                })
                .start()

            }

            


        }else{
            switch(self._stage)
            {
                case 0:
                    self.jueseName.active             = false;
                    self.layoutRoleSelect.active      = true;
                    self.layoutLingChongSelect.active = false;
                    self.chong.node.active = false;
                    self.buttonRoleSelect.active      = true;
                    self.buttonNext      .active      = false;
                    self.layoutIntroduce.active       = true;
                    break;
                case 1:
                    self.jueseName.active             = true;
                    self.layoutRoleSelect.active      = false;
                    self.layoutLingChongSelect.active = true;
                    self.chong.node.active = true;
                    self.buttonRoleSelect.active      = false;
                    self.buttonNext      .active      = true;  
                    self.layoutIntroduce.active       = false;              
                    break;
            }            
        }


    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    private onRoleSelect()
    {
        let self = this;
        if(!self._isTouch)return;
        JCCS.instance.audioManager.playSound();
        self._stage = 1;
        self.upPage(true)
    }

    private onNext()
    {
        let self = this;
        if(!self._isTouch)return;
        JCCS.instance.audioManager.playSound();
        self._stage = 0;
        self.upPage(true)
    }

    private onGoToMap() {
        let self = this;

        JCCS.instance.audioManager.playSound();

        JCCS.instance.SendServer.setGuide();
        JCCS.instance.SendServer.setLingChong((data:any)=>{
            let obj = JSON.parse(data);
            if(obj.result)
            {

            }else{
                JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.MAP});                     
            }
        })

        JCCS.instance.SendServer.setRole((data:any)=>{

        })


    }

    private showHBJK()
    {
        let self = this;

        let uio = self.hbzkNode.getComponent(UIOpacity);
        uio.opacity = 0;
        self.hbzkNode.active = true;
        self.hbzkNode.scale = v3(0.1,0.1,1);

        tween(uio)
        .to(0.2,{opacity:255})
        .start()

        tween(self.hbzkNode)
        .to(0.2,{scale:v3(1,1,1)})
        .start()

    }

    private onLQHBJK()
    {
        let self = this;

        let uio = self.hbzkNode.getComponent(UIOpacity);

        tween(uio)
        .to(0.2,{opacity:0})
        .call(()=>{

            if(JCCS.instance.deBug)
            {
                JCCS.instance.SendServer.useTicket('12345678987654321',(d:any)=>{
                    console.log('使用门票反馈'+d);
                    let od = JSON.parse(d);
                    if(od.result)
                    {
                        let str = '';
                        switch(od.result)
                        {
                            case 1:
                                str = '已经有门票';
                                break;
                            case 2:
                                str = '门票不存在';
                                break;
                            case 3:
                                str = '门票已经使用';
                                break;
                        }
                        if(str != '')
                        {
                            showTip(JCCS.instance.MainNode,str)                                    
                        }
    
                    }else{
                        self.topUpPage.active = false;  
                        showTip(JCCS.instance.MainNode,'购买成功');                                                        
                    }
    
                })
            }

        })
        .start()

    }

    public showRoleData()
    {
        let self = this;
        self._tiaoguoStart = true;
        console.log('只显示角色');

        self.wutai.setPosition(0,self.wutai.getPosition().y);

        self.buttonBack.active          = true;
        self.mainBK.active              = false;
        self.buttonRoleSelect.active    = false;
        self.layoutRoleSelect.active    = false;
        self.jueseName.active           = true;

        

        self.layoutLingChongSelect.active = false;
        self.lingChongName.active         = true;

        self.jueseName.getChildByName('Button').active = false;
        self.lingChongName.getChildByName('Button').active = false;

        self.lingChong_wancheng.spriteFrame = self.lingChong_wancheng_sf[JCCS.instance.LingChong-1];
        let name = '';
        switch(JCCS.instance.LingChong)
        {
            case 1:
                name = '灵狐';
                break;
            case 2:
                name = '战狼';
                break;
            case 3:
                name = '仙鹿';
                break;
        }
        self.lingChong_wancheng_name.string = name;

        self.roleName.string = self._roleNames[JCCS.instance.RoleIndex-1];
        self.roleData.string = self._roleData[JCCS.instance.RoleIndex-1];

        self.chong.spriteFrame = self.chong_sf[JCCS.instance.LingChong-1];

        self.roleImg.spriteFrame = self.roleImgs[JCCS.instance.RoleIndex-1];

        self.roleName_head_s.spriteFrame = self.head_on1_s[JCCS.instance.RoleIndex-1];

    }


    private onHidePage()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.node.active = false;
    }


    private onHideTopUpPage()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();

        self.showHBJK();


    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
