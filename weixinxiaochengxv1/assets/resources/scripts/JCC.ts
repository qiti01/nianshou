import { color, director, Node } from "cc";
import { JClass } from "./common/JClass";
import { JLoadManage } from "./common/JLoadManage";
import { caozuoType, JSendServer } from "./common/JSendServer";
import { addDanChu, HideLoading, showLoading, showTip } from "./common/JTools";
import { JAudioManager } from "./JAudioManager";
import { JMain } from "./JMain";
import { PAGE_NAME } from "./JResource";
import { MapLayout } from "./mapLayout";


/**平台类型*/
export enum JPlatformType{
    /**微信手机浏览器*/
    wx_mobile=0,
    /**微信本地开发*/
    wx_localhost,
    /**本地开发浏览器*/
    web_localhost,
    /**手机浏览器*/
    web_mobile,
}

/**控制台*/
export class JCCS extends JClass {

    private static _instance: JCCS;
    
    public static get instance(): JCCS {
        if (!JCCS._instance) {
            JCCS._instance = new JCCS();
            JCCS._instance.init();
        }
        return JCCS._instance;
    }
    /**颜色集合*/
    public Colors = {
        minbk:color(126,81,55),
        title:color(255,245,197),
        title_line: color(183,87,34),
        green_line: color(35,124,15),
        yellow_line:color(185,111,3),
        blue_line:  color(20,107,131),
    };
    public deBug:boolean = true;
    
    /**资源加载管理*/
    public LoadManage:JLoadManage = new JLoadManage();
    /**请求服务器*/
    public SendServer:JSendServer = new JSendServer();

    /**打开DI*/
    public dakaID:number= 0;

    /**拥有道具列表*/
    public Items:number[] = [];

    public MainNode:Node | null     = null;
    /**主场景*/
    public MainS:JMain | null       = null;
    /**灵宠ID*/
    public LingChong:number         = -1;
    /**引导标记*/
    public GuiDe:number             = -1;
    /**抽奖次数*/
    public ChouJiangCiShu:number    = 0;
    /**抽奖记录*/
    public ChouJiangJiLu:any        = [];
    /**红包列表*/
    public hongBaoList:any          = [];
    /**已送走年兽*/
    public SongZouNianShou:number[] = [];

    /**玩家头像集合*/
    public HeadSpriteFrame:any      = {};

    public MyUID:number             = 0;

    public myHeadUrl:string         = '';

    public myName:string            = '';

    /**音频管理*/
    public audioManager:JAudioManager | null = null;

    public audio_off:boolean = false;

    /**角色索引*/
    public RoleIndex:number  = 0;

    /**景点名称集合*/
    public SpotNames:string[] = ['索道上站','南天门','四方桥','回音谷','我心永恒号','窗外的世界','石的故事','中天门',
                                 '拜象台','天门守山','果然桥','太行一柱','白的约定','核桃谷','红岩峰','挹芳亭',
                                 '古浪海勇','刘伶醒酒台']
    /**景点数据集合*/
    public SpotDatas:any = {};

    /**地点集合*/
    public Spots = [
                        {lat:36.946121,lon:113.913727,name:"索道上站"},
                        {lat:36.946003,lon:113.912941,name:"南天门"},
                        {lat:36.947308,lon:113.912643,name:"四方桥"},
                        {lat:36.949192,lon:113.914307,name:"我心永恒号"},
                        {lat:36.949448,lon:113.913628,name:"窗外的世界"},
                        {lat:36.952869,lon:113.911255,name:"爱之巢"},
                        {lat:36.953064,lon:113.910912,name:"石的故事"},
                        {lat:36.954025,lon:113.912735,name:"中天门"},
                        {lat:36.955139,lon:113.91436,name:"避雨亭"},
                        {lat:36.955372,lon:113.914757,name:"拜象台"},
                        {lat:36.956799,lon:113.914101,name:"天门守山"},
                        {lat:36.957504,lon:113.913223,name:"四首白云"},
                        {lat:36.958294,lon:113.912712,name:"垂直沟缝"},
                        {lat:36.959042,lon:113.912369,name:"果然桥"},
                        {lat:36.961300,lon:113.911186,name:"太行一柱"},
                        {lat:36.954266,lon:113.915894,name:"白的约定"},
                        {lat:36.952751,lon:113.915955,name:"核桃谷"},
                        {lat:36.952335,lon:113.917618,name:"红岩峰"},
                        {lat:36.952335,lon:113.917618,name:"挹芳亭"},
                        {lat:36.947567,lon:113.917046,name:"穿洞"},
                        {lat:36.946888,lon:113.916603,name:"古海浪勇"}
        ];

    /**我的坐标*/
    public mySpot = {Longitude:-1,Latitude:-1};

    /**游戏平台 0 其他 1 微信 2 微信开发工具 3 本地开发工具*/
    public Platform = 0;

    /**是否充值*/
    public isTopUp:boolean = false;

    public LoadingPage:any = null;

    public JiFen:any = null;

    /**我的地址*/
    public myLocation:{lat:number,lng:number}={lat:0,lng:0};

    /**打卡ID*/
    public checkinID = 0;

    /**地图Node*/
    public mapNode:MapLayout | null = null;

    /**初始化*/
    private init(){

        let sw = localStorage.getItem('audio');

        let self = this;

        


        if(Number(sw))
        {
            self.audio_off = true;
        }


        self.LoadingPage = director.getScene().getChildByName('Canvas').getChildByName('LoadingPage');

        

        // let did:any = localStorage.getItem('dakaid');
        // console.log(did);

        // if(did === undefined || did === null)
        // {
        //     did = -1;
        //     console.log('没有打卡数据')
        // }

        // if(!did)
        // {
        //     if(did!==0)
        //     {
        //         did =-1;
        //     }
        // }

        // self.dakaID = Number(did);

        // console.log('打开ID：'+self.dakaID);

        console.log('运行平台')
        console.log(window.location.href);

        if(window.location.href == 'game.js')
        {
            console.log('微信小游戏手机运行');
            self.Platform = JPlatformType.wx_mobile;
        }else{
            if(window.location.href.indexOf('gameContext')>=0)
            {
                console.log('微信开发工具');
                self.Platform = JPlatformType.wx_localhost;
            }else{
                if(window.location.href.indexOf('localhost')>=0)
                {
                    console.log('本地开发浏览器');
                    self.Platform = JPlatformType.web_localhost;
                }else{
                    console.log('h5浏览器');
                    self.Platform = JPlatformType.web_mobile;     
                }
            }
        }

        //self.Platform = JPlatformType.web_localhost;


        window['LocationBackCall'] = (res:any)=>{
            console.log(res);
            if(res && res.lat && res.lng)
            {
                self.myLocation.lat = res.lat;
                self.myLocation.lng = res.lng;
            }
        }
        window['qqMapInit']();   

        window['QRCodeBackCall'] = self.QRCodeBackCall.bind(self);

    }

    private QRCodeBackCall(data:any)
    {
        let self = this;


    }

    public upMySpot()
    {
        let self = this;

        showLoading(9999);

        window['qqGetCurLocation'](); 

        setTimeout(()=>{
            if(self.myLocation.lat && self.myLocation.lng)
            {
                for(let i = 0 ; i < self.Spots.length;i++)
                {
                    let spot = self.Spots[i];
                    if(spot.lat+0.0001 > self.myLocation.lat && spot.lat-0.0001 < self.myLocation.lat && 
                        spot.lon+0.0001 > self.myLocation.lng && spot.lon-0.0001 < self.myLocation.lng)
                    {
                        self.checkinID = i+1;
                        break;  
                    }
                }
            }
            this.checkin();
            HideLoading(9999);
        },5000)
    }

    /**打卡*/
    public checkin()
    {
        let self = this;
        console.log('打卡')
        console.log('self.checkinID:'+self.checkinID);
        //self.dakaID = Number( localStorage.getItem('dakaid') );
        if(!self.dakaID)
        {
            if(self.dakaID!==0)
            {
                self.dakaID =0;
            }
        }

        console.log('self.dakaID:'+self.dakaID);
        if(self.dakaID==20)
        {
            JCCS.instance.SendServer.sendCaozuo( caozuoType.all_complete );
        }
        if(self.checkinID)
        {
            if(self.checkinID > self.dakaID)
            {
                JCCS.instance.SendServer.checkin((self.dakaID+1).toString(),(data:any)=>{
                    let obj = JSON.parse(data);
                    if(obj.result)
                    {
                        let str = '';
                        switch(Number(obj.result))
                        {
                            case 1:
                                str = '已经获取过得地点';
                                break;
                            case 2:
                                break;
                        }
                        if(str != '')
                        {
                            //showTip(self.mapNode.node,str);                        
                        }
                        self.dakaID++;
                        //localStorage.setItem('dakaid',JCCS.instance.dakaID.toString());
                        self.mapNode.upSpotUI();
                        self.checkin();
                        
                    }else{
                        if(obj.item)
                        {
                            let nianID = Number(obj.item);
                            if(nianID<10)
                            {
                                //显示年兽获得页面
                                addDanChu(self.mapNode,PAGE_NAME.GET_SHOU,nianID)
                                self.mapNode.nianshous[nianID-1].spriteFrame = self.mapNode.longzisfs[nianID-1];
                            }else{
                                if(nianID>100)
                                {
                                    addDanChu(self.mapNode,PAGE_NAME.GET_DATI,nianID)
                                }else{
                                    //显示道具获得页面
                                    addDanChu(self.mapNode,PAGE_NAME.GET_ITEM,nianID)     
                                        
                                    self.mapNode.upLongZi(); 

                                }
                            }                        
                            JCCS.instance.Items.push(nianID);
                        }else{
                            showTip(JCCS.instance.MainNode,'很遗憾没有获得任何道具。');
                        }
    
                    }
                })                  
                
            }
        }
    }


}

