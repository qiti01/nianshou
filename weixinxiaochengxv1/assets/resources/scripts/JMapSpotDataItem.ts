
import { _decorator, Component, Node, Label } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JMapSpotDataItem
 * DateTime = Tue Dec 28 2021 00:44:35 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JMapSpotDataItem.ts
 * FileBasenameNoExtension = JMapSpotDataItem
 * URL = db://assets/resources/scripts/JMapSpotDataItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JMapSpotDataItem')
export class JMapSpotDataItem extends Component {

    /**地点名称*/
    @property(Label)
    lName:Label;

    /**经度*/
    @property(Label)
    lLongitude:Label;

    /**维度*/
    @property(Label)
    lLatitude:Label;

    start () {
        // [3]
    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    public upData(data:any)
    {
        let self = this;

        self.lName.string = data.name;
        self.lLongitude.string = data.Longitude;
        self.lLatitude.string  = data.Latitude;

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
