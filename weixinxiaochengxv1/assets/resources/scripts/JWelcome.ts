
import { _decorator, Node, Sprite, UITransform, view } from 'cc';
import { JComponent } from './common/JComponent';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { caozuoType } from './common/JSendServer';
import { showTip } from './common/JTools';
import { JCCS, JPlatformType } from './JCC';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JWelcome
 * DateTime = Mon Dec 06 2021 17:28:19 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JWelcome.ts
 * FileBasenameNoExtension = JWelcome
 * URL = db://assets/resources/scripts/JWelcome.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JWelcome')
export class JWelcome extends JComponent {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    testhead:Sprite | null = null;

    @property(Node)
    setSpotPage:Node;

    @property(Node)
    sendMsgTest:Node;

    private _spotName:string = '';

    onLoad(){
        let self = this;
        self.node.j_showTime = 0;

    }
    start () {
        // [3]

        let self = this;

        self.sendMsgTest.active = false;
        self.setSpotPage.active = false;


    }

    private onShowSendMsgTest()
    {
        let self = this;

        self.sendMsgTest.active = true;

    }


    // update (deltaTime: number) {
    //     // [4]
    // }

    /**开始*/
    private onStart()
    {
        let self = this;
        console.log('开启春节剧本');
        self.onLaunchFullScreen();
        JCCS.instance.audioManager.playSound('button');

        switch(JCCS.instance.Platform)
        {
            case JPlatformType.web_localhost:
                {
                    JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.GUIDE});
                }
                break;
            case JPlatformType.web_mobile:
                {
                    if(JCCS.instance.myName && JCCS.instance.myHeadUrl && JCCS.instance.SendServer.tou && JCCS.instance.MyUID)
                    {
                        JCCS.instance.SendServer.sendCaozuo(caozuoType.start);
                        let backCall = 0;
                        let toPage = ()=>{
                            backCall++;
    
                            if(backCall == 4)
                            {
                                let index = 1;
    
                                console.log('灵宠：'+JCCS.instance.LingChong);
                                console.log('引导：'+JCCS.instance.GuiDe);
                                console.log('角色：'+JCCS.instance.RoleIndex);
    
                                if(JCCS.instance.LingChong > 0 && JCCS.instance.GuiDe > 0 && JCCS.instance.RoleIndex > 0)
                                {
                                    index = 2;
                                }
    
                                switch(index)
                                {
                                    case 1://跳转到引导
                                        JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.GUIDE});
                                        break;
                                    case 2://跳转到地图
                                        JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.MAP});
                                        break;
                                }                            
                            }
    
                        }


                        JCCS.instance.SendServer.getLingChong((res:any)=>{
                            res = JSON.parse(res);
                            console.log('灵宠数据返回:');
                            if(!res.result)
                            {
                                if(!res.id)res.id = 0;
                                JCCS.instance.LingChong = Number( res.id );

                                toPage();
                            }else{
                                if(res.result == 8)
                                {
                                    JCCS.instance.LingChong = -1;
                                    toPage();
                                }else{
                                    showTip(JCCS.instance.MainNode,'已选择灵宠数据错误请重新登录')                                        
                                }
                            }
                        })

                        JCCS.instance.SendServer.getRole((res:any)=>{
                            res = JSON.parse(res);
                            console.log('人物数据返回:');
                            if(!res.result)
                            {
                                if(!res.id)res.id = 0;
                                JCCS.instance.RoleIndex = Number( res.id );
                                toPage();
                            }else{
                                if(res.result == 8)
                                {
                                    JCCS.instance.RoleIndex = -1;
                                    toPage();
                                }else{
                                    showTip(JCCS.instance.MainNode,'已选择角色数据返回错误请重新登录')
                                }
                            }
                        })

                        JCCS.instance.SendServer.isGuide((re:any)=>{
                            console.log('引导数据回调')
                            let r= JSON.parse(re);
                            console.log(r);

                            if(r && r.finished)
                            {
                                JCCS.instance.GuiDe = 1;
                            }else{
                                JCCS.instance.GuiDe = 0;
                            }
                            toPage();
                        })
                        JCCS.instance.SendServer.getComposed((data:any)=>{
                            let da = JSON.parse(data);
                            console.log('已送走年兽回调:'+data);
                            if(da.monsters)
                            {
                                JCCS.instance.SongZouNianShou = da.monsters;
                            }
                            console.log('已送走年兽：'+JSON.stringify(JCCS.instance.SongZouNianShou));
                        })
                        JCCS.instance.SendServer.getItems(()=>{
                            //JCCS.instance.Items;
                            toPage();
                        });
                        JCCS.instance.ChouJiangJiLu = [];
                        JCCS.instance.SendServer.LuckyDraw_record((res:any)=>{
                            if(!res)
                            {
                                
                            }else{
                                let re = JSON.parse(res);
                                if(re.result)
                                {

                                }else{
                                    console.log('服务器返回的抽奖记录：'+res);
                                    if(re.award)
                                    {
                                        JCCS.instance.ChouJiangJiLu = re.award;
                                    }
                                }                            
                            }

                        });
                    }
                }
                break;
            case JPlatformType.wx_mobile:
            case JPlatformType.wx_localhost:
                {

                    let backCall = 0;

                    let toPage = ()=>{
                        backCall++;

                        if(backCall == 4)
                        {
                            let index = 1;

                            console.log('灵宠：'+JCCS.instance.LingChong);
                            console.log('引导：'+JCCS.instance.GuiDe);
                            console.log('角色：'+JCCS.instance.RoleIndex);

                            if(JCCS.instance.LingChong > 0 && JCCS.instance.GuiDe > 0 && JCCS.instance.RoleIndex > 0)
                            {
                                index = 2;
                            }

                            switch(index)
                            {
                                case 1://跳转到引导
                                    JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.GUIDE});
                                    break;
                                case 2://跳转到地图
                                    JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.MAP});
                                    break;
                            }                            
                        }

                    }


                    window['wxGetSetting']((obj:any)=>{
                        console.log('微信授权');
                        console.log(obj);
                        JCCS.instance.myName    = obj.nickName;
                        JCCS.instance.myHeadUrl = obj.avatar;
                        // getUrlSpriteFrame(obj.avatar,(sf:SpriteFrame)=>{
                        // })                
                        JCCS.instance.SendServer.sendLogin(obj,(data:any)=>{
                            
                            let s = JSON.parse(data);
                            console.log('登录回调')
                            JCCS.instance.MyUID = s.uid;
                            JCCS.instance.SendServer.tou = s.session;

                            JCCS.instance.SendServer.getLingChong((res:any)=>{
                                res = JSON.parse(res);
                                console.log('灵宠数据返回:');
                                if(!res.result)
                                {
                                    if(!res.id)res.id = 0;
                                    JCCS.instance.LingChong = Number( res.id );

                                    toPage();
                                }else{
                                    if(res.result == 8)
                                    {
                                        JCCS.instance.LingChong = -1;
                                        toPage();
                                    }else{
                                        showTip(JCCS.instance.MainNode,'已选择灵宠数据错误请重新登录')                                        
                                    }

                                }

                            })

                            JCCS.instance.SendServer.getRole((res:any)=>{
                                res = JSON.parse(res);
                                console.log('人物数据返回:');
                                if(!res.result)
                                {
                                    if(!res.id)res.id = 0;
                                    JCCS.instance.RoleIndex = Number( res.id );
                                    toPage();
                                }else{
                                    if(res.result == 8)
                                    {
                                        JCCS.instance.RoleIndex = -1;
                                        toPage();
                                    }else{
                                        showTip(JCCS.instance.MainNode,'已选择角色数据返回错误请重新登录')
                                    }
                                }
                            })

                            JCCS.instance.SendServer.isGuide((re:any)=>{
                                console.log('引导数据回调')
                                let r= JSON.parse(re);
                                console.log(r);

                                if(r && r.finished)
                                {
                                    JCCS.instance.GuiDe = 1;
                                }else{
                                    JCCS.instance.GuiDe = 0;
                                }
                                toPage();

                            })
                            JCCS.instance.SendServer.getComposed((data:any)=>{
                                let da = JSON.parse(data);
                                console.log('已送走年兽回调:'+data);
                                if(da.monsters)
                                {
                                    JCCS.instance.SongZouNianShou = da.monsters;
                                }
                                console.log('已送走年兽：'+JSON.stringify(JCCS.instance.SongZouNianShou));
                                JCCS.instance.dakaID += JCCS.instance.SongZouNianShou.length*4;
                            })
                            JCCS.instance.SendServer.getItems(()=>{
                                //JCCS.instance.Items;
                                JCCS.instance.dakaID += JCCS.instance.Items.length;
                                toPage();
                            });
                            JCCS.instance.ChouJiangJiLu = [];
                            JCCS.instance.SendServer.LuckyDraw_record((res:any)=>{
                                if(!res)
                                {
                                    
                                }else{
                                    let re = JSON.parse(res);
                                    if(re.result)
                                    {

                                    }else{
                                        console.log('服务器返回的抽奖记录：'+res);
                                        if(re.award)
                                        {
                                            JCCS.instance.ChouJiangJiLu = re.award;
                                        }
                                    }                            
                                }

                            });


                        })
                    });
                }
                break;
        }


    }


    private onShowSetSpot()
    {
        let self = this;
        console.log('记录坐标页面')
        self.setSpotPage.active = true;
    }

    private onShowGetSpot() {
        let self = this;
        console.log('查看记录坐标页面')

        JCCS.instance.SendServer.getPoint();
    }

    /**清理服务器数据*/
    private clearServerData(){

        // window['wxGetSetting']((obj:any)=>{
        //     console.log('微信授权');
        //     console.log(obj);
        //     JCCS.instance.myName    = obj.nickName;
        //     JCCS.instance.myHeadUrl = obj.avatar;
        //     // getUrlSpriteFrame(obj.avatar,(sf:SpriteFrame)=>{
        //     // })                
            // JCCS.instance.SendServer.sendLogin(obj,(data:any)=>{
            //     let s = JSON.parse(data);
            //     JCCS.instance.SendServer.tou = s.session;
                JCCS.instance.SendServer.userClear();                
           // });
        //});
    }

    private onLaunchFullScreen()
    {
        window['launchFullScreen']();
    }


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
