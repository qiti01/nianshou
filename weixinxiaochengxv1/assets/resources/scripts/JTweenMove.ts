
import { _decorator, Component, Node, tween, UITransform, v3 } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JTweenMoveTo
 * DateTime = Tue Dec 21 2021 17:08:18 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JTweenMoveTo.ts
 * FileBasenameNoExtension = JTweenMoveTo
 * URL = db://assets/resources/scripts/JTweenMoveTo.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JTweenMoveTo')
export class JTweenMoveTo extends Component {

    @property
    moveX:number = 0;
    
    @property
    moveTime:number = 0;

    start () {
        // [3]
        let self = this;
        
        let spot = self.node.getPosition();
        tween(self.node)
        .to(self.moveTime,{position:v3(spot.x+self.moveX,spot.y)})
        .to(self.moveTime,{position:v3(spot.x,spot.y)})
        .union()
        .repeatForever()
        .start();

    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
