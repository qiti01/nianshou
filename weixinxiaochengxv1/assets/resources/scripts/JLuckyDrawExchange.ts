
import { _decorator, Component, Node, Label, ProgressBar, v3, UITransform, Input, EventTouch, size, Vec3 } from 'cc';
import { showTip } from './common/JTools';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JLuckyDrawExchange
 * DateTime = Mon Jan 10 2022 23:09:52 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JLuckyDrawExchange.ts
 * FileBasenameNoExtension = JLuckyDrawExchange
 * URL = db://assets/resources/scripts/JLuckyDrawExchange.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JLuckyDrawExchange')
export class JLuckyDrawExchange extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    /**游戏积分*/
    @property(Label)
    credits:Label;

    /**兑换数量*/
    @property(Label)
    amount:Label;

    /**进度条*/
    @property(ProgressBar)
    pb:ProgressBar;

    /**进度条*/
    @property(UITransform)
    pb2:UITransform;

    /**进度条上的按钮*/
    @property(Node)
    bu:Node;

    @property(Node)
    touchLayout:Node;

    private iChangeValue:number = 0;

    private _changeValue_Max:number = 0;

    private _iAmount:number = 0;

    public upJiFen:Function | null = null;

    start () {
        // [3]
        let self = this;
        
        self._changeValue_Max = JCCS.instance.JiFen.balance;
        self.credits.string = '我的积分：'+self._changeValue_Max+'分'
        let iExchange = 8;
        self._iAmount   = Math.floor( self._changeValue_Max/iExchange );

        console.log('iAmout:'+self._iAmount);

        self.pb.progress   = 0;
        self.amount.string = '0';
        self.bu.setPosition(v3(0,0,1));
        
        let w = self.pb.node.getComponent(UITransform).getBoundingBox().width;
        
        let mw = w/self._iAmount; 

        let startX = 0;

        self.bu.on(Input.EventType.TOUCH_START,(event:EventTouch)=>{
            let spot = self.bu.getPosition();
            startX = spot.x;
        })

        self.bu.on(Input.EventType.TOUCH_MOVE,(event:EventTouch)=>{
            
            let spot = self.bu.getPosition();

            spot.x += event.touch.getUIDelta().x;
            //spot.y = event.touch.getUIDelta().y;

            if(spot.x < 0)
            {
                spot.x = 0;
            }

            if(spot.x > w)
            {
                spot.x = w;
            }

            self.pb2.setContentSize(size(spot.x,self.pb2.contentSize.height));

            self.bu.setPosition(spot);

        })

        let  upX=()=>{ 
            let spot = self.bu.getPosition();
            let ti = Math.floor( spot.x/mw);
            if(startX < spot.x)
            {
                ti = Math.ceil( spot.x/mw);
            }
            self.iChangeValue = ti;
            if(self.iChangeValue > self._iAmount)
            {
                self.iChangeValue = self._iAmount;
                self.pb.progress  = 1;
            }else{
                self.pb.progress = self.iChangeValue/self._iAmount;
            }

            self.amount.string = self.iChangeValue.toString();

            self.bu.setPosition(v3(self.pb2.getBoundingBox().width,0,1));
        }

        self.bu.on(Input.EventType.TOUCH_END,(event:EventTouch)=>{
            upX();

        })
        
        self.bu.on(Input.EventType.TOUCH_CANCEL,(event:EventTouch)=>{
            upX();

        })


        self.touchLayout.on(Input.EventType.TOUCH_START,(event:EventTouch)=>{
            let self = this;
            JCCS.instance.audioManager.playSound();
            console.log(event);

            console.log(self.touchLayout.getWorldPosition());
            console.log(event.touch.getUILocation());
            //console.log(event.touch.getLocation());

            let yuan = new Vec3();
            self.touchLayout.getComponent(UITransform).convertToNodeSpaceAR(v3(event.touch.getUILocationX(),event.touch.getUILocationY()),yuan);
            console.log(yuan);

            let spot = self.bu.getPosition();
            startX = spot.x;
            spot.x = yuan.x;
            self.bu.setPosition(spot);
            upX();
        })




        self.iChangeValue = self._iAmount;
        self.pb.progress  = 1;
        self.amount.string = self.iChangeValue.toString();
        self.bu.setPosition(v3(self.pb2.getBoundingBox().width,0,1));
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private addChangeValue()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.iChangeValue++;
        if(self.iChangeValue > self._iAmount)
        {
            self.iChangeValue = self._iAmount;
            self.pb.progress  = 1;
        }else{
            self.pb.progress = self.iChangeValue/self._iAmount;
        }

        self.amount.string = self.iChangeValue.toString();

        self.bu.setPosition(v3(self.pb2.getBoundingBox().width,0,1));
    }

    private minusChangeValue()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.iChangeValue--;
        if(self.iChangeValue < 0)
        {
            self.iChangeValue = 0;
            self.pb.progress  = 0;
        }else{
            self.pb.progress = self.iChangeValue/self._iAmount;
        }

        self.amount.string = self.iChangeValue.toString();

        self.bu.setPosition(v3(self.pb2.getBoundingBox().width,0,1));
    }


    private onShowExchange()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();

        if(!JCCS.instance.JiFen || !JCCS.instance.JiFen.balance)
        {
            showTip(JCCS.instance.MainNode,'无可用积分')
            return;
        }

        if(JCCS.instance.JiFen < 8)
        {
            showTip(JCCS.instance.MainNode,'8分开始可兑换')
            return;
        }

        self.start();

        self.node.active = true;
    }

    private onHideExchange()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();

        if(!self.iChangeValue)
        {
            self.node.active = false; 
        }else{
            JCCS.instance.SendServer.sendScore2Chance({score:self.iChangeValue*8,chance:0},(res:any)=>{
                res = JSON.parse(res);
                if(res.result)
                {
                    showTip(JCCS.instance.MainNode,'积分兑换失败')
                }else{
                    showTip(JCCS.instance.MainNode,'积分兑换成功')

                    self.node.active = false;  
                    //JCCS.instance.JiFen = res;{"score":13,"balance":2,"scoreChance":3,"monsterChance":1}	 score 获取到的积分，balance 剩余积分，scoreChance 积分换的抽奖机会，monsterChance 年兽抽奖机会
                    if(!JCCS.instance.JiFen)
                    {
                        JCCS.instance.JiFen = {};
                    }

                    if(!res.score)res.score = 0;
                    if(!res.balance)res.balance = 0;
                    if(!res.scoreChance)res.scoreChance = 0;
                    if(!res.monsterChance)res.monsterChance = 0;

                    JCCS.instance.JiFen = res;

                    console.log('准备修改积分')
                    if(self.upJiFen){
                        self.upJiFen();
                    }else{
                        console.error('回调接口为空')
                    }
                }
            })            
        }



    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
