
import { _decorator, Component,UITransform, size } from 'cc';
import { JEventCenter, JEventName } from './common/JEventCenter';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JNianShouMask
 * DateTime = Thu Jan 13 2022 22:22:54 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JNianShouMask.ts
 * FileBasenameNoExtension = JNianShouMask
 * URL = db://assets/resources/scripts/JNianShouMask.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JNianShouMask')
export class JNianShouMask extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property
    inum = 0;

    onEnable()
    {
        let self = this;
        JEventCenter.on(JEventName.upData,self.upData.bind(self),self.name);
    }
    onDisable(){
        let self = this;
        JEventCenter.off(JEventName.upData,self.name);
        
    }
    public upData(data:any)
    {
        let self = this;
        if(!data)return;
        if(data.upTypes){
            for(let i = 0 ; i < data.upTypes.length;i++)
            {
                switch(data.upTypes[i])
                {
                    case 'upNianShouMask':
                        {
                            
                        }
                        break;
                }
            }
        }
    }
    
    start () {
        // [3]

        let self = this;

        self.node.j_value = self.node.getComponent(UITransform).getBoundingBox().height;

        self.upHeight(0);
    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    public upHeight(num:number)
    {
        let self = this;

        let tui = self.node.getComponent(UITransform);
        if(num==0)
        {
            tui.setContentSize(size(tui.getBoundingBox().width,0)); 
        }else{
            tui.setContentSize(size(tui.getBoundingBox().width,self.node.j_value*(num/4)));            
        }
        // console.log(self.node.j_value);
        // console.log('num:'+num);
        // console.log(tui.contentSize);

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
