
import { _decorator, Component, Node, Toggle, Label, EventTouch, Input } from 'cc';
import { showTip } from './common/JTools';
import { JCCS, JPlatformType } from './JCC';
import { JLuckyDrawExchange } from './JLuckyDrawExchange';
import { JJiangLiList } from './LuckyDraw/JJiangLiList';
import { JLayoutQRCode } from './LuckyDraw/JLayoutQRCode';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JLuckyDraw
 * DateTime = Tue Dec 07 2021 01:30:37 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JLuckyDraw.ts
 * FileBasenameNoExtension = JLuckyDraw
 * URL = db://assets/resources/scripts/JLuckyDraw.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JLuckyDraw')
export class JLuckyDraw extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Node)
    chouJiang:Node | null = null;

    // @property(Node)
    // jiangLi:Node | null = null;

    @property(Label)
    choujiangcishu:Label | null = null;

    @property(Node)
    jiangpin:Node | null = null;

    @property(Node)
    jiangpin_jnp:Node;

    @property(Label)
    jiangpin_jine1:Label | null = null;

    @property(Label)
    jiangpin_jine2:Label | null = null;

    @property(Node)
    choujiangButtons:Node | null = null;

    @property(Node)
    layoutExchange:Node | null = null;

    @property(JLayoutQRCode)
    jlqrcode:JLayoutQRCode;

    @property(Label)
    shengyujifen:Label;

    @property(Node)
    jianglijilu:Node;

    @property(JJiangLiList)
    jianglijiluS:JJiangLiList;

    @property(Node)
    jiangliNull:Node;

    @property(Label)
    duiHuanCode:Label;

    start () {
        // [3]
        let self = this;

        self.layoutExchange.active = false;
        self.jianglijilu.active = false;

        self.chouJiang.active = true;
        //self.jiangLi.active   = false;

        self.jlqrcode.node.active = false;

        self.jiangliNull.active = false;

        self.duiHuanCode.string = '未选择兑换红包';

        for(let i = 0 ; i < JCCS.instance.ChouJiangJiLu.length;i++)
        {
            let id = JCCS.instance.ChouJiangJiLu[i].monster;
            let dindex = JCCS.instance.SongZouNianShou.indexOf(id);
            if(dindex>=0)
            {
                JCCS.instance.SongZouNianShou.splice(dindex,1);
            }
        }

        let buttons = self.choujiangButtons.children;

        buttons.forEach(button=>{
            button.on(Input.EventType.TOUCH_END,(event:EventTouch)=>{
                JCCS.instance.audioManager.playSound('button');
                if(!JCCS.instance.JiFen || JCCS.instance.JiFen.zongCJCS==0)
                {
                    showTip(JCCS.instance.MainNode,'没有抽奖机会');
                    return;
                }
                let monster = JCCS.instance.SongZouNianShou[0];

                if(!monster)monster = 0;

                switch(JCCS.instance.Platform)
                {
                    case JPlatformType.web_localhost:
                        {
                            self.jiangpin.active = true;
                        }
                        break;
                    case JPlatformType.web_mobile:
                        {
                            JCCS.instance.SendServer.LuckyDraw(monster,(res:any)=>{
                                let re = JSON.parse(res);
                                if(re.result)
                                {
                                    
                                }else{
                                    JCCS.instance.SongZouNianShou.shift();

                                    if(!re.monsterChance)re.monsterChance = 0;
                                    if(!re.scoreChance)re.scoreChance = 0;

                                    JCCS.instance.JiFen.scoreChance = re.monsterChance + re.scoreChance;

                                    self.upSJiFen();
                                    
                                    if(re.award)
                                    {
                                        
                                        if(!re.award.type)re.award.type = 0;

                                        switch(re.award.type)
                                        {
                                            case 0:
                                                self.jiangpin.active = true;
                                                self.jiangpin_jine1.string = '恭喜获得'+re.award.amount+'元现金红包';
                                                self.jiangpin_jine2.string = '¥'+re.award.amount;                                                
                                                break;
                                            case 1:
                                                self.jiangpin_jnp.active = true;
                                                break;
                                        }

                                        //{"scoreChance":2,"award":{"date":"2022-01-25","amount":1.0,"name":"旅游纪念品","id":4,"type":1}}
                                        //{"scoreChance":1,"award":{"date":"2022-01-25","amount":1.0,"name":"年票","id":5,"type":1}}
                                        //{"award":{"date":"2022-01-25","amount":18.8,"name":"微信红包","id":3}}

                                    }else{
                                        self.jiangliNull.active = true;
                                    }
                                }
                            })                                
                        }
                        break;
                    case JPlatformType.wx_mobile:
                    case JPlatformType.wx_localhost:
                        {
                        
                        }
                        break;
                }



            })
        })


        self.shengyujifen.string = '0分';

        switch(JCCS.instance.Platform)
        {
            case JPlatformType.web_localhost:
                {

                }
                break;
            case JPlatformType.web_mobile:
                {
                    JCCS.instance.SendServer.getScore((res:any)=>{
                        res = JSON.parse(res);
                        if(res.result)
                        {

                        }else{
                            JCCS.instance.JiFen      =  res;
                            self.upSJiFen();     
                        }

                    })                    
                }
                break;
            case JPlatformType.wx_mobile:
            case JPlatformType.wx_localhost:
                {

                }   
                break;
        }     



        self.layoutExchange.getComponent(JLuckyDrawExchange).upJiFen = self.upSJiFen.bind(self);
    }

    public upSJiFen()
    {
        let self = this;
        
        console.log('积分修改');
        console.log(JSON.stringify(JCCS.instance.JiFen));
        if(!JCCS.instance.JiFen)
        {
            JCCS.instance.JiFen = {};
        }
        if(!JCCS.instance.JiFen.balance)
        {
            JCCS.instance.JiFen.balance = 0;
        }        
        
        self.shengyujifen.string   = JCCS.instance.JiFen.balance+'分'; 

        if(!JCCS.instance.JiFen.scoreChance)JCCS.instance.JiFen.scoreChance=0;
        if(!JCCS.instance.JiFen.monsterChance)JCCS.instance.JiFen.monsterChance=0;
        JCCS.instance.JiFen.zongCJCS = JCCS.instance.JiFen.scoreChance + JCCS.instance.JiFen.monsterChance;
        self.choujiangcishu.string = '剩余'+JCCS.instance.JiFen.zongCJCS+'次抽奖机会';
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private onSelect(a:Toggle)
    {
        
    }

    private gotoJLJL(){
        //console.error(a); 
        let self = this;   
        JCCS.instance.audioManager.playSound();

        switch(JCCS.instance.Platform)
        {
            case JPlatformType.web_localhost:
                {
                    self.upChouJiangJiLu();
                }
                break;
            case JPlatformType.web_mobile:
                {
                    JCCS.instance.SendServer.getRedeemCodes((res:any)=>{
                        res = JSON.parse(res);
                        if(res && res.data)
                        {
                            let data = res.data;

                            JCCS.instance.hongBaoList = data;
                        }else{
                            JCCS.instance.hongBaoList = [];
                        }

                        self.upChouJiangJiLu1();
                    });                    
                }
                break;
            case JPlatformType.wx_mobile:
            case JPlatformType.wx_localhost:
                {


                    return;
                    JCCS.instance.SendServer.LuckyDraw_record((res:any)=>{

                        let re = JSON.parse(res);
                        if(re.result || !re.award)
                        {

                            if(!re.result && !re.award)
                            {
                                JCCS.instance.ChouJiangJiLu = [];
                                self.upChouJiangJiLu();
                            }

                        }else{

                            JCCS.instance.ChouJiangJiLu = re.award;
                            for(let i = 0 ; i < JCCS.instance.ChouJiangJiLu.length;i++)
                            {
                                let id = JCCS.instance.ChouJiangJiLu[i].monster;
                                if(!id)id=0;
                                let dindex = JCCS.instance.SongZouNianShou.indexOf(id);
                                if(dindex>=0)
                                {
                                    JCCS.instance.SongZouNianShou.splice(dindex,1);
                                }
                            }
                            self.upChouJiangJiLu();
                        }

                    })                        
                }
                break;
        }
    }

    private onChouJiang2() {
        let self = this;
        JCCS.instance.SendServer.getRedeem((res:any)=>{
            res = JSON.parse(res);
            self.gotoJLJL();
        }) 
    }

    private onChouJiang(a:Toggle)
    {
        if(!a.isChecked)//使用了反向
        {
            let self = this;
            JCCS.instance.SendServer.getRedeem((res:any)=>{
                res = JSON.parse(res);
                self.gotoJLJL();
            })
        }
    }

    public upChouJiangJiLu1()
    {
        let self = this;

        console.log('送走年兽:')
        console.log(JCCS.instance.SongZouNianShou);
        console.log(JCCS.instance.hongBaoList);

        
        self.chouJiang.active       = false;
        self.jiangpin.active        = false;
        self.jiangpin_jnp.active    = false;

        self.jianglijilu.active     = true;
        self.jianglijiluS.upUI1();

    }

    public upChouJiangJiLu()
    {
        let self = this;

        console.log('送走年兽:')
        console.log(JCCS.instance.SongZouNianShou);
        console.log(JCCS.instance.ChouJiangJiLu);

        self.jianglijiluS.upUI();
        self.chouJiang.active       = false;
        self.jiangpin.active        = false;
        self.jiangpin_jnp.active    = false;

        self.jianglijilu.active     = true;

    }

    private onJiangLi(a:Toggle)
    {
        if(!a.isChecked)//使用了反向
        {
            //console.error(a);     
            let self = this;   
            JCCS.instance.audioManager.playSound();
            self.chouJiang.active = true;
            //self.jiangLi.active   = false;  
            self.jianglijilu.active = false;
            for(let i = 0 ; i < JCCS.instance.ChouJiangJiLu.length;i++)
            {
                let id = JCCS.instance.ChouJiangJiLu[i].monster;
                let dindex = JCCS.instance.SongZouNianShou.indexOf(id);
                if(dindex>=0)
                {
                    JCCS.instance.SongZouNianShou.splice(dindex,1);
                }
            }   
            
        }
    }

    private onDelete()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.node.removeFromParent();
    }

    private onHideJinagPin()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.jiangpin.active = false;

    }


    private gotoChoujinagPage()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.chouJiang.active = true;
        //self.jiangLi.active   = false;
        self.jianglijilu.active = false;
    }


    private onTixian()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        showTip(JCCS.instance.MainNode,'提现成功');
    }

    private onHideJiangLiNull()
    {
        let self = this;
        self.jiangliNull.active = false;
    }

    private onCopy()
    {
        let webCopyString=(str)=>{
            var input = str;
            const el = document.createElement('textarea');
            el.value = input;
            el.setAttribute('readonly', '');
            el.style.contain = 'strict';
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            el.style.fontSize = '12pt';
     
            const selection = getSelection();
            var originalRange:any = false;
            if (selection.rangeCount > 0) {
                originalRange = selection.getRangeAt(0);
            }
            document.body.appendChild(el);
            el.select();
            el.selectionStart = 0;
            el.selectionEnd = input.length;
     
            var success = false;
            try {
                success = document.execCommand('copy');
            } catch (err) {}
     
            document.body.removeChild(el);
     
            if (originalRange) {
                selection.removeAllRanges();
                selection.addRange(originalRange);
            }
     
            return success;               
        }

        let self = this;
        if(self.duiHuanCode.string == '' || self.duiHuanCode.string == '未选择兑换红包')
        {
            return;
        }

        showTip(JCCS.instance.MainNode,'成功复制兑换码');

        webCopyString(self.duiHuanCode.string);

        console.log('复制成功：'+self.duiHuanCode.string);

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
