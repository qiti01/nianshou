
import { _decorator, Component, Node, Sprite, SpriteFrame } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JAudioButton
 * DateTime = Sat Dec 25 2021 19:43:41 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JAudioButton.ts
 * FileBasenameNoExtension = JAudioButton
 * URL = db://assets/resources/scripts/JAudioButton.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JAudioButton')
export class JAudioButton extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    buttonSprite:Sprite;

    @property(SpriteFrame)
    buttonSFs:SpriteFrame[]=[];

    private _state_off:boolean = false;

    start () {
        // [3]
        let self = this;

        if(JCCS.instance.audio_off)
        {
            self._state_off = true;
            self.buttonSprite.spriteFrame = self.buttonSFs[1];
        }

    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private onSwitch()
    {
        let self = this;
        self._state_off = !self._state_off;

        if(self._state_off)
        {
            self.buttonSprite.spriteFrame = self.buttonSFs[1];
            JCCS.instance.audio_off = true;            
            JCCS.instance.audioManager.stopAll();
            localStorage.setItem('audio','1');
        }else{
            self.buttonSprite.spriteFrame = self.buttonSFs[0];
            JCCS.instance.audio_off = false;            
            JCCS.instance.audioManager.playBGM();
            localStorage.setItem('audio','0');
        }

        JCCS.instance.audioManager.playSound();
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
