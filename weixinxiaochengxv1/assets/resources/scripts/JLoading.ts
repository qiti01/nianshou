
import { _decorator, Component, Node, tween, SliderComponent } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JLoading
 * DateTime = Mon Jan 17 2022 19:28:59 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JLoading.ts
 * FileBasenameNoExtension = JLoading
 * URL = db://assets/resources/scripts/JLoading.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JLoading')
export class JLoading extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Node)
    loadingSprite:Node;

    @property(Node)
    loadingNode:Node;

    start () {
        // [3]
        let self = this;

        tween(self.loadingSprite)
        .by(1.5,{angle:360})
        .union()
        .repeatForever()
        .start()

        self.loadingNode.active = false;
        if(self.node.j_convey_data)
        {
            self.loadingNode.active = true;
        }

    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
