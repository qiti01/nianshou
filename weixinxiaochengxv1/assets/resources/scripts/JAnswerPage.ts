
import { _decorator, Component, tween, UIOpacity, v3, Label } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JAnswerPage
 * DateTime = Mon Jan 10 2022 18:54:17 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JAnswerPage.ts
 * FileBasenameNoExtension = JAnswerPage
 * URL = db://assets/resources/scripts/JAnswerPage.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JAnswerPage')
export class JAnswerPage extends Component {

    @property(UIOpacity)
    bk:UIOpacity;

    @property(UIOpacity)
    npc:UIOpacity;

    @property(UIOpacity)
    chatBK:UIOpacity;

    @property(UIOpacity)
    chatText:UIOpacity;

    @property(Label)
    chatText_Label:Label;

    @property(UIOpacity)
    spriteMap:UIOpacity;

    public AnimationEnd:Function;

    start () {
        // [3]

        let self = this;

        self.npc.node.scale = v3(0,0,1);

        self.spriteMap.node.active = true;
        //self.spriteMap.opacity = 0;

        self.bk.opacity = 0;

        // tween(self.spriteMap)
        // .delay(1)
        // .to(0.4,{opacity:0})
        // .start()

        tween(self.npc)
        .delay(1)
        .to(0.1,{opacity:255})
        .call(()=>{

        })
        .start()

        tween(self.npc.node)
        .delay(1)
        .to(4.3,{scale:v3(2.2,2.2,1)})
        .call(()=>{
            tween(self.bk)
            .to(1,{opacity:255})
            .start()
        })
        .start()

            tween(self.chatBK)
            .to(0.2,{opacity:255})
            .call(()=>{
                tween(self.chatText)
                .to(0.1,{opacity:255})
                .call(()=>{
                    self.startTyping();
                })
                .start()
            })
            .start()


    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    /**开始打字*/
    private startTyping()
    {
        let self = this;

        let index = 0;

        let ts= '唐太宗东巡，路过滏口陉，等太行而弯弓展志，约三百步，人马齐鸣。恰逢春节，于是驻军东太行，与民共庆佳节。';

        console.log(ts.length);
        
        JCCS.instance.audio_off = false;
        JCCS.instance.audioManager.playSound('typing');

        self.chatText_Label.string += ts[index];
        index++;

        let tf = ()=>{
            self.chatText_Label.string += ts[index];
            index++;
            if(index == ts.length)
            {
                self.unschedule(tf);
                self.scheduleOnce(()=>{
                    console.log('唐太宗页面动画结束了')

                    tween(self.node.getComponent(UIOpacity))
                    .to(0.5,{opacity:0})
                    .start()

                    if(self.AnimationEnd)self.AnimationEnd();
                },1)
            }
        }

        self.schedule(tf,7/ts.length);

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
