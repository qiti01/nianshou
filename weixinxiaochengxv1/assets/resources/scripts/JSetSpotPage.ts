
import { _decorator, Component, Node, EditBox, Label, Button, Sprite, Toggle } from 'cc';
import { showTip } from './common/JTools';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JSetSpotPage
 * DateTime = Fri Dec 24 2021 15:54:53 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JSetSpotPage.ts
 * FileBasenameNoExtension = JSetSpotPage
 * URL = db://assets/resources/scripts/JSetSpotPage.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JSetSpotPage')
export class JSetSpotPage extends Component {

    @property(EditBox)
    jingdianname:EditBox;

    @property(Label)
    tip0:Label;

    @property(Label)
    tip1:Label;

    @property(Label)
    tip2:Label;


    @property(Node)
    loding:Node;

    @property(Button)
    button0:Button;

    @property(Button)
    button1:Button;

    @property(Button)
    button2:Button;

    @property(Label)
    lon:Label;

    @property(Label)
    lat:Label;

    private _jdname:string = '';

    start () {
        // [3]

        let self = this;
        self.loding.active = false;
        self._jdname = JCCS.instance.SpotNames[0];
    }
onEnable()
{
    let self = this;

        self.lat.string = '获取中';
        self.lon.string = '获取中';
        self.initInputBox();
        window['wxGetLocation']((ress:any)=>{

            if(ress.error)
            {
                showTip(JCCS.instance.MainNode,'微信坐标获取失败,请重试');
                self.loding.active = false;
                return;
            }

            self.lat.string = ress.latitude;
            self.lon.string = ress.longitude;

        })
}
    // update (deltaTime: number) {
    //     // [4]
    // }

    private onHide()
    {
        let self = this;
        self.node.active = false;
    }

    private setSpot0()
    {
        let self = this;
        let sendname = '';


        if(self.jingdianname.string == ''){
            sendname = self._jdname;
        }else{
            sendname = self.jingdianname.string;
        }

        if(sendname == ''){
            showTip(JCCS.instance.MainNode,'请输入景点名称');
            return;
        }
        self.loding.active = true;
        
        if(window['wxGetLocation'])
        window['wxGetLocation']((ress:any)=>{

            if(ress.error)
            {
                showTip(JCCS.instance.MainNode,'微信坐标获取失败,请重试');
                self.loding.active = false;
                return;
            }

            JCCS.instance.SendServer.setPoint({name:sendname,lat:ress.latitude,lon:ress.longitude},(res)=>{
                res = JSON.parse(res);
                if(res.result)
                {
                    self.loding.active = false;
                    self.tip0.string = '上传失败，请重试'
                    showTip(JCCS.instance.MainNode,'上传失败');   
                }else{
                    self._jdname = sendname;
                    self.scheduleOnce(()=>{
                        self.loding.active = false;
                        self.tip0.string = '上传成功'
                        showTip(JCCS.instance.MainNode,'上传成功');   
                        self.button0.node.active = false;                      
                    },5)
                }
            })

        });


    }

    private setSpot1()
    {
        let self = this;
        let sendname = '';


        if(self.jingdianname.string == ''){
            sendname = self._jdname;
        }else{
            sendname = self.jingdianname.string;
        }

        if(sendname == ''){
            showTip(JCCS.instance.MainNode,'请输入景点名称');
            return;
        }
        self.loding.active = true;

        if(window['wxGetLocation'])
        window['wxGetLocation']((ress:any)=>{

            if(ress.error)
            {
                showTip(JCCS.instance.MainNode,'微信坐标获取失败,请重试');
                self.loding.active = false;
                return;
            }

            JCCS.instance.SendServer.setPoint({name:sendname,lat:ress.latitude,lon:ress.longitude},(res)=>{
                res = JSON.parse(res);
                if(res.result)
                {
                    self.loding.active = false;
                    self.tip1.string = '上传失败，请重试'
                    showTip(JCCS.instance.MainNode,'上传失败');   
                }else{
                    self._jdname = sendname;
                    self.scheduleOnce(()=>{
                        self.loding.active = false;
                        self.tip1.string = '上传成功'
                        showTip(JCCS.instance.MainNode,'上传成功'); 
                        self.button1.node.active = false;                      
                    },5)               
                }
            })

        });
    }

    private setSpot2()
    {
        let self = this;
        let sendname = '';


        if(self.jingdianname.string == ''){
            sendname = self._jdname;
        }else{
            sendname = self.jingdianname.string;
        }

        if(sendname == ''){
            showTip(JCCS.instance.MainNode,'请输入景点名称');
            return;
        }
        self.loding.active = true;

        if(window['wxGetLocation'])
        window['wxGetLocation']((ress:any)=>{

            if(ress.error)
            {
                showTip(JCCS.instance.MainNode,'微信坐标获取失败,请重试');
                self.loding.active = false;
                return;
            }

            JCCS.instance.SendServer.setPoint({name:sendname,lat:ress.latitude,lon:ress.longitude},(res)=>{
                res = JSON.parse(res);
                if(res.result)
                {
                    self.loding.active = false;
                    self.tip2.string = '上传失败，请重试'
                    showTip(JCCS.instance.MainNode,'上传失败');   
                }else{
                    self._jdname = sendname;
                    self.scheduleOnce(()=>{
                        self.loding.active = false;
                        self.tip2.string = '上传成功'
                        showTip(JCCS.instance.MainNode,'上传成功');     
                        self.button2.node.active = false;                  
                    },5)             
                }
            })

        });
    }


    private initInputBox()
    {
        let self = this;
        if(self._jdname != self.jingdianname.string)
        {
            self.tip0.string = '需要上传';
            self.tip1.string = '需要上传';
            self.tip2.string = '需要上传';
            self.button0.node.active = true;
            self.button1.node.active = true;
            self.button2.node.active = true;


        }
    }

    private onSelect(event:Toggle)
    {
        let self = this;
        let name = event.node.getChildByName('name').getComponent(Label).string;
        console.log(name);
        self._jdname = name;
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
