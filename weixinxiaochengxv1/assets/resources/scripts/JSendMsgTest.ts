
import { _decorator, Component, Node, EditBox, Label } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JSendMsgTest
 * DateTime = Sat Jan 22 2022 18:54:07 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JSendMsgTest.ts
 * FileBasenameNoExtension = JSendMsgTest
 * URL = db://assets/resources/scripts/JSendMsgTest.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JSendMsgTest')
export class JSendMsgTest extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(EditBox)
    editBox_login:EditBox;

    @property(EditBox)
    editBox_checkin:EditBox;

    @property(EditBox)
    editBox_SNS:EditBox;

    @property(EditBox)
    editBox_setLingChong:EditBox;

    @property(EditBox)
    editBox_Choujiang:EditBox;

    @property(EditBox)
    editBox_msg:EditBox;

    @property(EditBox)
    editBox_uid:EditBox;

    @property(EditBox)
    editBox_roleID:EditBox;

    @property(EditBox)
    editBox_tiID:EditBox;

    @property(EditBox)
    editBox_daanID:EditBox;

    @property(EditBox)
    editBox_jifen:EditBox;

    @property(EditBox)
    editBox_choujiangjihui:EditBox;

    @property(Node)
    layout_log:Node;

    @property(Label)
    sendLabel:Label;

    @property(Label)
    backCallLabel:Label;

    start () {
        // [3]
        let self = this;

        //self.editBox_login.string = '"code":"051Harll2gG4v84SqRll28LFw23HarlZ","city":"","country":"","gender":0,"nickName":"데스미온","avatar":"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI0lItQLtOZGiaibsUfgFOxOsaNhOCS5GZ6HbxnHMMboffyURyDhu6LAia1QXgje7PEujrb1dGNeHyLA/132","language":"zh_CN"}';
        //self.editBox_login.string = '"code":"061igx000KsJaN1jdv100P2dyo4igx0c","city":"","country":"","gender":0,"nickName":"데스미온","avatar":"","language":"zh_CN"}';
        //self.editBox_checkin.string = '1';
        self.layout_log.active = false;
        JCCS.instance.SendServer.TestBackCall = self.onShowLog.bind(self);
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    public onLogIn()
    {
        let self = this;

        // JCCS.instance.SendServer.sendLogin(self.editBox_login.string,(res:any)=>{
        //     res = JSON.parse(res);
        //     console.log(res);
        // })
        window['wxGetSetting']((obj:any)=>{
            console.log('微信授权');
            console.log(obj);
            JCCS.instance.myName    = obj.nickName;
            JCCS.instance.myHeadUrl = obj.avatar;
             
            JCCS.instance.SendServer.sendLogin(obj,(data:any)=>{
                
                let s = JSON.parse(data);
                console.log('登录回调')
                
                JCCS.instance.SendServer.tou = s.session;

            })
        });
    }

    public onCheckin()
    {
        let self = this;
        JCCS.instance.SendServer.checkin(self.editBox_checkin.string,(res:any)=>{
            res = JSON.parse(res);
            console.log(res);
        })
    }

    public onChechin_lian()
    {
        let self = this;
        for(let i = 1;i <=21;i++)
        {
            JCCS.instance.SendServer.checkin(i.toString(),(res:any)=>{
                res = JSON.parse(res);
                console.log(res);
            })            
        }
    }

    private showItemsData()
    {
        let nianshou:number[]=[];
        let nianshou1:number[]=[];
        let nianshou2:number[]=[];
        let nianshou3:number[]=[];
        let nianshou4:number[]=[];

        console.log('总共获得年兽和道具:'+JSON.stringify(JCCS.instance.Items));
        let items = JCCS.instance.Items;
        
        for(let i = 0 ; i < items.length;i++)
        {
            let item = items[i];
            if(item < 10)
            {
                nianshou.push(item);
            }else if(item < 20)
            {
                nianshou1.push(item);
            }else if(item < 30)
            {
                nianshou2.push(item);
            }else if(item < 40)
            {
                nianshou3.push(item);
            }else{
                nianshou4.push(item);
            }   
        }

        console.log('获得年兽:'+JSON.stringify(nianshou));
        console.log('年兽1道具:'+JSON.stringify(nianshou1));
        console.log('年兽2道具:'+JSON.stringify(nianshou2));
        console.log('年兽3道具:'+JSON.stringify(nianshou3));
        console.log('其他:'+JSON.stringify(nianshou4));       
        
        return {ns:nianshou,a:nianshou1,b:nianshou2,c:nianshou3,d:nianshou4};
    }

    public onGetItems()
    {
        JCCS.instance.SendServer.getItems(()=>{
            this.showItemsData();
        });
    }

    public onSongNianShou()
    {
        let self = this;
        let obj = self.showItemsData();
        let id  = Number(self.editBox_SNS.string);
        if(obj.ns.indexOf(id) >=0)
        {
            let s = [];
            switch(id)
            {
                case 1:
                    s = obj.a;
                    break;
                case 2:
                    s = obj.b;
                    break;
                case 3:
                    s = obj.c;
                    break;
            }

            if(s.length == 4)
            {
                JCCS.instance.SendServer.songZou(id,s,(res:any)=>{
                    res = JSON.parse(res);
                    console.log(res);
                }) 
            }else{
                console.error('道具数量不够');
            }

        }else{
            console.error('没抓到年兽');
        }
 
    }


    public getGuide()
    {
        JCCS.instance.SendServer.isGuide((res:any)=>{
            
        })
    }

    public setGuide()
    {
        JCCS.instance.SendServer.setGuide()
    }

    public getLingChong(){
        JCCS.instance.SendServer.getLingChong(()=>{});
    }

    public setLingChong()
    {
        JCCS.instance.LingChong = Number( this.editBox_setLingChong.string )+1;
        JCCS.instance.SendServer.setLingChong(()=>{});
    }

    public initData()
    {
        JCCS.instance.SendServer.userClear();
    }

    public ChouJiang()
    {
        JCCS.instance.SendServer.LuckyDraw(this.editBox_Choujiang.string,()=>{})
    }

    public getChouJiang()
    {
        JCCS.instance.SendServer.LuckyDraw_record(()=>{});
    }

    public sendMsg()
    {
        JCCS.instance.SendServer.sendChatMsg(this.editBox_msg.string,()=>{

        })
    }

    public getMsg()
    {
        JCCS.instance.SendServer.getChatMsg(()=>{});
    }

    public getQuGanNianShou()
    {
        JCCS.instance.SendServer.getComposed(()=>{});
    }

    public getUserList()
    {
        JCCS.instance.SendServer.getUserList(()=>{});
    }

    public sendLike()
    {
        JCCS.instance.SendServer.like(this.editBox_uid.string,()=>{})
    }

    public getRoleID()
    {
        JCCS.instance.SendServer.getRole(()=>{});
    }

    public setRoleID()
    {
        JCCS.instance.RoleIndex = Number(this.editBox_roleID.string)+1;
        JCCS.instance.SendServer.setRole(()=>{});
    }

    public onDaTi()
    {
        JCCS.instance.SendServer.sendAnswer({id:this.editBox_tiID.string,
                                            index:this.editBox_daanID.string},()=>{

        })
    }

    public getJiFen()
    {
        JCCS.instance.SendServer.getScore(()=>{});
    }

    public duiHuan()
    {
        JCCS.instance.SendServer.sendScore2Chance({score:this.editBox_jifen.string,chance:this.editBox_choujiangjihui.string},()=>{

        })
    }


    private onShowLog(a:string,b:string)
    {
        let self = this;
        self.layout_log.active = true;

        self.sendLabel.string = a;
        self.backCallLabel.string = b;

    }

    private onHideLog()
    {
        let self = this;
        self.layout_log.active = false;

    }


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
