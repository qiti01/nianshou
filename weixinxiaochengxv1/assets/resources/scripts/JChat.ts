
import { _decorator, Component, Node, Sprite, SpriteFrame, UITransform, Button, v3, EditBox, instantiate, size } from 'cc';
import { JCCS } from './JCC';
import { JChatItem } from './JChatItem';
import { JChatUserList } from './JChatUserList';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JChat
 * DateTime = Tue Dec 07 2021 17:43:16 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JChat.ts
 * FileBasenameNoExtension = JChat
 * URL = db://assets/resources/scripts/JChat.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JChat')
export class JChat extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    SpriteChat:Sprite | null = null;

    @property(SpriteFrame)
    SpriteChat_sfs:SpriteFrame[] = [];

    @property(SpriteFrame)
    zhedieButtonsfs:SpriteFrame[] = [];

    @property(Button)
    zhedie:Button | null = null;

    @property(Button)
    fasong:Button | null = null;

    @property(Node)
    userList:Node | null = null;

    @property(EditBox)
    sendMsgLabel:EditBox | null = null;

    @property(Node)
    contentNode:Node | null = null;

    @property(Node)
    chatItem:Node | null = null;

    @property(Node)
    chatItem2:Node | null = null;

    @property(JChatUserList)
    chatUserList:JChatUserList | null = null;

    private _index:number = 0;

    private _endTime:number = 0;

    start () {
        // [3]

        let self = this;
        self.chatItem.active = false;
        self.chatItem.parent = self.node;

        self.chatItem2.active = false;
        self.chatItem2.parent = self.node;

        //let qw = self.SpriteChat.getComponent(UITransform).contentSize.width;
        self.SpriteChat.spriteFrame = self.SpriteChat_sfs[0];
        self.userList.active = false;


        // let hw = self.SpriteChat.getComponent(UITransform).contentSize.width;
        // self.zhedie.node.setPosition(v3(self.zhedie.node.getPosition().x-(qw-hw),self.zhedie.node.getPosition().y));
        // self.fasong.node.setPosition(v3(self.fasong.node.getPosition().x-(qw-hw),self.fasong.node.getPosition().y));
    
        
        self.getMsgList();
        
        self.schedule(()=>{
            self.getMsgList();
        },30);

    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private getMsgList() {
        let self = this;
        JCCS.instance.SendServer.getChatMsg((res:any)=>{
            res = JSON.parse(res);
            if(!res.msgs)return;
            if(self._endTime != 0)
            {
                for(let i = 0 ; i < res.msgs.length ;i++)
                {
                    let msg = res.msgs[i];
                    if(msg.time != self._endTime)
                    {
                        self.addMsg(msg.avatar,msg.sender,msg.msg);
                    }else{
                        self._endTime = res.msgs[0].time;
                        return;
                    }

                }
            }else{
                for(let i = res.msgs.length-1 ; i >=0 ;i--)
                {
                    let msg = res.msgs[i];
                    if(i==0)
                    {
                        self._endTime = msg.time;
                    }
                    self.addMsg(msg.avatar,msg.sender,msg.msg);
                }
            }

        })
    }

    private onShow()
    {
        let self = this;

        let jianw = 0;

        if(self._index==0)
        {
            //缩短
            self.chatUserList.upList();
            self._index = 1;
            let qw = self.SpriteChat.getComponent(UITransform).contentSize.width;
            self.SpriteChat.spriteFrame = self.SpriteChat_sfs[1];
            let hw = self.SpriteChat.getComponent(UITransform).contentSize.width;
            jianw = qw-hw;
            self.zhedie.node.setPosition(v3(self.zhedie.node.getPosition().x-(qw-hw),self.zhedie.node.getPosition().y));
            self.fasong.node.setPosition(v3(self.fasong.node.getPosition().x-(qw-hw),self.fasong.node.getPosition().y));
            //self.zhedie.node.getComponent(Sprite).spriteFrame = self.zhedieButtonsfs[1];
            self.userList.active = true;
            self.zhedie.node.active = false;

            self.contentNode.children.forEach(node=>{
                let snode = node.getComponent(JChatItem);
                snode.upItemWidth(-jianw);
            })
        }else{
            //拉长
            self._index = 0;
            let qw = self.SpriteChat.getComponent(UITransform).contentSize.width;
            self.SpriteChat.spriteFrame = self.SpriteChat_sfs[0];
            let hw = self.SpriteChat.getComponent(UITransform).contentSize.width;
            jianw = hw-qw;
            self.zhedie.node.setPosition(v3(self.zhedie.node.getPosition().x+(hw-qw),self.zhedie.node.getPosition().y));
            self.fasong.node.setPosition(v3(self.fasong.node.getPosition().x+(hw-qw),self.fasong.node.getPosition().y));
            //self.zhedie.node.getComponent(Sprite).spriteFrame = self.zhedieButtonsfs[0];
            self.userList.active = false;
            self.zhedie.node.active = true;

            self.contentNode.children.forEach(node=>{
                let snode = node.getComponent(JChatItem);
                snode.upItemWidth(jianw);
            })
        }




    }
    
    private onDelete()
    {
        let self = this;
        self.node.removeFromParent();
    }

    private addMsg(head:string,name:string,msg:string)
    {
        let self = this;

        let uit = self.contentNode.getComponent(UITransform);
        //console.error(uit.contentSize);
        //console.error(self.contentNode.getPosition());
        //uit.setContentSize(size(uit.width,1000))
        //console.error(uit.contentSize);

        let tnode = self.chatItem;

        if(head==JCCS.instance.myHeadUrl)
        {
            tnode = self.chatItem2;
        }

        let cnode = instantiate(tnode);
        cnode.active = true;
        self.contentNode.addChild(cnode);

        // let y = -100*(self.contentNode.children.length-1);
        
        // cnode.setPosition(v3(0,y));

        // console.log('----------------------------------');
        // console.log(y);
        // console.log(100*self.contentNode.children.length);
        // console.log(uit.height);
        // console.log(cnode);

        // if(100*self.contentNode.children.length>uit.height)
        // {
        //     uit.setContentSize(size(uit.width,100*self.contentNode.children.length));
        // }

        let chatItems = cnode.getComponent(JChatItem);
        if(head==JCCS.instance.myHeadUrl)
        {
            chatItems.my = true;
        }
        chatItems.setData({headUrl:head,name:name,msg:msg});

        self.contentNode.updateWorldTransform();

    }

    private onSendMsg()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        if(self.sendMsgLabel.string != '')
        {
            JCCS.instance.SendServer.sendChatMsg(self.sendMsgLabel.string,()=>{

                self.sendMsgLabel.string = '';
                self.contentNode.children.forEach(node=>{
                    node.setPosition(v3(node.getPosition().x,node.getPosition().y+100))
                })
                //let inode = self.chatItem;
                //self.addMsg(JCCS.instance.myHeadUrl,JCCS.instance.myName,self.sendMsgLabel.string);
                self.getMsgList();
            });


        }
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
