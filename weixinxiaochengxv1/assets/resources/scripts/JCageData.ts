
import { _decorator, Component, Node, Sprite, spriteAssembler, SpriteFrame, Button, Label } from 'cc';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { addDanChu, showTip } from './common/JTools';
import { JCCS, JPlatformType } from './JCC';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JCageData
 * DateTime = Tue Dec 07 2021 14:15:00 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JCageData.ts
 * FileBasenameNoExtension = JCageData
 * URL = db://assets/resources/scripts/JCageData.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JCageData')
export class JCageData extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;
    @property(Sprite)
    shou:Sprite    | null = null;
    @property(SpriteFrame)
    shous:SpriteFrame[] = [];
    @property(SpriteFrame)
    shou_nulls:SpriteFrame[] = [];
    @property(Button)
    songzou:Button | null = null;
    @property(Sprite)
    items:Sprite[] = [];
    @property(SpriteFrame)
    itemSF1:SpriteFrame[] = [];
    @property(SpriteFrame)
    itemSF2:SpriteFrame[] = [];
    @property(SpriteFrame)
    itemSF3:SpriteFrame[] = [];

    @property(Node)
    songzouNode:Node | null = null;

    @property(Node)
    choujiangNode:Node | null = null;


    @property(Label)
    choujiangLabel1:Label | null = null;

    @property(Label)
    choujiangLabel2:Label | null = null;

    @property(Sprite)
    songSpriteBK:Sprite | null = null;

    @property(SpriteFrame)
    songSFs:SpriteFrame[] = [];

    private _sou_id:number = 0;

    private _items:number[] = []

    start () {
        // [3]
        let self = this;
        console.log('年兽道具收集状态 年兽ID：'+self.node.j_convey_data);

        let index = Number(self.node.j_convey_data);
        self.upData(index);

        self.songzouNode.active   = true;
        self.choujiangNode.active = false;
    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    private upData(index:number)
    {
        let self = this;
        self._sou_id = index;
        index--;
        if(JCCS.instance.Items.indexOf(self._sou_id)>=0)
        {
            self.shou.spriteFrame = self.shous[index];
        }else{
            self.shou.spriteFrame = self.shou_nulls[index];             
        }

        let imgs:SpriteFrame[] = [];
        switch(self._sou_id)
        {
            case 1:
                imgs = self.itemSF1;
                
                break;
            case 2:
                imgs = self.itemSF2;

                break;
            case 3:
                imgs = self.itemSF3;

                break;
        }

        let items = JCCS.instance.Items;
        let item_ = [];
        if(items.length)
        {
            for(let i = 1 ; i <= 4;i++)
            {
                let it = (self._sou_id)*10+i;
                if(items.indexOf(it)<0)
                {
                    continue;
                }
                item_.push(it);
                self.items[i-1].spriteFrame = imgs[i-1];
            }            
        }

        if(item_.length < 4)
        {
            self.songzou.interactable = false;
            self.songzou.node.getComponent(Sprite).grayscale = true;
        }else{
            self._items = item_;
        }

    }

    private onDelete()
    {
        let self = this;
        self.node.removeFromParent();
    }

    private onSongZou()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        if(self._items.length)
        {

            if(JCCS.instance.Platform == JPlatformType.web_localhost)
            {
                JCCS.instance.ChouJiangCiShu++;
                self.showSongJieGuo(JCCS.instance.ChouJiangCiShu);

            }else{
                JCCS.instance.SendServer.songZou(self._sou_id,self._items,(res:any)=>{
                    let r = JSON.parse(res);
                    if(r.result)
                    {
                        showTip(JCCS.instance.MainNode,'送走年兽失败');
                    }else{
                        self.showSongJieGuo(Number(r.total));
                    }
                });                  
            }

        }


    }

    private showSongJieGuo(total:number)
    {
        let self = this;
        JCCS.instance.SongZouNianShou.push(self._sou_id);
        switch(self._sou_id)
        {
            case 1:
                self.choujiangLabel1.string = '经过你的英勇奋战，终将噬运兽踢回了“火星';
                self.choujiangLabel2.string = '福星高照';
                break;
            case 2:
                self.choujiangLabel1.string = '经过你的英勇奋战，终将噬财兽踢回了“火星';
                self.choujiangLabel2.string = '财源滚滚';
                break;
            case 3:
                self.choujiangLabel1.string = '经过你的英勇奋战，终将噬情兽踢回了“火星”';
                self.choujiangLabel2.string = '两情相悦';
                break;
        }
        JCCS.instance.ChouJiangCiShu = total;

        self.songzouNode.active   = false;
        self.choujiangNode.active = true;  

        self.songSpriteBK.spriteFrame = self.songSFs[self._sou_id-1];
        JEventCenter.emit(JEventName.upData,{upTypes:['hideLongZi'],id:self._sou_id}); 
    }


    private onChouJiang()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        self.node.removeFromParent();
        addDanChu(JCCS.instance.MainS,PAGE_NAME.LUCKY_DRAW)
    }

    /**关闭页面*/
    private onHide()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        self.node.removeFromParent();   

        JEventCenter.emit(JEventName.upData,{upTypes:['hideCageData']}); 
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
