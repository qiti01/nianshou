
import { _decorator, Component, Node, Label, instantiate } from 'cc';
import { JCCS } from './JCC';
import { JMapSpotDataItem } from './JMapSpotDataItem';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JMapSpotData
 * DateTime = Tue Dec 28 2021 00:27:32 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JMapSpotData.ts
 * FileBasenameNoExtension = JMapSpotData
 * URL = db://assets/resources/scripts/JMapSpotData.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JMapSpotData')
export class JMapSpotData extends Component {

    @property(Node)
    content:Node;

    @property(Node)
    item:Node;

    /**维度*/
    @property(Label)
    myLatitude:Label;

    /**经度*/
    @property(Label)
    myLongitude:Label;


    start () {
        // [3]
        let self = this;

        self.item.active = false;

        self.item.parent = self.node;

    }

    onEnable()
    {
        let self = this;
        self.myLatitude.string  = JCCS.instance.mySpot.Latitude.toString();
        self.myLongitude.string = JCCS.instance.mySpot.Longitude.toString();
        JCCS.instance.SendServer.getPoint((res:any)=>{
            if(res.points)
            {
                self.upList(res.points)
            }
        })
    }
    onDisable(){
        let self = this;


    }


    private upList(datas:any)
    {
        let self = this;

        let tobj:any = {};


        for(let i = 0 ; i < datas.length;i++)
        {
            let data = datas[i];

            if(!tobj[data.name])
            {
                tobj[data.name] = {};
                tobj[data.name].Latitude  = 0;
                tobj[data.name].Longitude = 0;
                tobj[data.name].datas    = [];
            }
            tobj[data.name].Latitude  += data.lat;
            tobj[data.name].Longitude += data.lon;

            tobj[data.name].datas.push( {Latitude:data.lat,Longitude:data.lon} );

        }


        let obj:{name:string,Latitude:number,Longitude:number,datas:any[]}[] = [];
        
        for(let key in tobj)
        {
            tobj[key].Latitude  = tobj[key].Latitude/tobj[key].datas.length;
            tobj[key].Longitude = tobj[key].Longitude/tobj[key].datas.length;
            let o = {name:key,Latitude:tobj[key].Latitude,Longitude:tobj[key].Longitude,datas:tobj[key].datas};
            if(!JCCS.instance.SpotDatas[key])
            {
                JCCS.instance.SpotDatas[key] = {};
                JCCS.instance.SpotDatas[key].Longitude = tobj[key].Longitude;
                JCCS.instance.SpotDatas[key].Latitude  = tobj[key].Latitude;                   
            }
         
            obj.push(o);
        }

        console.log(obj);
        self.content.removeAllChildren();
        for(let i = 0 ; i < obj.length;i++)
        {
            let data = obj[i];

            let node = instantiate(self.item);
            node.active = true;

            self.content.addChild(node);

            let sn = node.getComponent(JMapSpotDataItem);

            sn.upData(data);


        }

    }




    // update (deltaTime: number) {
    //     // [4]
    // }

    private onHide()
    {
        let self = this;

        self.node.active = false;
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
