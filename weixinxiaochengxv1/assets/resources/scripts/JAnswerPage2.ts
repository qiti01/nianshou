
import { _decorator, Node, Label, SpriteFrame, Sprite, Input, color, v3, LabelOutline, UIOpacity, tween } from 'cc';
import { JComponent } from './common/JComponent';
import { addDanChu, showTip } from './common/JTools';
import { JCCS } from './JCC';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JAnswerPage2
 * DateTime = Fri Jan 14 2022 18:25:30 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JAnswerPage2.ts
 * FileBasenameNoExtension = JAnswerPage2
 * URL = db://assets/resources/scripts/JAnswerPage2.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JAnswerPage2')
export class JAnswerPage2 extends JComponent {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    npc:Sprite;

    @property(SpriteFrame)
    npc_sf:SpriteFrame[] = [];

    @property(Sprite)
    my:Sprite;

    @property(Node)
    next:Node;

    @property(Node)
    confirm:Node;

    @property(Label)
    daan:Label[] = [];

    @property(Label)
    label_tip:Label;

    @property(Node)
    daanPage:Node;

    @property(SpriteFrame)
    role_sf:SpriteFrame[]=[];

    @property(Label)
    tis:Label[]=[];

    @property(UIOpacity)
    myuio:UIOpacity;

    private _daan_index = -1;

    /**题 0将军 1农夫 2樵夫 3诗人 4侠客 5落魄公子 6才女 7公主 8谋臣*/
    private _timu:string[] = [
        '我一路护送陛下，勤勤恳恳，却还是出了差错，回京时间迫近，还没找到陛下，此罪甚大，或株连九族？请贤者告知，我现在如何是好？',
        '陛下北巡，在我家吃的一碗驴肉火烧，还亲题匾额，往年见过年兽所居，今主上蒙难，我该如何，请帮我选择：',
        '除夕夜，我下山太晚，看见年兽挟持一人向西山奔去，一路跟踪到这里，不知去向，不知何人遭难？',
        '虽则本次榜上无名，却也不耽误感受这大好景致，诸人纷扰，何不一醉方休',
        '刺杀了刘三这欺男霸女的恶绅，总算是为民除了一害，这满山的官兵， 难道是来抓我，贤士为我解惑？',
        '祖上基业，被贪官把持，家父除夕之夜，蒙辱而去，此仇不共戴天？',
        '夫君半年前入京赶考，榜上无名，不知何处去了，却是愁煞人也',
        '父皇带我北巡，却被怪兽抓走了，如何是好',
        '曾劝君上不要在此久留，最终不停，今日蒙难，如何是好？请贤者谓我',
    ];

    /**答题*/
    private _data:string[][] =[
        [
            'A - 为性命记，潜回京城携带家眷逃去东夷；',
            'B - 纵粉身碎骨，也与年兽势不两立，定要救主归朝；',
            'C - 率兵回京，夺取王位，自立为王；',
            'D - 拥立新君，谋求新机；',
        ],
        [
            'A - 既知怪物藏身处，何不独自上山，杀兽救主，谋取高官；',
            'B - 年兽凶恶，你上有老下有小，祈主平安；',
            'C - 召集乡勇，助将军一臂之力；',
            'D - 王侯将相，与我何干；',
        ],
        [
            'A - 告诉樵夫，陛下蒙难，劝其相救；',
            'B - 不做理会，自顾自去；',
            'C - 这厮力大，恐抢我功，胡言而去；',
            'D - 询问详情，自作判断，告知并急切寻去；',
        ],
        [
            'A - 匆匆共饮一杯，急切前行；',
            'B - 打听消息；',
            'C - 一起欣赏美景；',
            'D - 告知陛下蒙难，劝其抓住进身机会；',   
        ],
        [
            'A - 为其宽心，点赞；',
            'B - 告知陛下情况，邀请一起；',
            'C - 劝其逃亡，免得被抓；',
            'D - 抓住他，交给官府；',
        ],
        [
            'A - 同情之余，悄然离去；',
            'B - 告知陛下情况，告诉其可救主伸冤；',
            'C - 推荐侠客，为其报仇；',
            'D - 与我无干，快速离去；',
        ],
        [
            'A - 告知诗人的位置；',
            'B - 告知陛下情况，邀其救主；',
            'C - 不与理会；',
            'D - 山上兵荒马乱，劝其下山；',
        ],
        [
            'A - 允诺救主，请其放心；',
            'B - 不予理会；',
            'C - 山上豺狼虎豹，劝其下山等候；',
            'D - 留下来保护她；',
        ],
        [
            'A - 为性命记，潜回京城携带家眷逃去东夷；',
            'B - 纵粉身碎骨，也与年兽势不两立，定要救主归朝；',
            'C - 联合将军率军回京，夺取王位；',
            'D - 拥立新君，谋求新机；',
        ]
    ]

    private _tiID = 0;

    private _touchNext:boolean = false;

    start () {
        // [3]
        let self = this;

        self.label_tip.node.active = true;
        self.daanPage.active = false;

        self.next.active = true;
        self.confirm.active = false;

        self.npc.node.active = true;
        self.my.node.active  = false;

        //self.upDaAnSpot();

        let index = Number(self.node.j_convey_data);

        if(!index)
        {
            index = 0;
        }else{
            index -=101;
        }

        self.npc.spriteFrame = self.npc_sf[index];

        self.label_tip.string = self._timu[index];

        let lxz = self._data[index];

        self._tiID = index;

        for(let i = 0 ; i < self.tis.length;i++)
        {
            let ti = self.tis[i];

            ti.string = lxz[i];

            ti.isUnderline = false;

            ti.node.on(Input.EventType.TOUCH_END,()=>{

                //ti.color = color(255,253,177);
                ti.color = color(178,242,255);
                //ti.node.getComponent(LabelOutline).enabled =true;

                ti.isUnderline = true;

                self._daan_index = i;

                //ti.isBold = true;
                //ti.fontSize = 26;
                for(let k = 0 ; k < self.tis.length;k++)
                {
                    if(i!=k)
                    {
                        self.tis[k].color = color(255,255,255);  
                        //self.tis[k].node.getComponent(LabelOutline).enabled =false;
                        //self.tis[k].isBold = false;
                        //self.tis[k].fontSize = 24;
                        self.tis[k].isUnderline = false;
                    }
                }

                self.upDaAnSpot();
            })

            //console.log(ti);
        }



    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    private upDaAnSpot()
    {
        let self = this;

        self.scheduleOnce(()=>{
            self.daan[2].node.setWorldPosition(v3(self.daan[0].node.getWorldPosition().x,self.daan[2].node.getWorldPosition().y));
            self.daan[3].node.setWorldPosition(v3(self.daan[1].node.getWorldPosition().x,self.daan[3].node.getWorldPosition().y));
        },0.02)


    }

    private onNext()
    {
        let self = this;
        if(self._touchNext)return;
        self._touchNext = true;

        JCCS.instance.audioManager.playSound();
        let uio_next    = self.next.getComponent(UIOpacity);
        let uio_confirm = self.confirm.getComponent(UIOpacity);
        let uio_tip     = self.label_tip.getComponent(UIOpacity);
        let uio_daan    = self.daanPage.getComponent(UIOpacity);
        let uio_npc     = self.npc.getComponent(UIOpacity);
        let uio_my      = self.my.getComponent(UIOpacity);

        uio_next.opacity    = 255;
        uio_confirm.opacity = 0;

        uio_tip.opacity     = 255;
        uio_daan.opacity    = 0;

        uio_npc.opacity     = 255;
        uio_my.opacity      = 0;

        let time = 0.5;

        self.next.active    = true;
        self.confirm.active = true;

        self.label_tip.node.active  = true;
        self.daanPage.active        = true;

        self.npc.node.active = true;
        self.my.node.active  = true;

        tween(uio_next)
        .to(time,{opacity:0})
        .start()

        tween(uio_confirm)
        .to(time,{opacity:255})
        .call(()=>{
            self.next.active = false;
        })
        .start()

        tween(uio_tip)
        .to(time,{opacity:0})
        .start()

        tween(uio_daan)
        .to(time,{opacity:255})
        .call(()=>{
            self.label_tip.node.active = false;
        })
        .start()

        tween(uio_npc)
        .to(time,{opacity:0})
        .start()

        tween(uio_my)
        .to(time,{opacity:255})
        .call(()=>{
            self.npc.node.active = false;

            self._touchNext = false;
        })
        .start()

        self.my.spriteFrame = self.role_sf[JCCS.instance.RoleIndex-1];

        self.scheduleOnce(()=>{
            self.upDaAnSpot();            
        },0.02)

    }

    private onConfirm()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();

        if(self._daan_index == -1)
        {
            showTip(JCCS.instance.MainNode,'请选择答案');
            return;
        }
        //let uio = self.node.getComponent(UIOpacity);
        //console.log(self.myuio);

        JCCS.instance.SendServer.sendAnswer({id:self._tiID+1,index:self._daan_index+1},(res:any)=>{

            res = JSON.parse(res);

            if(res.result || !res.score)
            {
                
                self.node.removeFromParent();
                JCCS.instance.dakaID++;
                JCCS.instance.mapNode.upSpotUI();
                JCCS.instance.checkin();
            }else{
                tween(self.myuio)
                .to(0.5,{opacity:0})
                .call(()=>{
                    self.node.removeFromParent();  
                    addDanChu(JCCS.instance.MainS,PAGE_NAME.GET_ITEM,res.score+100);                      
                })
                .start()                
            }
            
        })


    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
