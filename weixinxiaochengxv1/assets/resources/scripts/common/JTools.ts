import { assetManager, Node, SpriteFrame, Texture2D } from "cc";
import { JCCS } from "../JCC";
import { PAGE_NAME, PAGE_PATH } from "../JResource";
import { JComponent } from "./JComponent";

class JTools{
    public loadings:number[] = [];
}

let TOOLS = new JTools();

export function showTip(pNode:Node,str:string)
{
    let node = JCCS.instance.LoadManage.getNode(PAGE_NAME.TIP);
    if(!node)
    {
        JCCS.instance.LoadManage.loadPrefab(PAGE_NAME.TIP,()=>{
            
            node = JCCS.instance.LoadManage.getNode(PAGE_NAME.TIP);
            node.j_convey_data = str;
            pNode.addChild(node);

        },PAGE_PATH)
    }else{
        node.j_convey_data = str;
        pNode.addChild(node);
    } 

    JCCS.instance.MainS.scheduleOnce(()=>{
        node.removeFromParent();
    },2);

}

export function showLoading(index:number = 0)
{
    if(!JCCS.instance.LoadingPage)return;
    if(TOOLS.loadings.indexOf(index) >=0)
    {
        return;
    }
    TOOLS.loadings.push(index);
    console.log('loading==开启')
    let node = JCCS.instance.LoadManage.getNode(PAGE_NAME.LOADING);
    if(!node)
    {
        JCCS.instance.LoadManage.loadPrefab(PAGE_NAME.LOADING,()=>{

            if(TOOLS.loadings.length)
            {
                node = JCCS.instance.LoadManage.getNode(PAGE_NAME.LOADING);
                if(index==9999)
                {
                    node.j_convey_data = true;
                }
                JCCS.instance.LoadingPage.addChild(node);                
            }

        },PAGE_PATH)
    }else{
        if(index==9999)
        {
            node.j_convey_data = true;
        } 
        JCCS.instance.LoadingPage.addChild(node);
    } 

}

export function HideLoading(index:number = 0)
{
    let i = TOOLS.loadings.indexOf(index);
    if(i>=0)
    {
        TOOLS.loadings.splice(i,1);
    }

    if(TOOLS.loadings.length ==0)
    {
        console.log('loading==关闭')
        let n = JCCS.instance.LoadingPage as Node;
        n.removeAllChildren();
    }

}


export function addDanChu(pCom:JComponent,pageName:PAGE_NAME,data:any = null)
{
    let node = JCCS.instance.LoadManage.getNode(pageName);
    if(!node)
    {
        JCCS.instance.LoadManage.loadPrefab(pageName,()=>{
            
            node = JCCS.instance.LoadManage.getNode(pageName);
            node.j_convey_data = data;
            pCom.addPage_danchu(node);

        },PAGE_PATH)
    }else{
        node.j_convey_data = data;
        pCom.addPage_danchu(node);
    }
}

export function getUrlSpriteFrame(url:string,backCall:Function)
{
    assetManager.downloader.downloadDomImage(url, Texture2D ,(err: any, img: any) => {
        if(!err)
        {
            let sf = SpriteFrame.createWithImage(img);
            JCCS.instance.HeadSpriteFrame[url] = sf;
            backCall(sf)              
        }else{
            console.error('加载图片错误：'+err);
        }
    })    
}

//开始经纬度，结束经纬度
export function distance(la1, lo1, la2, lo2) {
    console.log(la1+"||"+lo1+"||"+la2+"||"+lo2);
    
    var lon1 = (Math.PI / 180) * la1;//开始经度
    var lon2 = (Math.PI / 180) * la2;//结束经度
    var lat1 = (Math.PI / 180) * lo1;//开始纬度
    var lat2 = (Math.PI / 180) * lo2;//结束纬度
    // 地球半径
    var R = 6371;
    // 两点间距离 km，如果想要米的话，结果*1000就可以了
    var s = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;
    console.log("计算结果",s);
    return s;
  }

/** 
*随机数组
*/
export function shuffleSwap2(arr:any[]) {
    let randomNumber = ()=>{
        return 0.5-Math.random();
    }
    return arr.sort(randomNumber);
}