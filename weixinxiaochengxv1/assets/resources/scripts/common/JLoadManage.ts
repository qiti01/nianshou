
import { AudioClip, instantiate, Node, Prefab, resources, SpriteFrame, Texture2D } from 'cc';
import { JClass } from './JClass';


import { JEventCenter, JEventName } from './JEventCenter';


type LM_backCallFun = {
    name:string,
    fun:Function,
}

type LM_prefab_d = {
    /**名称*/
    name:string,
    /**路径*/
    path:string,
    /**单个加载完成回调*/
    backCallFuns:LM_backCallFun[]
}

/**回调*/
type T_loadEndBackCall = {
    [key:string]:Function
}
type T_LEBC = {
    [key:string]:T_loadEndBackCall
}

export type LM_img_d = {
    /**路径*/
    path:string,
    /**图片名称*/
    name:string,
}
export type LM_stage_d={
    /**预制体信息*/
    prefabs:LM_prefab_d[],
    /**图片信息数组*/
    imgds:LM_img_d[],
    /**加载数量*/
    length:number,
    /**总加载数量*/
    size:number,
    /**是否加载完成*/
    available:boolean,
    /**阶段完成执行回调列表*/
    backCallFuns:LM_backCallFun[],
}



/**加载管理*/
export class JLoadManage extends JClass {
    private init() {
        let self = this;
        self.name = 'JLoadManage';
    }

    /**预制体*/
    private m_prefab_array:any          = {};
    /**图片SF集合*/
    private m_spriteFrame_array:any     = {};    
    /**已加载数量*/
    private m_load_number               = 0;
    /**加载总数量*/
    private m_load_size                 = 0;
    /**当前加载阶段*/
    private m_load_stage:number         = -1;
    /**加载阶段组合*/
    private m_res:LM_stage_d[]          = [];
    /**加载完成回调*/
    private _loadEndBackCalls:T_LEBC    = {};

    /**是否完成加载某个模块资源*/
    public isRES_completion:boolean[]   = [];

    /**获得当前加载完成阶段*/
    get loadStage(){
        return this.m_load_stage;
    }
    /**获得sf 集合*/
    get SpriteFrameArray(){
        return this.m_spriteFrame_array;
    }
    /**获得node*/
    public getNode(name:string):Node | null{

        if(!this.m_prefab_array[name])
        {
            return null;
        }

        return instantiate(this.m_prefab_array[name]);

    }
    /**获得prefab*/
    public getPrefab(name:string)
    {
        return this.m_prefab_array[name];    
    }
    /**给单个prefab 加载 加入回调
     *@name prefab名称
     *@fun  回调方法
     *@funName 会叫方法名称 识别唯一性 防止反复压入 
    */
    public addPrefab_loadBack(name:string,fun:Function,funName:string){
        let self = this;

        if(!self._loadEndBackCalls[name])
        {
            self._loadEndBackCalls[name] = {};
        }

        self._loadEndBackCalls[name][funName] = fun;

    }
    /**是否存在这个方法*/
    private funsName_indexOf(funs:LM_backCallFun[],funName:string):boolean{
        let self = this;
        for(let i = 0 ; i < funs.length;i++)
        {
            let fd = funs[i];
            if(fd.name == funName)
            {
                return true;
            }
        }

        return false;

    }
    /**获得 spriteFrame*/
    public getSpriteFrame(name:string){

        return this.m_spriteFrame_array[name];

    }
    /**阶段加载完成加入回调
     *@n    阶段
     *@fun  回调方法
     *@funName 会叫方法名称 识别唯一性 防止反复压入 
    */
    public addStageFun(n:number,fun:Function,funName:string){
        let self = this;
        if(!self.m_res.length || !self.m_res[n])return;
        if(self.funsName_indexOf(self.m_res[n].backCallFuns,funName))
        {
            self.j_log('已经加入过同样的回调:'+funName,'本地');
        }else{
            self.m_res[n].backCallFuns.push({
                name:funName,
                fun:fun
            });            
        }

    }
    /**加载音频文件*/
    public resourcesLoad_audio(path:string,backCall:Function)
    {
        let self = this;
        resources.load(path, AudioClip, (err, res) => {
            if(err){
                self.j_error(err);
                return;
            }
            backCall(res);
        });        
    }
    /**基础加载 base */
    private resourcesLoad_prefab(path:string,backCall:Function,name:LM_prefab_d | string){
        let self = this;
        resources.load(path, Prefab, (err, res) => {
            if (err) {
                self.j_error(err);
                return;
            }
            backCall(res,name);
        });
    }
    /**基础加载 base */
    private resourcesLoad_spriteFrame(path:string,backCall:Function,name:string){
        let self = this;
        resources.load(path+'/texture', Texture2D, (err, res) => {
            if (err) {
                self.j_error(err);
                return;
            }
            let spriteFrame = new SpriteFrame();
            spriteFrame.texture = res;
            backCall(spriteFrame,name);
            
        });
    }

    /**加入加载的 大阶段
     * @stage_ds 阶段数组
    */
    public load_stage_ds(stage_ds:LM_stage_d[]){
        let self = this;
        stage_ds.forEach(ds=>{
            self.m_res.push(ds);
        })   
        
        self.loadRes(true);
    }
    /**加载单个 Prefab
    *@name      加载的预制体名称
    *@backCall  加载回调
    */
    public loadPrefab(name:string,backCall:Function,path:string = '')
    {
        let self = this;
        let loadBackCall = function name(params:Prefab,name:string) {     
            self.m_prefab_array[name] = params; 
            backCall();                   
        }
        self.resourcesLoad_prefab(path+name,loadBackCall,name);
    }
    /**加载资源
     * @halfway 是否中途加入 需要加载的资源
    */
    public loadRes(halfway:boolean = false){

        let self = this;

        self.m_load_number  = 0;
        self.m_load_size    = 0;

        let loadIndex = 0;
        //计算总加载
        for(let i = 0 ; i < self.m_res.length;i++){

            let tres    = self.m_res[i];
            if(tres.available)
            {
                loadIndex++;
            }
            let tPrefab = tres.prefabs;
            let tImg    = tres.imgds;

            self.m_load_size    += tPrefab.length;
            self.m_load_size    += tImg.length;
            if(!halfway || i != 0)//考虑到 中途加入的资源 需要计算 块总和 但是0阶段的不需要重新计算
            {
                tres.size           += tPrefab.length; 
                tres.size           += tImg.length;
            }

        }

        self.j_log("加载总和："+self.m_load_size,'本地');

        if(halfway && loadIndex == 0)
        {
            self.j_log('只重新计算加载总和','本地');
        }else{
            self.loadRes_stage(loadIndex);
        }
        

    }
    /**更新加载进度
    *@res  加载阶段信息
    *@n    加载阶段
    *@name 加载完成资源名称
    */
    private upLoadProportion(res:LM_stage_d,n:number,name:string){
        let self = this;

        res.length++;

        if(res.length == res.size)
        {
            res.available = true;
            self.j_log(n+'阶段 加载完成','本地');
            self.m_load_stage = n;

            self.loadRes_stage(n+1);

            if(res.backCallFuns.length)
            {
                for(;res.backCallFuns.length >0;)
                {
                    let fun = res.backCallFuns[0];
                    if(fun)
                    {
                        fun.fun(); 
                        res.backCallFuns.shift();                       
                    }

                }
            }

        }
        

        self.m_load_number++;

        self.isLoadEnd();

    }

    /**判断是否加载完成*/
    public isLoadEnd()
    {
        let self = this;
        let stb = 100;
        if(self.m_load_number != self.m_load_size)
        {
            stb = Math.floor((self.m_load_number/self.m_load_size)*100);
            self.j_log(self.m_load_number + '/' + self.m_load_size,'本地'); 
            JEventCenter.emit(JEventName.upData,{upTypes:['loadProgress']});    
        }else{
            self.j_log('全部资源 加载完成','本地'); 
            JEventCenter.emit(JEventName.upData,{upTypes:['loadAllEnd']});
        }
    }

    /**加载阶段*/
    private loadRes_stage(n:number)
    {
        let self = this;
        let res  = self.m_res[n];
        if(!res)
        {
            return;
        }

        let tPrefab = res.prefabs;
        let tImg    = res.imgds;

        let backCall = function (params:Prefab,tp:LM_prefab_d) {
                       
            self.m_prefab_array[tp.name] = params;
            for(let i = 0 ; i < tp.backCallFuns.length ; i++)
            {
                tp.backCallFuns[i].fun();
            }
            self.upLoadProportion(res,n,tp.name);
            
            for(let key in self._loadEndBackCalls[tp.name])
            {
                self._loadEndBackCalls[tp.name][key]();
            }
            
            delete self._loadEndBackCalls[tp.name];
        }

        for(let i = 0 ; i < tPrefab.length ; i++)
        {
            let tp = tPrefab[i];          
            self.resourcesLoad_prefab('prefab/'+tp.path+tp.name,backCall,tp);
        }


        let loadBackCall = function (params:SpriteFrame,name:string) {

            self.m_spriteFrame_array[name] = params;
            self.upLoadProportion(res,n,name);          

        }

        for(let k = 0 ; k < tImg.length ; k++){

            let tname = tImg[k];
            self.resourcesLoad_spriteFrame(tname.path,loadBackCall,tname.name);
        }        

    }


    /**加载单个模块*/
    public loadRes_single(res:LM_stage_d,progressBackCall:Function)
    {
        let self = this;

        let tPrefab   = res.prefabs;
        let tImg      = res.imgds;
        res.size      = tPrefab.length + tImg.length;
        res.length    = 0;
        res.available = false;

        let isEnd = ()=>{
            res.length++;
            if(res.length >= res.size)
            {
                res.available = true;
                //self.isRES_completion[res.moduleType] = true;
            }
            progressBackCall(res.length,res.size);

        }

        let backCall = function (params:Prefab,tp:LM_prefab_d) {                       
            self.m_prefab_array[tp.name] = params;
            for(let i = 0 ; i < tp.backCallFuns.length ; i++)
            {
                tp.backCallFuns[i].fun();
            }
            if(self._loadEndBackCalls[tp.name])
            {
                for(let key in self._loadEndBackCalls[tp.name])
                {
                    self._loadEndBackCalls[tp.name][key]();
                }
                
                delete self._loadEndBackCalls[tp.name];                 
            }

            isEnd();
        }

        for(let i = 0 ; i < tPrefab.length ; i++)
        {
            let tp = tPrefab[i];          
            self.resourcesLoad_prefab(tp.path+tp.name,backCall,tp);
        }

        let loadBackCall = function (params:SpriteFrame,name:string) {
            self.m_spriteFrame_array[name] = params;      
            isEnd();
        }

        for(let k = 0 ; k < tImg.length ; k++){
            let tname = tImg[k];
            self.resourcesLoad_spriteFrame(tname.path,loadBackCall,tname.name);
        }        

    }



}