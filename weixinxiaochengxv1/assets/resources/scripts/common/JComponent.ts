
import { _decorator, Component, Node, UIOpacity, tween } from 'cc';
import { JLogManage } from './JLogManage';

const { ccclass, property } = _decorator;

@ccclass('JComponent')
export  class JComponent extends Component {

    /**log管理*/
    private logm:JLogManage = JLogManage.instance;
    //protected _showTime:number = 0.2;
        
    protected j_log(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'log');
    } 
    protected j_error(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'error');        
    }
    protected j_warn(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'warn');        
    }

    
    public addPage_danchu(node:Node,endBackCall:Function | null = null)
    {
        let self = this;

        self.node.addChild(node);

        let showTime = node.j_showTime;

        if(!showTime){
            if(endBackCall)endBackCall();
        }else{
            let uio = node.getComponent(UIOpacity);
            if(!uio)
            {
                uio = node.addComponent(UIOpacity);
            }

            uio.opacity = 0;
            tween(uio)
            .to(showTime,{opacity:255})
            .call(()=>{
                if(endBackCall)endBackCall();
            })
            .start();            
        }
        

    }


    protected hide_danchu(node:Node,time:number,endBackCall:Function | null = null)
    {
        let self = this;
        let uio = node.getComponent(UIOpacity);
        if(!uio)
        {
            uio = node.addComponent(UIOpacity);
        }

        uio.opacity = 255;
        tween(uio)
        .to(time,{opacity:0})
        .call(()=>{
            if(endBackCall)endBackCall();
        })
        .start();  
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
