const { jsQR } = require("./jsqr.min");

var wxLogin = function(backCall){
    wx.login({
        success: function(res)
        {
            // 获取用户信息
            wx.getUserInfo({
                withCredentials: true,      // 必须在wx.login之后，这里才能为true
                success: function(result)
                {
                    console.log(result);
                    let pl = JSON.parse(result.rawData);
                    let sendstr ={}; 
                    sendstr.code = res.code;
                    sendstr.city = pl.city;
                    sendstr.country = pl.country;
                    sendstr.gender  = pl.gender;
                    sendstr.nickName = pl.nickName;
                    sendstr.avatar   = result.userInfo.avatarUrl;
                    sendstr.language = pl.language;
        
                    console.log(sendstr);
                    backCall(sendstr);
  
                },
                fail: function(result)
                {
                  console.error(result);
                    // 错误处理
                },
            });
        },
        fail: function(res)
        {
          console.error(res);
            // 错误处理
        },
    });    
}

var wxGetLocation = function(backCall){

  wx.getSetting({
    success(res)
    {
      // 获取用户位置
      wx.getLocation({
        type: 'gcj02',
        success: function (res) {
            //console.log('res', res)
            let latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
            let longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
            var speed = res.speed; // 速度，以米/每秒计
            var accuracy = res.accuracy; // 位置精度
            
            console.log('-------')
            console.log(latitude);
            console.log(longitude);
            console.log(speed);
            console.log(accuracy);

            backCall({latitude:latitude,longitude:longitude,accuracy:accuracy})

        },
        fail: function (res) {
            backCall({error:'获取失败'})
        }
      });
    }
});   
 

}

window.wxGetLocation = wxGetLocation;

var wxGetSetting = function(backCall){

    // wx.setEnableDebug({
    //   enableDebug: true
    // });  

    wx.getSetting({
        success(res)
        {
          // 已授权
          if (res.authSetting["scope.userInfo"])
          {
            console.log(res);
            console.log('执行登录1');
            wxLogin(backCall);
          }
          // 显示授权按钮
          else
          {
            let sysInfo = wx.getSystemInfoSync();
            let button = wx.createUserInfoButton(
                {
                type: "text",
                text: "微信登录",
                style: {
                  left: sysInfo.windowWidth / 2 - 75,
                  top:  sysInfo.windowHeight / 2,
                  width: 140,
                  height: 60,
                  backgroundColor: "#FDF6DA",
                  color:           "#940B0B",
                  borderColor:     "#DF631E",
                  textAlign:       "center",
                  fontSize: 16,
                  borderWidth: 1,
                  borderRadius: 1,
                  lineHeight: 60,
                }
              }
            );
            button.onTap(function(res)
            {
              if (res.userInfo)
              {
                console.log(res);
                console.log('执行登录2');
                button.destroy();
                wxLogin(backCall);
              }
              else
              {
                wx.showModal({
                  title: "温馨提示",
                  content: "游戏需要您的用户信息登录游戏。",
                  showCancel: false,
                });
              }
            });
            button.show();
          }



        }
    });    
}

window.wxGetSetting = wxGetSetting;

var wxScanCode=(backCall)=>{
  // 只允许从相机扫码
  wx.scanCode({
    //onlyFromCamera: true,
    success (res) {
      //console.log(res)
      backCall(res);
    }
  })  
}
window.wxScanCode = wxScanCode;



function loadScript(url, callback){
  var script = document.createElement ("script")
  script.type = "text/javascript";
  if (script.readyState){ //IE
      console.log('走了1');
      script.onreadystatechange = function(){
          if (script.readyState == "loaded" || script.readyState == "complete"){
              script.onreadystatechange = null;
              callback();
          }else{
            console.log('走了3');
          }
      };

  } else { //Others
      console.log('走了2');
      script.onload = function(){
          callback();
      };
  }
  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
}


/**腾讯地图初始化*/
var qqMapInit=()=>{

  if(!window.qqMap)
  {
    // let url = "https://apis.map.qq.com/tools/geolocation/min?key=VHPBZ-T733X-KQY47-7HJWB-PGXVF-HVFDH&referer=myapp";
    // console.log('加载腾讯地图插件')
    // loadScript(url,()=>{
      console.log('插件加载完成')
      window.qqMap = new qq.maps.Geolocation("VHPBZ-T733X-KQY47-7HJWB-PGXVF-HVFDH","myapp");
      showWatchPosition();    
      qqGetCurLocation();
    //})

  }else{
    console.warn('腾讯地图插件已存在')
  }

}

var options = {timeout: 9000};
var positionNum = 0;

/**获得当前坐标*/
function qqGetCurLocation() {
    console.log('获得位置');
    window.qqMap.getLocation(showPosition, showErr, options);
}
/**坐标获取成功反馈*/
function showPosition(position) {
  if(window.LocationBackCall)
  {
    window.LocationBackCall(position);
  }
};

function showErr() {
  console.error('定位失败!')
};

function showWatchPosition() {
  console.log('定位开始监听')
  window.qqMap.watchPosition(showPosition);
};

function showClearWatch() {
  console.log('定位监听关闭')
  window.qqMap.clearWatch();
};

window.qqGetCurLocation = qqGetCurLocation;

window.qqMapInit = qqMapInit;



function seriesLoadScripts(scripts,callback) {
	if(typeof(scripts) != "object") var scripts = [scripts];
	var HEAD = document.getElementsByTagName("head").item(0) || document.documentElement;
	var s = new Array(), last = scripts.length - 1, recursiveLoad = function(i) {  //递归
		s[i] = document.createElement("script");
		s[i].setAttribute("type","module");
		s[i].onload = s[i].onreadystatechange = function() {  //Attach handlers for all browsers
			if(!/*@cc_on!@*/0 || this.readyState == "loaded" || this.readyState == "complete") {
				this.onload = this.onreadystatechange = null; this.parentNode.removeChild(this);
				if(i != last) recursiveLoad(i + 1); else if(typeof(callback) == "function") callback();
			}
		}
		s[i].setAttribute("src",scripts[i]);
		HEAD.appendChild(s[i]);
	};
	recursiveLoad(0);
}

var timer  = null;
var video  = null;
var cvsele = null;
var canvas = null;
var isAnimation = false;
var result = null;

let cance=()=>{
  isAnimation = false;
  if(timer)
  {
    cancelAnimationFrame(timer);
    setTimeout(() => {
      cvsele.style.display = "none";
      video.style.display  = "none";
    }, 1000);    
  }
  if(video)
  {
    
  }
}

/**坐标获取成功反馈*/
function setQRCodeBackCall(position) {
  if(window.QRCodeBackCall)
  {
    window.QRCodeBackCall(position);
  }
};

let sweep=()=>{
  if(!video)return;

  // console.log('--------------');
  // console.log(video.readyState);
  // console.log(video.HAVE_ENOUGH_DATA);
  // console.log(canvas);
  // console.log('==============');
  // console.log(jsQR);
  // console.log(window.jsQR);

  if (video.readyState === video.HAVE_ENOUGH_DATA && canvas) {
    const { videoWidth, videoHeight } = video;
    cvsele.width = videoWidth;
    cvsele.height = videoHeight;
    canvas.drawImage(video, 0, 0, videoWidth, videoHeight);
    try {
      const img = canvas.getImageData(0, 0, videoWidth, videoHeight);
      const obj = window.jsQR(img.data, img.width, img.height, {
        inversionAttempts: "dontInvert",
      });
      if (obj) {
        if (result != obj.data) {
          cance();
          console.log("识别结果：", obj.data);
          setQRCodeBackCall(obj.data);
        }
      }
    } catch (err) {
      //console.error("识别失败，请检查二维码是否正确！", err);
      cance();
    }
  }
  if (isAnimation) {
    timer = requestAnimationFrame(() => {
      sweep();
    });
  }
}

var QRCodeInit = (vSize)=>{


  //let div = document.createElement('div');
  console.log(document.getElementById('GameDiv'));
  //console.log(document.getElementById('splash'));
  //console.log(document.getElementsByClassName('cocosVideo'));
  //let div = document.getElementById('GameDiv');
  //let div = document.getElementById('splash');
  //let div = document.createElement('div');
  let div = document.getElementById('Cocos3dGameContainer');
  video  = document.createElement("video");

  //cvsele = document.createElement('canvas');
  cvsele = document.getElementById('cocosVideo');

  //div.appendChild(cvsele);  
  if(!cvsele)return;
  //cvsele.style.display     = "block";
  //cvsele.style.visibility    = "visible";

//-----------------
  cvsele.style.width         = vSize.w+'px';
  cvsele.style.height        = vSize.h+'px';
  cvsele.style.objectFit     = 'fill';
  cvsele.style.transform     = 'matrix(0.552,0,0,0.551667,-5.68434e-14,-2.84217e-14)';
  canvas = cvsele.getContext('2d');
  cvsele.tabIndex = -10;
  cvsele.style.position    = 'absolute';
  cvsele.style.bottom      = '0px';
  cvsele.style.left        = '0px';
  cvsele.style['transform-origin'] = '0px 100% 0px';
  cvsele.style['-webkit-transform-origin'] = '0px 100% 0px';
  cvsele.setAttribute('preload', 'auto');
  cvsele.setAttribute('webkit-playsinline', '');
  cvsele.setAttribute('x5-playsinline', '');
  cvsele.setAttribute('playsinline', '');
  cvsele.setAttribute("z-index", "-2");

//---------------------------

  // video.style.position    = 'absolute';
  // video.style.bottom      = '0px';
  // video.style.left        = '0px';
  // video.style['transform-origin'] = '0px 100% 0px';
  // video.style['-webkit-transform-origin'] = '0px 100% 0px';
  // video.setAttribute('preload', 'auto');
  // video.setAttribute('webkit-playsinline', '');
  // video.setAttribute('x5-playsinline', '');
  // video.setAttribute('playsinline', '');
  // video.setAttribute("width", "100%");
  // video.setAttribute("height", "100%");

  //div.appendChild(video);

  isAnimation = true;

  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  if (navigator.mediaDevices) {
    navigator.mediaDevices.getUserMedia({
      video: { facingMode: "environment" },
    }).then((stream) => {
      video.srcObject = stream;
      video.setAttribute("playsinline", true);
      video.setAttribute("webkit-playsinline", true);
      video.addEventListener("loadedmetadata", () => {
        video.play();
        sweep();
      });
    }).catch((error) => {
      console.log('对不起：未识别到扫描设备!1');
      console.log(error.code + "：" + error.name + "，" + error.message);
    });
  } else if (navigator.getUserMedia) {
    navigator.getUserMedia({
        video: { facingMode: "environment" },
      }, (stream) => {
        video.srcObject = stream;
        video.setAttribute("playsinline", true);
        video.setAttribute("webkit-playsinline", true);
        video.addEventListener("loadedmetadata", () => {
          video.play();
          sweep();
        });
      }, (error) => {
        console.log('对不起：未识别到扫描设备!2');
        console.log(error.code + "：" + error.name + "，" + error.message);
      }
    );
  } else {
    console.log("对不起：未识别到扫描设备!3");
  }

}

window.QRCodeInit = QRCodeInit;

var launchFullScreen = ()=>{
  let element = document.documentElement;
  // 先检测最标准的方法
  if (element.requestFullScreen) {
      element.requestFullScreen();
  } else if (element.mozRequestFullScreen) {
      // 其次，检测Mozilla的方法
      element.mozRequestFullScreen();
  } else if (element.webkitRequestFullScreen) {
      // if 检测 webkit的API
      element.webkitRequestFullScreen();
  }

  console.log('执行了');
}
window.launchFullScreen = launchFullScreen;










var wxSDKinit=()=>
{
  console.log('微信SDK初始化')
  wx.config({
    debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: 'wxf8f88e804ccada86', // 必填，公众号的唯一标识
    timestamp: '', // 必填，生成签名的时间戳
    nonceStr: '', // 必填，生成签名的随机串
    signature: '',// 必填，签名
    jsApiList: [] // 必填，需要使用的JS接口列表
  });
  wx.ready(function(){
    // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
  });
  wx.error(function(res){
    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
    console.error(err);
  });



}
