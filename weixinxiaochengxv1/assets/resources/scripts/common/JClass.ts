import { JLogManage } from "./JLogManage";

/**基础类*/
export class JClass {

    private _name:string = '';

    public get name(){
        return this._name;
    }
    public set name(str:string){
        this._name = str;
    }
    /**log管理*/
    private logm:JLogManage = JLogManage.instance;

    protected j_log(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'log');
    } 
    protected j_error(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'error');        
    }
    protected j_warn(obj:any,name:string = ''){
        this.logm.log(this.name,name,obj,'warn');        
    }


}