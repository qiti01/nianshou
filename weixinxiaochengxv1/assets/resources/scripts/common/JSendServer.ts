import { JCCS } from "../JCC";
import { JClass } from "./JClass";
import { HideLoading, showLoading, showTip } from "./JTools";


let http_url = 'https://app.17boluo.com/';

export enum caozuoType{
    /**开始*/
    start=1,
    /**跳过*/
    guide_skip,
    /**完成引导*/
    guide_complete,
    /**付费*/
    pay,
    /**分享*/
    share,
    /**宝箱*/
    treasure_box,
    /**完成了所有*/
    all_complete, 

}

/**服务器消息派发*/
export class JSendServer extends JClass {

    public tou = '';

    public TestBackCall:Function | null = null;

    public getUrlParam(name)
    {
        // let reg = new RegExp('(^|&)'+name+'=([^&]*)(&|$)');
        // let r = window.location.search.substring(1).match(reg)
        // if(r!=null)return unescape(r[2])
        // return null;

        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [, ''])[1]
					.replace(/\+/g, '%20')) || null
    }

    public httpRequest_wx(backCall:Function | null = null)
    {
        const code = this.getUrlParam('code');
        if(code == null || code == '')
        {
            console.log('请求code');
            window.location.href = "https://open.weixin.qq.com/connect/qrconnect?appid=wx7f78db24bec0ecdd&redirect_uri=https://data.17boluo.com/tour/&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect"
        }else{
            console.log('code:'+code);

        }
     
    } 


    public httpRequest(url:string,obj:object,backCall:Function | null = null)
    {
         let self = this;
         var httpRequest = new XMLHttpRequest();//第一步：创建需要的对象
         httpRequest.open('POST',http_url + url, true); //第二步：打开连接
         /**
         *发送json格式文件必须设置请求头 ；如下 - 
         */
         httpRequest.setRequestHeader("Content-type","application/json");//设置请求头 注：post方式必须设置请求头（在建立连接后设置请求头）
         if(self.tou != '')
         {
           //console.log('加入头请求:'+self.tou);
           httpRequest.setRequestHeader("Session-Key",self.tou);
         }
         console.log('http请求数据:'+JSON.stringify(obj));

         showLoading();
         httpRequest.send(JSON.stringify(obj));//发送请求 将json写入send中
         /**
          * 获取数据后的处理程序
          */
         httpRequest.onreadystatechange = function () {//请求后的回调接口，可将请求成功后要执行的程序写在其中
             //self.j_log('httpRequest.readyState:'+httpRequest.readyState+'   httpRequest.status:'+httpRequest.status)
             if (httpRequest.readyState == 4 && httpRequest.status == 200) {//验证请求是否发送成功
                 var json = httpRequest.responseText;//获取到服务端返回的数据
                 console.log(json); 
                 if(backCall)backCall(json);
                HideLoading();
                 if(self.TestBackCall)self.TestBackCall(JSON.stringify(obj),json);

             }
         };       
    } 


    public httpRequest_get(url:string,backCall:Function | null = null)
    {
         let self = this;
         var httpRequest = new XMLHttpRequest();//第一步：创建需要的对象
         httpRequest.open('GET',url, true); //第二步：打开连接
         /**
         *发送json格式文件必须设置请求头 ；如下 - 
         */
         //httpRequest.setRequestHeader("Content-type","application/json");//设置请求头 注：post方式必须设置请求头（在建立连接后设置请求头）

         httpRequest.send();//发送请求 将json写入send中
         /**
          * 获取数据后的处理程序
          */
         httpRequest.onreadystatechange = function () {//请求后的回调接口，可将请求成功后要执行的程序写在其中
             //self.j_log('httpRequest.readyState:'+httpRequest.readyState+'   httpRequest.status:'+httpRequest.status)
             if (httpRequest.readyState == 4 && httpRequest.status == 200) {//验证请求是否发送成功
                 var json = httpRequest.responseText;//获取到服务端返回的数据
                 console.log('----------------------h5登录返回');
                 self.j_log(json); 
                 if(backCall)backCall(json);
             }
         };       
    } 


    public h5Login(code:any,backCall:Function)
    {
        let self = this;
        console.log('h5登录')
        self.httpRequest('tour/h5login',{code:code},(res:any)=>{
            res = JSON.parse(res);
            console.log(res);
            JCCS.instance.MyUID     = res.uid;
            self.tou                = res.session;
            JCCS.instance.myName    = res.nickname;
            JCCS.instance.myHeadUrl = res.avatar;
        });
    }

    public gzhLogin(code:any,backCall:Function)
    {
        let self = this;
        console.log('公众号登录')
        self.httpRequest('tour/gzhLogin',{code:code},(res:any)=>{
            res = JSON.parse(res);
            console.log(res);
            JCCS.instance.MyUID     = res.uid;
            self.tou                = res.session;
            JCCS.instance.myName    = res.nickname;
            JCCS.instance.myHeadUrl = res.avatar;
        });
    }


    /**登录*/
    public sendLogin(obj:any,backCall:Function)
    {
        let self = this;
        console.log('登录请求')
        self.httpRequest('tour/login',obj,(res:any)=>{
            let t = JSON.parse(res);

            if(t)
            {

            }

            backCall(res);
        });
    }



    /**使用门票*/
    public useTicket(ticket:string,backCall:Function)
    {
        let self = this;
        console.log('使用门票请求')
        JCCS.instance.SendServer.sendCaozuo(caozuoType.pay);
        self.httpRequest('tour/enter',{ticket:ticket},backCall);
    }

    /**打卡*/
    public checkin(id:string,backCall:Function)
    {
        let self = this;
        console.log('打卡请求')
        self.httpRequest('tour/checkin',{id:id},backCall);
    }


    /**获得当前拥有道具*/
    public getItems(backCall:Function | null = null)
    {
        let self = this;
        console.log('获得拥有道具')
        self.httpRequest('tour/items',{},(data:any)=>{
            let obj = JSON.parse(data);
            console.log('服务器返回的道具：'+data);
            if(!obj.items)
            {
                JCCS.instance.Items = [];   
            }else{
                JCCS.instance.Items = obj.items;                
            }

            if(backCall)backCall(JCCS.instance.Items);
        });
    }

    /**送走年兽*/
    public songZou(id:number,item_:number[],backCall)
    {
        let self = this;
        console.log('送走年兽请求')
        self.httpRequest('tour/compose',{monster:id,items:item_},backCall);
    }    

    /**引导数据*/
    public isGuide(backCall:Function)
    {
        let self = this;
        console.log('获得引导数据')
        self.httpRequest('tour/guide',{},backCall);
    }

    /**做过引导*/
    public setGuide()
    {
        let self = this;
        console.log('引导完成请求')
        JCCS.instance.SendServer.sendCaozuo(caozuoType.guide_complete);
        self.httpRequest('tour/guide',{finished:1},()=>{

        });
    }

    /**灵宠数据*/
    public getLingChong(backCall:Function)
    {
        let self = this;
        console.log('灵宠数据请求')
        self.httpRequest('tour/pet',{},(res:any)=>{
            backCall(res);
        });
    }

    /**选择灵宠*/
    public setLingChong(backCall:Function)
    {
        let self = this;
        console.log('确定选择灵宠请求')
        self.httpRequest('tour/pet',{id:JCCS.instance.LingChong},backCall);
    }

    /**初始化角色数据*/
    public userClear()
    {
        let self = this;
        console.log('初始化角色数据')
        self.httpRequest('tour/clear',{id:0},()=>{
            showTip(JCCS.instance.MainNode,'初始化数据成功');
        });
    }

    /**抽奖*/
    public LuckyDraw(monster:any,backCall:Function)
    {
        let self = this;
        console.log('抽奖请求')
        self.httpRequest('tour/lottery',{monster:monster},backCall);
    }

    /**抽奖记录*/
    public LuckyDraw_record(backCall:Function)
    {
        let self = this;
        console.log('抽奖记录请求')
        self.httpRequest('tour/awards',{},backCall);
    }

    /**发送聊天*/
    public sendChatMsg(msg:string,backCall:Function)
    {
        let self = this;
        console.log('发送聊天')

        self.httpRequest('tour/chat',{msg:msg},(res:any)=>{
            let re = JSON.parse(res);
            if(re.result)
            {

            }else{
                backCall();
            }
        });  


    }

    /**获取聊天记录*/
    public getChatMsg(backCall:Function)
    {
        let self = this;
        console.log('获得聊天记录');
        self.httpRequest('tour/board',{size:10},backCall);           
    }


    /**获得token*/
    public getToken(backCall:Function)
    {
        let self = this;
        console.log('获得token');
        self.httpRequest('tour/token',{},backCall);
    }


    /**获得已驱赶年兽*/
    public getComposed(backCall:Function)
    {
        let self = this;
        console.log('获得已送走年兽')
        self.httpRequest('tour/getComposed',{},backCall);
    }

    /**拉取用户列表*/
    public getUserList(backCall:Function)
    {
        let self = this;
        self.httpRequest('tour/chatUsers',{},backCall);
    }

    /**点赞*/
    public like(uid:any,backCall)
    {
        let self = this;
        self.httpRequest('tour/like',{user:uid},backCall);
    }

    /**发送地点*/
    public setPoint(data:{name:string,lat:any,lon:any},backCall)
    {
        let self = this;
        self.httpRequest('tour/setPoint',data,backCall);
    }

    /**发送操作*/
    public sendCaozuo(caozuo:caozuoType)
    {
        let self = this;
        self.httpRequest('tour/setPoint',{name:JCCS.instance.myName,lat:JCCS.instance.MyUID,lon:caozuo},(res:any)=>{
            res = JSON.parse(res);
            if(res.result)
            {
                console.log('操作上传失败')
            }else{
                console.log('操作上传成功')
            }
        });
    }    

    /**发送地点*/
    public getPoint(backCall:Function | null = null)
    {
        let self = this;
        self.httpRequest('tour/getPoints',{},(res:any)=>{
            console.log('返回已保存的地图坐标');
            console.log(res)
            //showTip(JCCS.instance.MainNode,res);   
            if(backCall)backCall(JSON.parse(res))
        });
    }



    /**获得已选择角色*/
    public getRole(backCall:Function)
    {
        let self = this;
        console.log('角色数据请求')

        self.httpRequest('tour/role',{},(res:any)=>{
            backCall(res);
        });
    }

    /**选择角色*/
    public setRole(backCall:Function)
    {
        let self = this;
        console.log('确定选择角色请求')
        self.httpRequest('tour/role',{id:JCCS.instance.RoleIndex},backCall);
    }

    
    /**答题*/
    public sendAnswer(obj:any,backCall:Function)
    {
        let self = this;
        console.log('答题')
        self.httpRequest('tour/answer',{question:obj.id,answer:obj.index},backCall);
    }

    /**获取积分*/
    public getScore(backCall:Function)
    {
        let self = this;
        console.log('获取积分')
        self.httpRequest('tour/getScore',{},backCall);
    }

    /**积分兑换抽奖机会*/
    public sendScore2Chance(obj:any,backCall:Function)
    {
        let self = this;
        console.log('积分获得抽奖机会')
        self.httpRequest('tour/score2Chance',{score:obj.score,chance:obj.chance},backCall);
    } 

    /**获取红包码-单个*/
    public getRedeem(backCall:Function)
    {
        let self = this;
        console.log('获取获取红包码-单个')
        self.httpRequest('tour/redeem',{},backCall);
    }

    /**获取红包码-多个*/
    public getRedeemCodes(backCall:Function)
    {
        let self = this;
        console.log('获取获取红包码-多个')
        self.httpRequest('tour/getRedeemCodes',{},backCall);
    }

}