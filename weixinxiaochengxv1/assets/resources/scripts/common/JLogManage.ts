
/**输出管理器*/
export class JLogManage {

    private init() {
        let self = this;
        
    }
    /**是否显示log*/
    public ShowLog:boolean = false;
    public isServer:boolean= false;
    private static _instance: JLogManage;
    public static get instance(): JLogManage {
        if (!JLogManage._instance) {
            JLogManage._instance = new JLogManage();
            JLogManage._instance.init();
        }
        return JLogManage._instance;
    }   
    /**所有log集合*/
    private m_all_logs:     string[]    = [];
    /**错误log集合*/
    private m_error_logs:   string[]    = [];
    /**警告log集合*/
    private m_warn_logs:    string[]    = [];
    /**log储存最大数量*/
    private m_log_size:     number      = 20;
    /**设置储存log数量*/
    public set LogSize(n:number){
        this.m_log_size = n;
    }
    public get AllLogs()
    {
        return JSON.stringify( this.m_all_logs );
    }
    /**
    *@className 类的名称
    *@describe  log 说明
    *@obj       输出内容
    *@type      输出类型
    */
    public log(className:string,describe:string,obj:any,type:string){

        let self = this;

        if(self.ShowLog)
        {
            switch(type)
            {
                case 'log':
                    if(typeof obj == 'string')
                    {
                        console.log('------ '+className + ' >>>'+ describe +'------ ['+obj+']');
                    }else{
                        console.log('------↓-------'+className+'------↓-------'+describe);
                        console.log(obj);
                        console.log('------↑-------'+className+'------↑-------'+describe);
                    }                    
                    break;
                case 'error':
                    if(typeof obj == 'string')
                    {
                        console.error('------ '+className + ' >>>'+ describe +'------ ['+obj+']');
                    }else{
                        console.error('------↓-------'+className+'------↓-------'+describe);
                        console.error(obj);
                        console.error('------↑-------'+className+'------↑-------'+describe);
                    } 
                    break;
                case 'warn':
                    if(typeof obj == 'string')
                    {
                        console.warn('------ '+className + ' >>>'+ describe +'------ ['+obj+']');
                    }else{
                        console.warn('------↓-------'+className+'------↓-------'+describe);
                        console.warn(obj);
                        console.warn('------↑-------'+className+'------↑-------'+describe);
                    }                     
                    break;
            }

        }

        switch(type)
        {
            case 'log':

                break;
            case 'error':
                if(typeof obj == 'string')
                {        
                    self.m_error_logs.push(className+'->'+describe+'['+obj+']');
                }else{
                    self.m_error_logs.push(className+'->'+describe+'['+JSON.stringify( obj )+']');
                }                
                break;
            case 'warn':
                if(typeof obj == 'string')
                {        
                    self.m_warn_logs.push(className+'->'+describe+'['+obj+']');
                }else{
                    self.m_warn_logs.push(className+'->'+describe+'['+JSON.stringify( obj )+']');
                }
                break;
        }

        if(typeof obj == 'string')
        {        
            self.m_all_logs.push(className+'->'+describe+'['+obj+']');
        }else{
            self.m_all_logs.push(className+'->'+describe+'['+JSON.stringify( obj )+']');
        }

        if(self.m_all_logs.length > self.m_log_size)
        {
            self.m_all_logs.shift();
        }
        if(self.m_error_logs.length > self.m_log_size)
        {
            self.m_error_logs.shift();
        }
        if(self.m_warn_logs.length > self.m_log_size)
        {
            self.m_warn_logs.shift();
        }

    }


}
