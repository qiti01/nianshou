/**消息名称集合*/
export enum JEventName {
    /**跳转页面*/
    pageJump = 1,
    /**服务器下发*/
    serverMsg ,
    /**请求服务器*/
    sendMsg   ,
    /**更新数据*/
    upData    ,
    /**声音回调*/
    voiceControl ,
    /**语言更新*/
    upTextType,
    /**打牌*/
    playCard,
    /**中控*/
    CCS,
}

type t_eventFD = {
    fun:Function,
    id:string,
}

type t_eventFDs = {
    [key:number]:t_eventFD[]
}

class Jevent{



    private _eventArray:t_eventFDs = {};
    on(name:JEventName,fun:Function,p:string){
        let self = this;
        if(!self._eventArray[name] || -1 == self.find_abs(name,p))
        {
            self.in(name,fun,p);
        }
    }
    off(name:JEventName,p:string){
        let self = this;
        if(!self._eventArray[name])return;
        self.remove_abs(name,p);
    }
    emit(name:JEventName,data:any = null)
    {
        let self = this;
        let funs = self._eventArray[name];
        if(!funs)return;
        for(let i = 0 ; i < funs.length ;i++)
        {
            funs[i].fun(data);
        }
    }
    private in(name:JEventName,fun:Function,id:string){
        let self = this;
        if(!self._eventArray[name])
        {
            self._eventArray[name] = []
        }
        self._eventArray[name].push({
            fun:fun,
            id:id,
        });
    }

    private remove_abs(name:JEventName,id:string)
    {
        let self = this;
        let a = self.find_abs(name,id);
        if(a != -1){
            self._eventArray[name].splice(a,1);
        }     
    }

    private find_abs(name:JEventName,id:string):number{
        if(!this._eventArray[name])return -1;

        for(let i = 0 ;i < this._eventArray[name].length;i++)
        {
            let a = this._eventArray[name][i];
            if(a.id == id)
            {
                return i;
            }            
        }

        return -1;
    }

}

export const JEventCenter: Jevent = new Jevent();