
import { _decorator, Component, Node, Sprite, SpriteFrame, Label } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JGetItem
 * DateTime = Tue Dec 07 2021 14:59:52 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JGetItem.ts
 * FileBasenameNoExtension = JGetItem
 * URL = db://assets/resources/scripts/JGetItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JGetItem')
export class JGetItem extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Sprite)
    spriteItem:Sprite | null;

    @property(SpriteFrame)
    itemSF1:SpriteFrame[] = [];
    @property(SpriteFrame)
    itemSF2:SpriteFrame[] = [];
    @property(SpriteFrame)
    itemSF3:SpriteFrame[] = [];
    
    @property(Label)
    nameLabel:Label | null = null;

    @property(String)
    names:string[]=[];

    @property(SpriteFrame)
    jifensf:SpriteFrame;

    start () {
        // [3]
        let self = this;
        let index = Number(self.node.j_convey_data);


        if(index>100)
        {
            self.spriteItem.spriteFrame = self.jifensf;
            self.nameLabel.string = '生财积分x'+(index-100);
            return;
        }

        let items:SpriteFrame[] =[];

        let type = Math.floor( index/10 );
        switch(type)
        {
            case 1:
                items = self.itemSF1;
                break;
            case 2:
                items = self.itemSF2;
                break;
            case 3:
                items = self.itemSF3;
                break;
        }

        self.spriteItem.spriteFrame = items[index%10-1];
        
        self.nameLabel.string = self.names[index%10-1];

    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private onDelete()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.node.removeFromParent();

        JCCS.instance.dakaID++;
        //localStorage.setItem('dakaid',JCCS.instance.dakaID.toString());
        JCCS.instance.mapNode.upSpotUI();
        JCCS.instance.checkin();
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
