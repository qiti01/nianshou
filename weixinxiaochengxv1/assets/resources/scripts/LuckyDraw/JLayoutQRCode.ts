
import { _decorator, Component, Node, Graphics, Color, UITransform } from 'cc';
import { QRCode } from '../common/QRCode';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JLayoutQRCode
 * DateTime = Sat Jan 22 2022 15:59:53 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JLayoutQRCode.ts
 * FileBasenameNoExtension = JLayoutQRCode
 * URL = db://assets/resources/scripts/LuckyDraw/JLayoutQRCode.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JLayoutQRCode')
export class JLayoutQRCode extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;
    @property(Graphics)
    graphics:Graphics;

    start () {
        // [3]
        let self = this;
        self.showQRCode('http://www.baidu.com');
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

	public showQRCode(url:string){

        let self = this;

		var qrcode = new QRCode(-1, 2);
		//qrcode.addData(window.location.href);
        qrcode.addData(url);
		qrcode.make();

        /**获得二维码绘制区域*/
		let size = self.graphics.node.getComponent(UITransform).getBoundingBox().width;
		let num = qrcode.getModuleCount();
		var ctx = self.graphics;
		ctx.clear();
		ctx.fillColor = Color.BLACK;
		// compute tileW/tileH based on node width and height
		var tileW = size / num;
		var tileH = size / num;
		// draw in the Graphics
		for (var row = 0; row < num; row++) {
			for (var col = 0; col < num; col++) {
				if (qrcode.isDark(row, col)) {
					var w = (Math.ceil((col + 1) * tileW) - Math.floor(col * tileW));
					var h = (Math.ceil((row + 1) * tileW) - Math.floor(row * tileW));
					ctx.rect(Math.round(col * tileW), size - tileH - Math.round(row * tileH), w, h);
					ctx.fill();
				}
			}
		}
	}

    private onHide()
    {
        let self = this;
        self.node.active = false;
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
