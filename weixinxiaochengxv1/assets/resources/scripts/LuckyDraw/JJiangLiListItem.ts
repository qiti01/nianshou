
import { _decorator, Component, Node, Label } from 'cc';
import { JCCS } from '../JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JJiangLiListItem
 * DateTime = Sat Jan 22 2022 16:19:31 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JJiangLiListItem.ts
 * FileBasenameNoExtension = JJiangLiListItem
 * URL = db://assets/resources/scripts/LuckyDraw/JJiangLiListItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JJiangLiListItem')
export class JJiangLiListItem extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Node)
    page1:Node;

    @property(Node)
    page2:Node;

    @property(Node)
    page3:Node;

    @property(Node)
    page4:Node;

    start () {
        // [3]

        let self = this;


    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    public upPage(type:number,data:any)
    {
        let self = this;

        //let names = ['','噬运兽','噬财兽','噬情兽'];

        let layout0_1 = self.page1;
        let layout0_2 = self.page2;
        let layout0_3 = self.page3;
        let layout0_4 = self.page4;

        layout0_1.active = false;
        layout0_2.active = false;
        layout0_3.active = false;
        layout0_4.active = false;

        switch(type)
        {
            case 0://没有抽奖机会
                layout0_1.active = true;
                break;
            case 1://可抽奖没抽奖
                layout0_2.active = true;
                break;
            case 2://已抽奖待领取
                layout0_4.active = true;
                break;
            case 3://已抽奖已领取
                break;
        }

        
        // if(JCCS.instance.SongZouNianShou[index])
        // {
            
        //     layout0_1.active = false;
        //     layout0_2.active = true;
        //     layout0_3.active = false;

        //     let miaosu = layout0_2.getChildByName('Label-002').getComponent(Label);
        //     if(miaosu)
        //     {
        //         miaosu.string = '已成功送走'+names[JCCS.instance.SongZouNianShou[index]];
        //     }else{
        //         console.error('没找到描述预制体');
        //     }

        // }else if(JCCS.instance.ChouJiangJiLu[jiangli])
        // {
        //     layout0_1.active = false;
        //     layout0_2.active = false;
        //     layout0_3.active = true;

        //     let miaosu = layout0_3.getChildByName('Label-002').getComponent(Label);
        //     if(miaosu)
        //     {
        //         miaosu.string = '送走'+names[JCCS.instance.ChouJiangJiLu[jiangli].monster]+'获得现金红包';
        //     }else{
        //         console.error('没找到描述预制体');
        //     }

        //     let jine = layout0_3.getChildByName('Label-001').getComponent(Label);
        //     if(jine)
        //     {
        //         jine.string =JCCS.instance.ChouJiangJiLu[jiangli].amount+'元';
        //     }else{
        //         console.error('没找到金额预制体');
        //     }

        //     let miaosu2 = layout0_3.getChildByName('Label-003').getComponent(Label);
        //     if(miaosu2)
        //     {
        //         miaosu2.string = '送走'+names[JCCS.instance.ChouJiangJiLu[jiangli].monster]+'获得现金红包';
        //     }else{
        //         console.error('没找到描述2预制体');
        //     }                    

        // }else{
        //     layout0_1.active = true;
        //     layout0_2.active = false;
        //     layout0_3.active = false;
        // }

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
