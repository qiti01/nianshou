
import { _decorator, Component, Node, Label, Input } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JJiangLiListItem1
 * DateTime = Tue Feb 01 2022 19:53:43 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JJiangLiListItem1.ts
 * FileBasenameNoExtension = JJiangLiListItem1
 * URL = db://assets/resources/scripts/LuckyDraw/JJiangLiListItem1.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JJiangLiListItem1')
export class JJiangLiListItem1 extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Label)
    la1:Label;

    @property(Label)
    la2:Label;

    public data:any;

    start () {
        // [3]

        let self = this;

        self.node.on(Input.EventType.TOUCH_END,()=>{

            if(self.data)
            {
                self.la1.string = self.data.code;
            }

        })

    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    public upPage(data:any)
    {
        let self = this;

        self.data = data;

        self.la2.string = '+'+data.amount+'元';

    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
