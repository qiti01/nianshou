
import { _decorator, Component, Node, instantiate } from 'cc';
import { JCCS } from '../JCC';
import { JJiangLiListItem } from './JJiangLiListItem';
import { JJiangLiListItem1 } from './JJiangLiListItem1';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JJiangLiList
 * DateTime = Tue Jan 25 2022 00:42:01 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JJiangLiList.ts
 * FileBasenameNoExtension = JJiangLiList
 * URL = db://assets/resources/scripts/LuckyDraw/JJiangLiList.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JJiangLiList')
export class JJiangLiList extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Node)
    contentNode:Node;

    @property(Node)
    item:Node;

    @property(Node)
    scrollView:Node;

    @property(Node)
    contentNode2:Node;

    @property(Node)
    item2:Node;

    start () {
        // [3]
        let self = this;
        self.item.active = false;    
        self.item.parent = self.node;    

        self.item2.active = false;    
        self.item2.parent = self.node;    

        self.scrollView.active = false;
    }

    // update (deltaTime: number) {
    //     // [4]
    // }


    public upUI()
    {
        let self = this;

        self.contentNode.removeAllChildren();

        let iend = 3;

        let xuyaochoujiang = 0

        if(JCCS.instance.JiFen && JCCS.instance.JiFen.zongCJCS)
        {
            xuyaochoujiang = JCCS.instance.JiFen.zongCJCS;             
        }


        if(JCCS.instance.ChouJiangJiLu.length + xuyaochoujiang > 3)
        {
            iend = JCCS.instance.ChouJiangJiLu.length;
            iend += xuyaochoujiang;
        }

        let yichoujiang    = JCCS.instance.ChouJiangJiLu.length;

        for(let i = 0 ;i < iend;i++)
        {
            let node = instantiate(self.item);
            node.active = true;
            self.contentNode.addChild(node);
            console.log('加入奖励记录:'+i);
            let s = node.getComponent(JJiangLiListItem);
            if(xuyaochoujiang)
            {
                xuyaochoujiang--;
                s.upPage(1,null);
            }else if(yichoujiang)
            {
                yichoujiang--;
                s.upPage(2,null);
            }else{
                s.upPage(0,null);
            }
        }

    }

    public upUI1()
    {
        let self = this;

        self.contentNode2.removeAllChildren();

        let hongbaolist    = JCCS.instance.hongBaoList;

        for(let i = 0 ;i < hongbaolist.length;i++)
        {
            let node = instantiate(self.item2);
            node.active = true;
            self.contentNode2.addChild(node);
            console.log('加入奖励记录:'+i);
            let s = node.getComponent(JJiangLiListItem1);

            s.upPage(hongbaolist[i]);
            
        }

    }


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
