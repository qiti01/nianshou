
import { _decorator, Node, v3, Vec3, UITransform, EventTouch, Sprite, SpriteFrame, Input, tween, Toggle, Label, view } from 'cc';
import { JComponent } from './common/JComponent';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { caozuoType } from './common/JSendServer';
import { addDanChu, getUrlSpriteFrame, HideLoading, showTip } from './common/JTools';
import { JCCS, JPlatformType } from './JCC';
import { JiuRenTangTaiZhong } from './JiuRenTangTaiZhong';
import { JNianShouMask } from './JNianShouMask';
import { PAGE_NAME, PAGE_PATH } from './JResource';
import { JSelectedRole } from './JSelectedRole';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = MapLayout
 * DateTime = Wed Dec 01 2021 18:24:52 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = mapLayout.ts
 * FileBasenameNoExtension = mapLayout
 * URL = db://assets/resources/scripts/mapLayout.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('MapLayout')
export class MapLayout extends JComponent {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    /**地图底板*/
    @property(Node)
    mapNode:Node | null = null;

    /**风景点*/
    @property(Node)
    scenicSpots:Node | null = null;

    @property(Node)
    mydian:Node | null = null;

    @property(Sprite)
    spriteLingChong:Sprite | null = null;

    @property(SpriteFrame)
    lingchongsfs:SpriteFrame[] = [];

    /**右下角角色头像*/
    @property(SpriteFrame)
    role_sf:SpriteFrame[] = [];

    @property(Sprite)
    nianshous:Sprite[] = [];

    @property(SpriteFrame)
    longzisfs:SpriteFrame[] = [];

    @property(SpriteFrame)
    longzi_null_sfs:SpriteFrame[] = [];

    @property(Node)
    spotList:Node;

    @property(Node)
    layout_dian:Node;

    @property(Label)
    qianDian:Label;

    @property(Label)
    nameDian:Label;

    @property(JNianShouMask)
    LongZiMasks:JNianShouMask[] = [];

    /**送走小年兽形成的宝石*/
    @property(Node)
    baoshis:Node[] =[];

    @property(JiuRenTangTaiZhong)
    jiurenPage:JiuRenTangTaiZhong;

    /**选择角色页面*/
    @property(JSelectedRole)
    selectRole:JSelectedRole;

    /**默认地图位置*/
    private _default_map_spot:Vec3 = v3(0,0,0);

    /**默认缩放*/
    private _default_map_scale:Vec3 = v3(0.6,0.6,1);
    //private _default_map_scale:Vec3 = v3(1,1,1);

    /**索引*/
    private _index_dian:number = -1;

    private _dian_array:Node[] = [];

    onEnable()
    {
        let self = this;
        JEventCenter.on(JEventName.upData,self.upData.bind(self),self.name);
    }
    onDisable(){
        let self = this;
        JEventCenter.off(JEventName.upData,self.name);
        
    }
    public upData(data:any)
    {
        let self = this;
        if(!data)return;
        if(data.upTypes){
            for(let i = 0 ; i < data.upTypes.length;i++)
            {
                switch(data.upTypes[i])
                {
                    case 'hideLongZi':
                        {
                            self.nianshous[data.id-1].node.active = false;
                            self.baoshis[data.id-1].active = true;
                        }
                        break;
                    case 'hideCageData':
                        {
                            if(JCCS.instance.SongZouNianShou.length == 3)
                            {
                                self.jiurenPage.node.active = true;                                
                            }
                        }
                        break;
                }
            }
        }
    }
    start () {
        // [3]
        let self = this;
        HideLoading(99);
        if(!self.mapNode || !self.scenicSpots){
            console.error('资源没有挂载');
            return;
        }
        console.log('进入地图')
        console.log(JCCS.instance.Items)

        JCCS.instance.mapNode = self;

        JCCS.instance.upMySpot();

        let hsf = JCCS.instance.HeadSpriteFrame[JCCS.instance.myHeadUrl];

        if(hsf)
        {
            self.mydian.getChildByName('Sprite').getComponent(Sprite).spriteFrame = hsf;
        }else{
            getUrlSpriteFrame(JCCS.instance.myHeadUrl,(sf:SpriteFrame)=>{
                self.mydian.getChildByName('Sprite').getComponent(Sprite).spriteFrame = sf;
            })            
        }


        if(JCCS.instance.Items && JCCS.instance.Items.length)
        {
            self._index_dian  = JCCS.instance.Items.length - 1; 
            console.log('已送走');
            console.log(JCCS.instance.SongZouNianShou);
            if(JCCS.instance.SongZouNianShou)
            {
                self._index_dian += JCCS.instance.SongZouNianShou.length*5;                   
            }
                    
        }else{
            self._index_dian = -1;
        }

        self.qianDian.string = (self._index_dian+1).toString();

        let snode = self.scenicSpots.children[self._index_dian];

        if(snode)
        {
            self.mydian.j_value = snode;
            self.mydian.setWorldPosition(snode.getWorldPosition());

            snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = true;
            snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = true; 
            
            self.nameDian.string = snode.getChildByName('Label').getComponent(Label).string;                
        }



        self.selectRole.node.active = false;

        self.spotList.active = false;
        console.log('灵宠ID：'+JCCS.instance.LingChong)


        console.log('角色ID：'+JCCS.instance.RoleIndex)

        if(JCCS.instance.RoleIndex == -1)
        {
            self.spriteLingChong.spriteFrame = self.role_sf[0];
        }else{
            self.spriteLingChong.spriteFrame = self.role_sf[JCCS.instance.RoleIndex-1]
        }


        self.baoshis.forEach(node=>{
            node.active = false;
        })

        self.jiurenPage.node.active = false;

        

        self.nianshous[0].spriteFrame = self.longzi_null_sfs[0];
        self.nianshous[1].spriteFrame = self.longzi_null_sfs[1];
        self.nianshous[2].spriteFrame = self.longzi_null_sfs[2];        

        if(JCCS.instance.Items.indexOf(1)>=0){self.nianshous[0].spriteFrame = self.longzisfs[0]};
        if(JCCS.instance.Items.indexOf(2)>=0){self.nianshous[1].spriteFrame = self.longzisfs[1]};
        if(JCCS.instance.Items.indexOf(3)>=0){self.nianshous[2].spriteFrame = self.longzisfs[2]};

        //console.error(JCCS.instance.ChouJiangJiLu)
        if(JCCS.instance.SongZouNianShou.indexOf(1)>=0){self.nianshous[0].node.active = false;}
        if(JCCS.instance.SongZouNianShou.indexOf(2)>=0){self.nianshous[1].node.active = false;}
        if(JCCS.instance.SongZouNianShou.indexOf(3)>=0){self.nianshous[2].node.active = false;}       

        self.mapNode.setPosition(self._default_map_spot);
        self.mapNode.scale = self._default_map_scale;

        let scenics = self.scenicSpots.children;
        for(let i = 0 ;i < scenics.length;i++)
        {
            let snode = scenics[i];
            snode.scale =v3(1/self._default_map_scale.x,1/self._default_map_scale.x,1);
            snode.getChildByName('Toggle').getComponent(Toggle).interactable     = false;  
            snode.getChildByName('ToggleName').getComponent(Toggle).interactable = false;   
            
            if(JCCS.instance.dakaID-1 >= i)
            {
                console.log('打卡成功id：'+i);
                console.log('daka:'+JCCS.instance.dakaID);
                snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = true;
                snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = true;   
                
                self.mydian.j_value = snode;
                self.mydian.setWorldPosition(snode.getWorldPosition());
            }else{
                snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = false;
                snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = false;                     
            }
        }

        self.mydian.scale =v3(1/self._default_map_scale.x,1/self._default_map_scale.x,1);
        if(self.mydian.j_value)
        self.mydian.setWorldPosition(self.mydian.j_value.getWorldPosition());

        //let dian = self.getScenicSpot(14);
        //self.upMapSpot(dian.getPosition());

        /**最大溢出值*/
        let offset  = 50;
        /**列表滑动位置x*/
        let curX    = 0;
        /**列表滑动位置y*/
        let curY    = 0;
        /**滑动力度*/
        let vy_x      = 0;
        let vy_y      = 0;
        /**弹力,值越大,到度或到顶后,可以继续拉的越远*/
        let fl        = 150;
        /**是否在滚动*/
        let isInTransition = false;
        /**开始时间*/
        let touchStartTime = 0;

        let start_x = 0;
        let start_y = 0;

        let friction_x = 0;
        let friction_y = 0;

        /**惯性定时器*/
        let inertiaTimer = ()=>{

            if(Math.abs(vy_x)>0.5)
            {
                if(vy_x>0)
                {
                    vy_x -= friction_x;
                }else{
                    vy_x += friction_x;
                }

            }else{
                vy_x = 0;
            }

            if(Math.abs(vy_y)>0.5)
            {
                if(vy_y>0)
                {
                    vy_y -= friction_y; 
                }else{
                    vy_y += friction_y; 
                }
               
            }else{
                vy_y = 0;
            }


            let spot = self.mapNode.getPosition();

            let tui      = self.mapNode.getComponent(UITransform);
            let mainRect = self.node.getComponent(UITransform).getBoundingBox();
            let w = tui.getBoundingBox().width/2 -mainRect.width/2  - offset;
            let h = tui.getBoundingBox().height/2-mainRect.height/2 - offset;

            let ab_x = 0;
            let ab_y = 0;

            let tSpot_x = spot.x+vy_x;
            let tSpot_y = spot.y+vy_y;

            if(tSpot_x > w || tSpot_x < -w)
            {
                ab_x = 50;
                if(tSpot_x > w)
                {
                    ab_x = -50;
                }
            }

            if(tSpot_y > h || tSpot_y < -h)
            {
                ab_y = 50;
                if(tSpot_y > h)
                {
                    ab_y = -50;
                }
            }   

            if(ab_x || ab_y)
            {
                let sspot = self.mapNode.getPosition();
                tween(self.mapNode)
                .to(0.2,{position:v3(sspot.x+ab_x,sspot.y+ab_y)},{easing:'cubicOut'})
                .start()
                self.unschedule(inertiaTimer);
                return;
            }
            
            self.mapNode.setPosition(tSpot_x,tSpot_y);

            if(vy_x==0 && vy_y==0)
            {
                self.unschedule(inertiaTimer);
            }

        }

        self.node.on(Input.EventType.TOUCH_START,(event:EventTouch)=>{
            self.unschedule(inertiaTimer);
            curX = 0;
            curY = 0;
            vy_x = 0;
            vy_y = 0;

            touchStartTime = new Date().getTime();

            start_x = event.getUILocation().x;
            start_y = event.getUILocation().y;
           

        })

        self.node.on(Input.EventType.TOUCH_MOVE,(event:EventTouch)=>{

            let a = event.getUIDelta();

            let touchs = event.getTouches();
            
            if(touchs.length >2){
                vy_x = 0;
                vy_y = 0;
                return;
            }
            if(touchs.length ==2)
            {
                touchs[0].j_value=true;
                touchs[1].j_value=true;
                let touch1 = touchs[0],touch2 = touchs[1];
                let delta1 = touch1.getDelta(),delta2=touch2.getDelta();
                let touchPoint1= touch1.getLocation();
                let touchPoint2= touch2.getLocation();
                let distance = touchPoint1.subtract(touchPoint2);
                let delta = delta1.subtract(delta2);
                let scale = 1;
                if(Math.abs(distance.x)>Math.abs(distance.y))
                {
                    scale = (distance.x+delta.x)/distance.x*self.mapNode.scale.x;
                    
                }else{
                    scale = (distance.y+delta.y)/distance.y*self.mapNode.scale.y;
                }

                let tui = self.mapNode.getComponent(UITransform);
                let mainRect = self.node.getComponent(UITransform).getBoundingBox();

                if(scale>4 || scale < 0.34){
                    vy_x = 0;
                    vy_y = 0;
                    return;
                }
                if(tui.contentSize.width*scale < mainRect.width || tui.contentSize.height*scale < mainRect.height)
                {
                    //self.mapNode.scale = v3(self.mapNode.scale.x+0.01,self.mapNode.scale.x+0.01);
                    self.mapNode.setPosition(v3(0,0));
                    vy_x = 0;
                    vy_y = 0;
                    return;
                }
                let spot = self.mapNode.getPosition();

                if(spot.x > 0)
                {
                    if(tui.contentSize.width*scale/2-spot.x - 50 < mainRect.width/2 )
                    {
                        let w = mainRect.width/2 - (tui.contentSize.width*scale/2-spot.x) ;
                        spot.x -=w;
                    }
                }else{
                    if(tui.contentSize.width*scale/2+spot.x - 50 < mainRect.width/2 )
                    {
                        let w = mainRect.width/2 - (tui.contentSize.width*scale/2+spot.x) ;
                        spot.x +=w;
                    }
                }
                if(spot.y > 0)
                {
                    if(tui.contentSize.height*scale/2-spot.y - 50 < mainRect.height/2 )
                    {
                        let h = mainRect.height/2 - (tui.contentSize.height*scale/2-spot.y) ;
                        spot.y -=h;
                    }
                }else{
                    if(tui.contentSize.height*scale/2+spot.y - 50 < mainRect.height/2 )
                    {
                        let h = mainRect.height/2 - (tui.contentSize.height*scale/2+spot.y) ;
                        spot.y +=h;
                    }
                }                

                self.mapNode.setPosition(spot);

                self.mapNode.scale=v3(scale,scale);

                self.scenicSpots.children.forEach(node=>{
                    node.scale =v3(1/scale,1/scale,1);
                })

                self.mydian.scale =v3(1/scale,1/scale,1);
                if(self.mydian.j_value)
                self.mydian.setWorldPosition(self.mydian.j_value.getWorldPosition());
                
                //self.upMapSpot(v3(0,0,0));

                vy_x = 0;
                vy_y = 0;
                return;
            }
            
            if(event.touch.j_value)
            {
                vy_x = 0;
                vy_y = 0;
                return;
            }

            let spot = self.mapNode.getPosition();

            spot.x += a.x;
            spot.y += a.y;

            let tui = self.mapNode.getComponent(UITransform);
            let mainRect = self.node.getComponent(UITransform).getBoundingBox();
            let w = tui.getBoundingBox().width/2 -mainRect.width/2  - 20;
            let h = tui.getBoundingBox().height/2-mainRect.height/2 - 20;

            if(spot.x > w || spot.x < -w)
            {
                vy_x = 0;
                vy_y = 0;
                return;
            }

            if(spot.y > h || spot.y < -h)
            {
                vy_x = 0;
                vy_y = 0;
                return;
            }

            self.mapNode.setPosition(spot);

            let touchEndTime = new Date().getTime();

            if(touchEndTime-touchStartTime > 40)
            {
                let end_x = event.getUILocation().x;
                let end_y = event.getUILocation().y;

                vy_x = end_x - start_x;
                vy_y = end_y - start_y;

                friction_x = 0.5;
                friction_y = 0.5;

                start_x = end_x;
                start_y = end_y;
            }



        })

        self.node.on(Input.EventType.TOUCH_END,(event:EventTouch)=>{
            let a = event.getUIDelta();
            let touchs = event.getTouches();

            if(event.touch.j_value)
            {
                return;
            }
            if(vy_x != 0 || vy_y != 0)
            {
                self.schedule(inertiaTimer,0.02);
            }

        })


        for(let i = 0 ;i < JCCS.instance.SongZouNianShou.length;i++)
        {
            self.nianshous[JCCS.instance.SongZouNianShou[i]-1].node.active = false;
            self.baoshis[JCCS.instance.SongZouNianShou[i]-1].active = true;            
        }                  

    }



    // update (deltaTime: number) {
    //     // [4]
    // }

    /**获得风景点*/
    public getScenicSpot(index:number)
    {
        let self = this;

        let node = self.scenicSpots!.getChildByName('dian'+index);

        return node;
    }
    
    /**更新地图位置*/
    public upMapSpot(dianSpot:Vec3)
    {
        let self = this;

        let tui = self.mapNode.getComponent(UITransform);

        let mapSpot  = self.mapNode.getPosition();
        let mainRect = self.node.getComponent(UITransform).getBoundingBox();

        let w = tui.getBoundingBox().width/2 -mainRect.width/2;
        let h = tui.getBoundingBox().height/2-mainRect.height/2;

        let tx = 0;
        if(dianSpot.x > 0)
        {
            tx = (mapSpot.x-dianSpot.x)*self.mapNode.scale.x;
            if(tx < -w)
            {
                tx = -w;
            }
        }else{
            tx = (mapSpot.x-dianSpot.x)*self.mapNode.scale.x;
            if(tx > w)
            {
                tx = w;
            }
        }

        let ty = 0;
        if(dianSpot.y > 0)
        {
            ty = (mapSpot.y-dianSpot.y)*self.mapNode.scale.y;
            if(ty < -h)
            {
                ty = -h;
            }
        }else{
            ty = (mapSpot.y-dianSpot.y)*self.mapNode.scale.y;
            if(ty > h)
            {
                ty = h;
            }
        }

        self.mapNode.setPosition(v3(tx,ty));


    }


    private onShowLuckyDraw()
    {
        //JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.LUCKY_DRAW});  
        JCCS.instance.audioManager.playSound('button');
        JCCS.instance.SendServer.sendCaozuo(caozuoType.treasure_box);
        addDanChu(JCCS.instance.MainS,PAGE_NAME.LUCKY_DRAW)
    }

    private onShowCageData(event:EventTouch,index:string)
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        //console.error(index);
        //JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.CAGE_DATA,data:index});

        let node = JCCS.instance.LoadManage.getNode(PAGE_NAME.CAGE_DATA);
        if(!node)
        {
            JCCS.instance.LoadManage.loadPrefab(PAGE_NAME.CAGE_DATA,()=>{
                
                node = JCCS.instance.LoadManage.getNode(PAGE_NAME.CAGE_DATA);
                node.j_convey_data = index;
                self.addPage_danchu(node);

            },PAGE_PATH)
        }else{
            node.j_convey_data = index;
            self.addPage_danchu(node);
        } 

        switch(index)
        {
            case '1'://铜色
                console.log('铜色');
                break;
            case '2'://黄色
                console.log('黄色');
                break;
            case '3'://红色
                console.log('红色');
                break;
        }
    }

    private showGetItem(id:string)
    {
        let self = this;
        JCCS.instance.SendServer.checkin(id,(data:any)=>{
            let obj = JSON.parse(data);
            if(obj.result)
            {
                let str = '';
                switch(Number(obj.result))
                {
                    case 1:
                        str = '已经获取过得地点';
                        break;
                    case 2:
                        break;
                }
                if(str != '')
                {
                    showTip(self.node,str);                        
                }
            }else{
                if(obj.item)
                {
                    let nianID = Number(obj.item);
                    if(nianID<10)
                    {
                        //显示年兽获得页面
                        addDanChu(self,PAGE_NAME.GET_SHOU,nianID)
                        self.nianshous[nianID-1].spriteFrame = self.longzisfs[nianID-1];
                    }else{
                        //显示道具获得页面
                        addDanChu(self,PAGE_NAME.GET_ITEM,nianID)
                    }                        
                    JCCS.instance.Items.push(nianID);
                }else{
                    showTip(JCCS.instance.MainNode,'很遗憾没有获得任何道具。');
                }
            }
        })
        

    }

    private showGetShou()
    {
        let self = this;
        //JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.GET_SHOU});
        //addDanChu(self,PAGE_NAME.GET_SHOU)
        //JCCS.instance.SendServer.userClear();

        switch(JCCS.instance.Platform)
        {
            case JPlatformType.web_localhost:
                {

                }
                break;
            case JPlatformType.web_mobile:
                {

                    // let size = view.getVisibleSize();
                    // console.log('开启扫码');
                    // self.scheduleOnce(()=>{
                    //     window['QRCodeInit']({w:size.width,h:size.height});            
                    // },1)

                    // window['wxScanCode']((res:any)=>{
                    //     console.log(res);
                    //     if(res && res.result)
                    //     {
                    //         if(Number(res.result) > 0 && Number(res.result) < 100)
                    //         {
                    //             self.showGetItem(res.result);                          
                    //         }else{
                    //             showTip(JCCS.instance.MainNode,'不是活动二维码');
                    //         }
                    //     }else{
                    //         showTip(JCCS.instance.MainNode,'不是活动二维码');
                    //     }

                    // });                     
                }
                break;
            case JPlatformType.wx_mobile:
            case JPlatformType.wx_localhost:
                {

                }
                break;
        }

    }

    private showChat()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        //JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.CHAT_PAGE});
        let node = JCCS.instance.LoadManage.getNode(PAGE_NAME.CHAT_PAGE);
        if(!node)
        {
            JCCS.instance.LoadManage.loadPrefab(PAGE_NAME.CHAT_PAGE,()=>{
                
                node = JCCS.instance.LoadManage.getNode(PAGE_NAME.CHAT_PAGE);
                self.addPage_danchu(node);

            },PAGE_PATH)
        }else{
            self.addPage_danchu(node);
        } 
    }


    private onShowMapSpotList()
    {
        let self = this;
        //self.spotList.active = true;
        JCCS.instance.audioManager.playSound();
        self.selectRole.node.active = true;
        self.selectRole.showRoleData();
    }

    /**更新坐标派发奖励*/
    private upSpot()
    {
        let self = this;

        if(JCCS.instance.mySpot.Latitude == -1 || JCCS.instance.mySpot.Longitude == -1)
        {
            console.warn('没有我的坐标');
            return;
        }

        console.log('更新坐标');

        let names = JCCS.instance.SpotNames;

        let myLon = JCCS.instance.mySpot.Longitude;
        let myLat = JCCS.instance.mySpot.Latitude;

        for(let i = names.length-1;i>=0;i--)
        {
            let name = names[i];
            if(JCCS.instance.SpotDatas[name])
            {
                let lon = JCCS.instance.SpotDatas[name].Longitude;
                let lat = JCCS.instance.SpotDatas[name].Latitude;

                if(lon + 10 > myLon && lon -10 < myLon &&
                   lat + 10 > myLat && lat -10 < myLat){
                    console.log('在这个点内')

                    if(i==JCCS.instance.dakaID || i < JCCS.instance.dakaID || JCCS.instance.dakaID==-1)
                    {
                        return;
                    }else{
                        
                        for(let k = i;k > JCCS.instance.dakaID;k--)
                        {
                            JCCS.instance.SendServer.checkin(k.toString(),(data:any)=>{
                                let obj = JSON.parse(data);
                                if(obj.result)
                                {
                                    let str = '';
                                    switch(Number(obj.result))
                                    {
                                        case 1:
                                            str = '已经获取过得地点';
                                            break;
                                        case 2:
                                            break;
                                    }
                                    if(str != '')
                                    {
                                        showTip(self.node,str);                        
                                    }
                                }else{
                                    if(obj.item)
                                    {
                                        let nianID = Number(obj.item);
                                        if(nianID<10)
                                        {
                                            //显示年兽获得页面
                                            addDanChu(self,PAGE_NAME.GET_SHOU,nianID)
                                            self.nianshous[nianID-1].spriteFrame = self.longzisfs[nianID-1];
                
                                        }else{
                                            //显示道具获得页面
                                            addDanChu(self,PAGE_NAME.GET_ITEM,nianID)
                                        }                        
                                        JCCS.instance.Items.push(nianID);
                                    }else{
                                        showTip(JCCS.instance.MainNode,'很遗憾没有获得任何道具。');
                                    }
                
                                }
                            })
                        }

                        self.upSpotUI();

                        return;
                    }

                }

            }
        }

    }


    /**更新已经过的点*/

    public upSpotUI(){
        let self = this;

        console.log('更新点UI：'+JCCS.instance.dakaID)
        let scenics = self.scenicSpots.children;
        for(let i = 0 ;i < scenics.length;i++)
        {
            let snode = scenics[i];
            if(JCCS.instance.dakaID-1 >= i)
            {
                snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = true;
                snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = true;   
                
                self.mydian.j_value = snode;
                self.mydian.setWorldPosition(snode.getWorldPosition());


                self.nameDian.string = snode.getChildByName('Label').getComponent(Label).string;
                self.qianDian.string = (i+1).toString();
            }else{
                snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = false;
                snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = false;                     
            }
        }        

    }


    /**更新点*/
    private chengGongDingWeiXiaYiGeDian()
    {
        let self = this;

        self._index_dian++;
        if(self._index_dian >=21)
        {
            return;
        }

        self.qianDian.string = (self._index_dian+1).toString();

        let snode = self.scenicSpots.children[self._index_dian];

        self.mydian.j_value = snode;
        self.mydian.setWorldPosition(snode.getWorldPosition());

        snode.getChildByName('Toggle').getComponent(Toggle).isChecked     = true;
        snode.getChildByName('ToggleName').getComponent(Toggle).isChecked = true; 
        
        self.nameDian.string = snode.getChildByName('Label').getComponent(Label).string;    
        
        if( JCCS.instance.Platform == JPlatformType.web_localhost )
        {
            let itemID = [1,2,3,11,12,13,14,21,22,23,24,31,32,33,34,101,102,103,104,105,106]
            let nianID = itemID[self._index_dian];
            if(self._index_dian >= itemID.length)
            {
                return;
            }
            if(nianID<10)
            {
                //显示年兽获得页面
                addDanChu(self,PAGE_NAME.GET_SHOU,nianID)
                self.nianshous[nianID-1].spriteFrame = self.longzisfs[nianID-1];

            }else{
                if(nianID>100)
                {
                    addDanChu(self,PAGE_NAME.GET_DATI,nianID)
                }else{
                    //显示道具获得页面
                    addDanChu(self,PAGE_NAME.GET_ITEM,nianID)                    
                }
            }                        
            JCCS.instance.Items.push(nianID);

            self.upLongZi();

        }else{

            JCCS.instance.SendServer.checkin(self._index_dian.toString(),(data:any)=>{
                let obj = JSON.parse(data);
                if(obj.result)
                {
                    let str = '';
                    switch(Number(obj.result))
                    {
                        case 1:
                            str = '已经获取过得地点';
                            break;
                        case 2:
                            break;
                    }
                    if(str != '')
                    {
                        showTip(self.node,str);                        
                    }
                }else{
                    if(obj.item)
                    {
                        let nianID = Number(obj.item);
                        if(nianID<10)
                        {
                            //显示年兽获得页面
                            addDanChu(self,PAGE_NAME.GET_SHOU,nianID)
                            self.nianshous[nianID-1].spriteFrame = self.longzisfs[nianID-1];

                        }else{
                            if(nianID>100)
                            {
                                addDanChu(self,PAGE_NAME.GET_DATI,nianID)
                            }else{
                                //显示道具获得页面
                                addDanChu(self,PAGE_NAME.GET_ITEM,nianID)     
                                  
                                self.upLongZi();                                
                            }
                        }                        
                        JCCS.instance.Items.push(nianID);
                    }else{
                        showTip(JCCS.instance.MainNode,'很遗憾没有获得任何道具。');
                    }

                }
            })
        }



    }

    public upLongZi()
    {
        let self = this;

        let a = [];
        let b = [];
        let c = [];

        JCCS.instance.Items.forEach(id=>{
            if(id<20 && id > 10)
            {
                a.push(id);
            }
            if(id<30 && id > 20)
            {
                b.push(id);
            }  
            if(id<40 && id > 30)
            {
                c.push(id);
            }          
        })

        console.log(a);
        console.log(b);
        console.log(c);

        self.scheduleOnce(()=>{
            self.LongZiMasks[0].upHeight(a.length);
            self.LongZiMasks[1].upHeight(b.length);
            self.LongZiMasks[2].upHeight(c.length);
        },0.1)

    }

    /**向前*/
    private onForward()
    {
        let self = this;

        self.chengGongDingWeiXiaYiGeDian();

        // if( self._index_dian == -1)
        // {
        //     self.mydian.setWorldPosition(self.scenicSpots.children[0].getWorldPosition()); 
        // }
        //self._index_dian++;
        //distance(36.946121,113.913727,36.946003,113.912941);
    }

    /**向后*/
    private onBackward()
    {
        let self = this;


    }    


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
