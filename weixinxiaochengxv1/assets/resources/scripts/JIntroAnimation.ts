
import { _decorator, Component, Node, UIOpacity, tween, v3 } from 'cc';
import { JCCS } from './JCC';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JIntroAnimation
 * DateTime = Wed Dec 22 2021 22:33:37 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JIntroAnimation.ts
 * FileBasenameNoExtension = JIntroAnimation
 * URL = db://assets/resources/scripts/JIntroAnimation.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JIntroAnimation')
export class JIntroAnimation extends Component {

    @property(UIOpacity)
    dayun1:UIOpacity;

    @property(UIOpacity)
    dayun2:UIOpacity;

    @property(Node)
    mountain:Node;

    @property(UIOpacity)
    liangshou:UIOpacity;

    @property(UIOpacity)
    xiaoshou:UIOpacity;

    @property(UIOpacity)
    dashou:UIOpacity;

    @property(UIOpacity)
    denglong:UIOpacity;

    @property(Node)
    lights:Node [] =[];

    @property(UIOpacity)
    nianshouTip:UIOpacity;

    @property(UIOpacity)
    layout1:UIOpacity;

    @property(UIOpacity)
    layout2:UIOpacity;

    public AnimationEnd:Function;

    start () {
        // [3]
        let self = this;
        self.layout2.node.active = false;
        self.nianshouTip.opacity = 0;


        //self.scheduleOnce(()=>{

            // tween(self.dayun1)
            // .to(1,{opacity:255})
            // .delay(1)
            // .to(1,{opacity:0})
            // .start()

            // tween(self.dayun2)
            // .to(1,{opacity:255})
            // .call(()=>{
            //     self.mountain.active = true;

            //     self.lights[5].active = true;
            //     self.lights[6].active = true;
            //     self.lights[7].active = true;
            //     self.lights[8].active = true;

            // })
            // .delay(0.5)
            // .call(()=>{
            //     self.lights[0].active = true;
            //     self.lights[1].active = true;
            //     self.lights[2].active = true;
            //     self.lights[3].active = true;
            //     self.lights[4].active = true;


            //     self.lights[9].active = true;
            //     self.lights[10].active = true;
            //     self.lights[11].active = true;
            //     self.lights[12].active = true;

            // })
            // .delay(0.5)
            // .to(1,{opacity:0})
            // .call(()=>{

            // })
            // .start()

        //},3)

        let ui = self.mountain.getComponent(UIOpacity);
        ui.opacity = 0;
        self.mountain.active = true;

        tween(ui)
        .to(1,{opacity:255})
        .call(()=>{
            self.lights[5].active = true;
            self.lights[6].active = true;
            self.lights[7].active = true;
            self.lights[8].active = true;
            self.scheduleOnce(()=>{
                self.lights[0].active = true;
                self.lights[1].active = true;
                self.lights[2].active = true;
                self.lights[3].active = true;
                self.lights[4].active = true;
            },0.5)
            self.scheduleOnce(()=>{
                self.lights[9].active = true;
                self.lights[10].active = true;
                self.lights[11].active = true;
                self.lights[12].active = true;
            },0.75)            
        })
        .start()



        self.scheduleOnce(()=>{

            tween(self.liangshou)
            .to(1,{opacity:255})
            .start()

            // tween(self.xiaoshou)
            // //.delay(0.5)
            // .to(1,{position:v3(-227.145,71.261),scale:v3(1,1,1)})
            // .start()

            self.xiaoshou.node.setPosition(v3(-227.145,71.261));
            self.xiaoshou.node.scale = v3(1,1,1);

            tween(self.xiaoshou)
            //.delay(1.5)
            .to(1.5,{opacity:255})
            .start()

            tween(self.dashou)
            //.delay(1.5)
            .to(1.5,{opacity:255})
            .start()

            tween(self.nianshouTip)
            //.delay(1.5)
            .to(1.5,{opacity:255})
            .start()

            tween(self.denglong)
            //.delay(1.5)
            .to(1,{opacity:255})
            .delay(3)
            .call(()=>{

                tween(self.layout1)
                .to(1,{opacity:0})
                .start();

                self.layout2.opacity = 0;
                self.layout2.node.active = true;
                tween(self.layout2)
                .to(1,{opacity:255})
                .start();

            })
            .start()

        },3)


        


    }

    private onEnd()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        console.log('年兽页面动画结束了')
        if(self.AnimationEnd)self.AnimationEnd();        
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
