
import { _decorator, Component, Node, UIOpacity, tween, v3 } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JLight
 * DateTime = Thu Dec 23 2021 00:07:58 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JLight.ts
 * FileBasenameNoExtension = JLight
 * URL = db://assets/resources/scripts/JLight.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JLight')
export class JLight extends Component {

    @property
    jx:number = 0;

    @property
    jy:number = 0;

    @property
    js:number = 0;

    @property
    time:number = 0;

    @property(UIOpacity)
    uio:UIOpacity;

    start () {
        // [3]

        let self = this;

        

    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    onEnable()
    {
        let self = this;
        tween(self.node)
        .to(self.time,{position:v3(self.jx,self.jy),scale:v3(self.js,self.js,1)})
        .start()

        tween(self.uio)
        .to(self.time/2,{opacity:255})
        .start()    
    }
    onDisable(){
        let self = this;

        
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
