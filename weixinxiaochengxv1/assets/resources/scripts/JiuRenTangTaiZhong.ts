
import { _decorator, Component, Node, Sprite, Label, SpriteFrame } from 'cc';
import { caozuoType } from './common/JSendServer';
import { addDanChu, getUrlSpriteFrame, showTip } from './common/JTools';
import { JCCS } from './JCC';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JiuRenTangTaiZhong
 * DateTime = Mon Jan 17 2022 17:34:37 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JiuRenTangTaiZhong.ts
 * FileBasenameNoExtension = JiuRenTangTaiZhong
 * URL = db://assets/resources/scripts/JiuRenTangTaiZhong.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JiuRenTangTaiZhong')
export class JiuRenTangTaiZhong extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    @property(Node)
    jiuren:Node;

    @property(Sprite)
    sHead:Sprite;

    @property(Label)
    lName:Label;

    start () {
        // [3]
        let self = this;
        self.jiuren.active = false;


        let hsf = JCCS.instance.HeadSpriteFrame[JCCS.instance.myHeadUrl];

        if(hsf)
        {
            self.sHead.spriteFrame = hsf;
        }else{
            getUrlSpriteFrame(JCCS.instance.myHeadUrl,(sf:SpriteFrame)=>{
                self.sHead.spriteFrame = sf;
            })            
        }

        self.lName.string = JCCS.instance.myName;

    }


    private onShowChifengPage()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.jiuren.active = true;
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    private onChouJiang()
    {
        let self = this;

        JCCS.instance.audioManager.playSound('button');
        self.node.removeFromParent();
        addDanChu(JCCS.instance.MainS,PAGE_NAME.LUCKY_DRAW)
    }

    private onFenXiang()
    {
        let self = this;
        showTip(JCCS.instance.MainNode,'功能在研发中')
        JCCS.instance.SendServer.sendCaozuo(caozuoType.share);
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
