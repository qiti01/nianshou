import { JClass } from "./common/JClass";
import { LM_stage_d } from "./common/JLoadManage";


/**共用页面名称*/
export enum PAGE_NAME{
    /**欢迎页面*/
    WELCOMEL = 'welcomeLayout',
    /**地图*/
    MAP         = 'mapLayout',
    /**抽奖相关*/
    LUCKY_DRAW  = 'LuckyDrawLayout',
    /**选灵宠*/
    GUIDE       = 'guideLayout',
    /**获得神兽*/
    GET_SHOU    = 'getShouLayout',
    /**获得物品*/
    GET_ITEM    = 'getItemLayout',
    /**已获得道具*/
    CAGE_DATA   = 'cageDataLayout',
    /**聊天页面*/
    CHAT_PAGE   = 'chatLayout',
    /**提示页面*/
    TIP         = 'tipLayout',
    /**加载页面*/
    LOADING     = 'LoadingPage',
    /**获得答题*/
    GET_DATI    = 'AnswerPage',
}

export const PAGE_PATH =  'prefab/';

/**公共资源*/
export class JResource extends JClass {

    public Prefabs:LM_stage_d = {
        prefabs:[{
                    name:PAGE_NAME.WELCOMEL,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.MAP,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.LUCKY_DRAW,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.GUIDE,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.GET_SHOU,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.GET_ITEM,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.CAGE_DATA,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.TIP,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },
                {
                    name:PAGE_NAME.LOADING,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },                
                {
                    name:PAGE_NAME.GET_DATI,
                    path:PAGE_PATH,
                    backCallFuns:[]
                },

        ],
        imgds:[],
        length:0,
        size:0,
        available:false,
        backCallFuns:[],
        //moduleType:E_ModuleType.SUBFRAME,

    }
    /**返回加载集合*/
    public getPrefabs():LM_stage_d{
        let self = this;
        return self.Prefabs;
    }
}