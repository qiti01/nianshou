
import { _decorator, Node, EventTouch, Label, Sprite, SpriteFrame } from 'cc';
import { JComponent } from './common/JComponent';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { caozuoType } from './common/JSendServer';
import { HideLoading } from './common/JTools';
import { JAnswerPage } from './JAnswerPage';
import { JCCS, JPlatformType } from './JCC';
import { JIntroAnimation } from './JIntroAnimation';
import { PAGE_NAME } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JGuide
 * DateTime = Mon Dec 06 2021 17:27:22 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JGuide.ts
 * FileBasenameNoExtension = JGuide
 * URL = db://assets/resources/scripts/JGuide.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('JGuide')
export class JGuide extends JComponent {

    @property(Node)
    yindao:Node | null = null

    /**选角色和灵宠*/
    @property(Node)
    xuanlingchong:Node | null = null;

    /**年兽出来的动画*/
    @property(Node)
    xiaoguo:Node | null = null;

    /**唐太宗动画*/
    @property(Node)
    tangTaiZhong:Node;

    @property(Node)
    yindao_frames:Node[] =[];

    @property(Node)
    graySprite:Node | null = null;

    // @property(Label)
    // wifiTip:Label | null = null;

    @property(Sprite)
    laotou:Sprite | null = null;
    
    @property(SpriteFrame)
    laotouSFs:SpriteFrame [] = [];

    private _yindao_index:number = 0;

    onLoad(){
        let self = this;
        self.node.j_showTime = 0.5;
    }
    start () {
        // [3]
        HideLoading(99);
        let self = this;
        self.yindao.active = false;
        self.xuanlingchong.active = false;
        self.xiaoguo.active       = false;
        self.tangTaiZhong.active  = true;

        let ttz = self.tangTaiZhong.getComponent(JAnswerPage);
        ttz.AnimationEnd = ()=>{
            //self.tangTaiZhong.active = false;

            self.xiaoguo.active      = true;

            switch(JCCS.instance.Platform)
            {
                case JPlatformType.web_localhost:
                case JPlatformType.web_mobile:
                    {
                        JCCS.instance.GuiDe = 0;
                    }
                    break;
                case JPlatformType.wx_mobile:
                case JPlatformType.wx_localhost:
                    {
    
                    }
                    break;
            }

            let ns = self.xiaoguo.getComponent(JIntroAnimation);
            ns.AnimationEnd = ()=>{
                self.yindao.active       = true;
                self.scheduleOnce(()=>{
                    self.xiaoguo.active       = false;
                },0.6)
                self.dataOK(0.1);                
            }

        }

        self.graySprite.active = false;

        //self.scheduleOnce(()=>{
            //self.yindao.active = true;
        //},1)

        
        
        for(let i = 0 ; i < 5;i++)
        {
            if(i==0)
            {
                self.yindao_frames[i].active = true;
            }else{
                self.yindao_frames[i].active = false;
            }
        }

        // let laotouindex = 0;
        // self.laotou.spriteFrame = self.laotouSFs[laotouindex];
        // self.schedule(()=>{
        //     laotouindex++;
        //     if(laotouindex==self.laotouSFs.length)
        //     {
        //         laotouindex = 0;
        //     }
        //     self.laotou.spriteFrame = self.laotouSFs[laotouindex];
        // },0.2)


    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    public dataOK(time:number)
    {
        let self = this;

        self.scheduleOnce(()=>{
            if(JCCS.instance.GuiDe != -1)
            {
                if(JCCS.instance.GuiDe == 1)
                {
                    console.log('---1')
                    JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.MAP}); 
                }else{
                    console.log('---0')
                    self.hide_danchu(self.xiaoguo,0.5);    
                }
            }else{
                self.dataOK(1);
            }
        },time)
    }

    private  nextPage()
    {
        let self = this;
        JCCS.instance.audioManager.playSound();
        self.yindao_frames[self._yindao_index].active = false;
        self._yindao_index++;
        self.graySprite.active = true;
        if(self._yindao_index==self.yindao_frames.length)
        {
            self.xuanlingchong.active = true;
            self.yindao.active = false;
        }else{
            self.yindao_frames[self._yindao_index].active = true;
        }

    }


    private onWifi(event:EventTouch)
    {
        let self = this;
        let node = event.target as Node;
        node.active = false;
        //self.wifiTip.string = 'WI-FI 已开启';

        JCCS.instance.audioManager.playSound('button');
    }


    private onGoToMap()
    {
        let self = this;
        JCCS.instance.audioManager.playSound('button');
        JCCS.instance.SendServer.setGuide();
        JCCS.instance.SendServer.setLingChong((data:any)=>{
            let obj = JSON.parse(data);
            if(obj.result)
            {

            }else{
                JEventCenter.emit(JEventName.pageJump,{page:PAGE_NAME.MAP});                     
            }
        })

        JCCS.instance.SendServer.setRole((data:any)=>{

        })

    }


    private onSkipGuide()
    {
        let self = this;
        console.log('跳过引导');
        JCCS.instance.SendServer.sendCaozuo(caozuoType.guide_skip);
        JCCS.instance.audioManager.playSound('button');
        self.xuanlingchong.active = true;
        self.yindao.active = false;

    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
