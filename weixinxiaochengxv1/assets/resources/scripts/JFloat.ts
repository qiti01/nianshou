
import { _decorator, Component, Node, tween, v3 } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JFloat
 * DateTime = Thu Dec 23 2021 19:16:53 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JFloat.ts
 * FileBasenameNoExtension = JFloat
 * URL = db://assets/resources/scripts/JFloat.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('JFloat')
export class JFloat extends Component {


    start () {
        // [3]
        let self = this;

        let spot = self.node.getPosition();
        tween(self.node)
        .to(2,{position:v3(spot.x,-50.09)},{easing:'cubicOut'})
        .to(2,{position:v3(spot.x,spot.y)},{easing:'cubicOut'})
        // .to(1,{position:v3(spot.x,-53.09)})
        // .delay(0.5)
        // .to(1,{position:v3(spot.x,spot.y)})
        .union()
        .repeatForever()
        .start()



    }


    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
