
import { _decorator, Node } from 'cc';
import { JComponent } from './common/JComponent';
import { JEventCenter, JEventName } from './common/JEventCenter';
import { JLogManage } from './common/JLogManage';
import { showLoading } from './common/JTools';
import { JCCS, JPlatformType } from './JCC';
import { JResource, PAGE_NAME, PAGE_PATH } from './JResource';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = JMain
 * DateTime = Mon Dec 06 2021 16:35:51 GMT+0800 (中国标准时间)
 * Author = qiti01
 * FileBasename = JMain.ts
 * FileBasenameNoExtension = JMain
 * URL = db://assets/resources/scripts/JMain.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 */
 
@ccclass('JMain')
export class JMain extends JComponent {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;
    private _pages:Node[] = [];
    private res:JResource = new JResource();

    isWechat() {
        return String(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)) === "micromessenger";
    }

    onLoad(){
        let self = this;
        self.node.j_showTime = 0;

        console.log('+++++连接地址++++++')
        console.log(location.href);

        switch(JCCS.instance.Platform)
        {
            case JPlatformType.web_localhost:
                {

                }
                break;
            case JPlatformType.web_mobile:
                {
                    const code     = JCCS.instance.SendServer.getUrlParam('code');     
                    let spotID:any = JCCS.instance.SendServer.getUrlParam('itemID');

                    if(!spotID)spotID = 0;

                    if(self.isWechat())
                    {
                        if(code == null || code == '')
                        {
                            console.log('走微信内嵌登录');
                            let local = encodeURIComponent(window.location.href); //获取当前页面地址作为回调地址
                            let appid = 'wxf8f88e804ccada86'
                            //通过微信官方接口获取code之后，会重新刷新设置的回调地址【redirect_uri】
                            window.location.href =
                                "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" +
                                appid + 
                                "&redirect_uri=" +
                                local +
                                "&response_type=code&scope=snsapi_userinfo&state="+spotID+"#wechat_redirect";

                        }else{

                            console.log('内嵌浏览器code:'+code);
                            spotID = JCCS.instance.SendServer.getUrlParam('state');
                            if(spotID)
                            {
                                JCCS.instance.checkinID = spotID;
                            }
                            JCCS.instance.SendServer.gzhLogin(code,(res:any)=>{

                            });

                        }

                    }else{

                        if(code == null || code == '')
                        {
                            console.log('请求code');
                            window.location.href = "https://open.weixin.qq.com/connect/qrconnect?appid=wx7f78db24bec0ecdd&redirect_uri=https://data.17boluo.com/tour/&response_type=code&scope=snsapi_login&state="+spotID+"#wechat_redirect"
                        }else{
                            spotID = JCCS.instance.SendServer.getUrlParam('state');
                            if(spotID)
                            {
                                JCCS.instance.checkinID = spotID;
                            }
                            console.log('code:'+code);
                            JCCS.instance.SendServer.h5Login(code,(res:any)=>{

                            });
                        }            
                    } 
                }
                break;
            case JPlatformType.wx_mobile:
            case JPlatformType.wx_localhost:
                {

                }
                break;
        }






    }
    onEnable()
    {
        let self = this;
        JEventCenter.on(JEventName.pageJump,self.pageJump.bind(self),self.name);
    }
    onDisable(){
        let self = this;
        JEventCenter.off(JEventName.pageJump,self.name);
        
    }
    
    start () {
        // [3]
        let self = this;
        /**log显示开关*/
        JLogManage.instance.ShowLog = true;
        JCCS.instance.MainNode = self.node;
        JCCS.instance.MainS    = self;
        JCCS.instance.LoadManage.loadRes_single(self.res.getPrefabs(),(a:number,b:number)=>{
            console.log('加载进度：'+a+'/'+b);
            if(a==b)
            {
                
            }
        })

        //self.showPage(PAGE_NAME.MAP);
        self.showPage(PAGE_NAME.WELCOMEL);
    }

    // update (deltaTime: number) {
    //     // [4]
    // }

    /**显示登录页面*/
    private showPage(name:string,data:any = null)
    {
        let self = this;
        let node = JCCS.instance.LoadManage.getNode(name);
        if(!node)
        {
            JCCS.instance.LoadManage.loadPrefab(name,()=>{
                
                node = JCCS.instance.LoadManage.getNode(name);
                node.j_convey_data = data;
                self.addGamePage(node);

            },PAGE_PATH)
        }else{
            node.j_convey_data = data;
            self.addGamePage(node);
        }  
    }

    /**加入游戏界面*/
    public addGamePage(page:Node)
    {
        let self = this;
        self._pages.push(page);          
        self.addPage_danchu(page,()=>{
            self.removePages();
        });
      
    }
    /**清理页面*/
    public removePages()
    {
        let self = this;
        while(self._pages.length)
        {
            if(self._pages.length==1)
            {
                break;
            }
            
            let node = self._pages.shift();
            node?.removeFromParent();
        }
    }

    /**切换页面*/
    private pageJump(data:any)
    {
        let self = this;
        if(!data || !data.page)return;
        self.j_log('跳转页面：'+ data.page)

        showLoading(99);

        self.showPage(data.page,data.data);

    }


}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/zh/scripting/life-cycle-callbacks.html
 */
